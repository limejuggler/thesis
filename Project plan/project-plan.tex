% Title: Project plan for degree projects
% Version: 1

\documentclass[12pt,a4paper,twoside]{article}
\usepackage{times}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}

\title{Project plan for degree projects}
\author{DV2524: Examensarbete i Datavetenskap för civilingenjörer}
\date{Version 2 -- \today} % Make sure to update 


\begin{document}
\maketitle

\vspace*{-3ex}
\noindent
\begin{tabular}{|l|l|p{10cm}|}
\hline
\multirow{2}{*}{Thesis}
 & Tentative title 		& Dynamic Creation of Multi-resolution Triangulated Irregular Network on GPU\\\cline{2-3}
 & Classification 		& I.3.3 Picture and Image Generation; I.3.7 Three-Dimensional Graphics and Realism. \\
% I.3.1 Parallel processing; I.3.5 Curve, surface, solid, and object representations \\
\hline
\multirow{4}{*}{Student 1} 
 & Name           		& Emil Bertilsson \\\cline{2-3}
 & e-Mail                   	& embl10@student.bth.se \\\cline{2-3}
 & Social security nr       	& 19910916-2595 \\\cline{2-3}
\hline
\multirow{3}{*}{Supervisor}
 & Name and title 		& Dr. Prashant Goswami \\\cline{2-3}
 & e-Mail                   	& prg@bth.se \\\cline{2-3}
 & Department               	&  Department of Creative Technologies\\
\hline
\end{tabular}

\section{Introduction}
Today the graphics cards are used to a far bigger extent than before and the areas of usage are not limited to just rendering \cite{nvidia}. Algorithms can migrate from using the central processing unit(CPU) to using only the graphics card and the massive parallelism inherent in today's graphics cards. The architecture is optimized for doing calculations in parallel and will in this thesis be used to generate a terrain mesh based on Triangulated Irregular Network(TIN).

A problem with applying a regular grid of triangles is that it does not differentiate areas of high resolution or low resolution. In areas where the elevation difference is low, the number of triangles will be notably more than what is otherwise needed and vice versa. Although this approach offers simplicity and speed, every rendering call is made with both redundant and missing triangles and level of detail algorithms are commonly used to reduce the number of triangles that are drawn for any given frame. TIN based meshes use triangles of irregular size to increase resolution where terrain features require more triangles and decrease resolution in absence of terrain features. Using TIN grids instead offers other problems, primarily computational complexity and execution time (and memory requirements to some extent). TIN meshes are subsequently generated in offline tools or during start up and not during run time \cite{multirestin}. The strategy of creating a multi-resolution TIN based mesh with different resolution dependent on the distance from the camera, rather than user defined resolution over the entire mesh \cite{multirestin}, would therefore be of interest for further studies.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/tinsVSsubdivision}
	\caption{TINs can easily adapt to high frequency variations of the terrain such as cliffs, while many subdivision levels are needed for regular subdivision meshes, that spend a large fraction of the triangle budget in edge following \protect\cite{bdam2003}.}
	\label{fig:tinsVSsubdivision}
\end{figure}

Regular grid meshes can employ subdivision in order to create multi-resolution, and in this category of algorithms there are techniques such as BDAM \cite{bdam2003}, SOAR \cite{soar2002} and GPU tessellation \cite{gputessellation2009}. A comparison between the SOAR technique and a TIN mesh is presented in Fig \ref{fig:tinsVSsubdivision}. It illustrates the problem of subdivision where terrain features meets a flat part of the terrain, which results in a large amounts of triangles close to the edge. In theory therefore, a TIN generated mesh can utilize better positioned points and subsequently less triangles. Tessellation implementations can achieve very good performance, but since it is done every frame, meshes that are carefully created can have an advantage over subdivided terrain. However, this precision is reached through far more computations which makes it difficult to manage during run-time and makes it hard to cache them on the graphics card's memory.

A multi-resolution TIN mesh where the calculations for the required amount of triangles to represent a given patch would use the Euclidean distance between the camera and the patch and use it as a measurement to determine the resolution. This measurement is modified using screen space pixel error, what change of resolution the user can and cannot see, and the complexity of the terrain features, as described earlier.

The basis of a TIN generated mesh is the Delaunay's triangulation method \cite{delaunay2000} which in this case needs to be implemented on the graphics card. A Delaunay's triangulation method ensures that triangles are not smaller than a certain size along the x-axis or z-axis. By implementing a parallel version of the algorithm, the data is grouped into patches which are then run in parallel \cite{paralleldelaunay}.

While TINs are a well studied subject, neither of aforementioned references deals with processing all of the data in runtime without any pre-processing. 

\section{Aim and objectives}
The aim of this thesis is to create a new algorithm with parts that have previously not been combined together so that a more optimized terrain mesh can be used. The layout of this mesh should contain less triangles while keeping a low pixel error and the structure of the mesh should not limit the ability to implement further optimizations. Other optimizations to terrain meshes will not be covered in this paper, however view dependent culling and storage optimizations are topics that are deemed to be the next step after this thesis.

The aim of the basic technique is to optimize the generated mesh so that it has fewer redundant or missing triangles while simultaneously keeping a good structural quality and minimizing the preprocessing computation. Thus, the objective will be to present a technique that, with the addition of more complex optimizations, will outperform meshes created by regular grids. The scope does not cover testing against other techniques and the result will therefore be presented on its own without comparisons.

\section{Research questions}
RQ1: Can a TIN based terrain mesh be generated during run-time using user specified pixel error?
%and if so, what generation thresholds can be identified that limit the velocity of the user?
\\\\
RQ2: How much can the mesh be simplified while respecting the complexity of the independent regions?
\\\\
The work conducted during this thesis goes under the hypothesis that a dynamically updating multi-resolution TIN will result in a strong basis for continuous research that, upon further research and improvement, may result in a high performing algorithm.

\section{Method}
To show the simplification performed on the mesh, video would be presented during final presentations. In the thesis report, comparison  would be provided as a function of pixel error \cite{bdam2003} \cite{gpumetrics2010} specified by the user and also compared against the original lossless mesh. The presented measurements will be quantitative, and to some extent qualitative. 

The quantitative measurements will be presented in tables, images and graphs. By presenting the data in these formats and through empirical testing of movement across the terrain mesh, the question of generation time in theoretical and practical sense will be answered. The threshold that will be identified for movement versus distance covered before updating the mesh will be presented and discussed. This includes the statistics of frames per second and triangle throughput, i.e. the number of triangles produced per second. Additionally, settings that are deemed to be reasonable for performance demanding applications will be presented in an attempt to put the findings into context.

%The measurements will be quantitative, and to some extent qualitative. The quantitative measurements will be data presented in tables, images and graphs while the qualitative measurements will be as described in the first paragraph.

\subsection{Implementation}
The strategy for the implementation is an iterative process which starts by creating a basic functioning version of the Delaunay triangulation on the CPU as well as a selection algorithm for determining points that would undergo triangulation. The next step in the process will be to translate the algorithm onto the graphics card and restructuring it so that the parallelism can be properly utilized. When completed, the next step will be to improve the selection algorithm by implementing view dependent selection. This step also includes setting up a basic rendering environment. Finally, error metrics and integrating the different parts along with basic settings concludes the implementation process.

The majority of the implementation is scheduled during phase 1 (as described in Section 7), and covers all but the last step described above.

\subsection{Experimentation}
The quantitative and qualitative measurements will be gathered and researched in the experimentation phase. By working with different settings and experimenting with optimization techniques, the data necessary for presenting a feasible result will be gathered. Essentially, this phase will be used for answering the research questions with conclusions and reasoning based on what has been collected. 

\section{High level description of algorithm}
The algorithm will be implemented both on the CPU and the graphics card as different parts of the algorithm can utilize one more than the other. Sequential code and high level commands to the triangulation method will be handled by the CPU and steps that can be run in parallel will be delegated to the graphics card.

\begin{enumerate}
  \item The height data (floating points stored in a list with Width x Height elements) is examined and points are selected so that the desired resolution is achieved. The selection process works with patches and use overlaps so that patches can be computed independent of each other. Difference in resolution is derived from user location and terrain features.
  \item 3D positions are created using the points derived from the previous step and are sent to the Delaunay triangulation on the graphics card.
  \item The triangulation is done on the created patches. The overlap eliminates the need for an additional stage handling borders.
  \item The buffers are swapped on the graphics card and the pointer on the CPU side is updated. The content of the previous buffer is discarded.
\end{enumerate}

Each patch, as mentioned in step 2, will write to its own buffer which will allow for patches to remain in the same state during updates. The benefit of keeping a patch unchanged is that patches that during last update were very far from the camera, and therefore had a very low resolution, may receive the same resolution during the next update. To determine such cases, a resolution value corresponding to every patch will be calculated and stored.

\section{Expected outcomes}
The yielded result of this research is expected to give clarity surrounding an implementation of real-time TIN generation on the graphics card and how well it can be utilized. Changes to the basic structure of the algorithm are expected to happen so that it may be more optimized to run in parallel. Reflecting on the aim of this algorithm, the actual execution time is not expected to allow real-time updating with small intervals when the user progresses rapidly over the terrain. Rapid movement, like a free roaming camera flying over the surface, that is faster and covers ground quickly, may prove to be too much for the algorithm to handle in real time. In such cases the update threshold will have to be increased, which in turn decreases the resolution of the mesh so that the computations can be completed in time. By increasing the threshold, pixel error of the mesh will be increased.

There will not be any difference between using this algorithm on a procedurally generated terrain or a statically defined terrain. As long as the data for all the terrain features can be sent to the algorithm, it does not matter how the mesh is created.

The resulting mesh is not expected to be in order, meaning that the triangles of the mesh might not be as structured in the same way as a mesh generated by a regular network of points. Further optimizations on the mesh, including compressing the number of bits needed to store the terrain data \cite{compressingtins2000} and a quad tree implementation on the TIN grid \cite{quadtin2002}, may need an additional pass to reorganize the mesh in order to apply the algorithms.

But there are problems with irregular grids and the data required to store and represent the necessary information. In a survey published 2007 \cite{gobbetti2007}, the authors wrote that:

\begin{quote}
Because of their flexibility, fully irregular approaches are theoretically capable of producing the minimum complexity representation for a given error measurement. However, this flexibility comes at a price. In particular, mesh connectivity, hierarchy, and dependencies must explicitly be encoded, and simplification and coarsening operations must handle arbitrary neighbourhoods.
\end{quote}
Although this flexibility comes at a price, the algorithm is still expected to deliver positive results, in the sense that it will shed light on dynamic multi-resolution irregular meshes and whether or not such approaches have enough potential for widespread usage.

\section{Time and activity plan}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{Images/gantt.pdf}
  \caption{High level view of the scheduled time, represented by a Gantt chart.}
  \label{fig:gantt}
\end{figure}

The implementation will be split into two major parts, as evident by Fig \ref{fig:gantt}, where the foundation is developed during the first phase. The foundation of the implementation is the Delaunay's triangulation method on the graphics card and the functionality to generate different resolution based on defined error metrics. The error metrics will be completed during the second phase, but the functionality for multi-resolution is considered to be vital for the basis of the algorithm and should therefore be completed earlier. The work is to be conducted incrementally and as such, the scope will be closely monitored and along with weekly meetings with supervisor, should not stray from the initial approach without good reasons.

Of this first phase, the majority of the time will be spent on adapting Delaunay's triangulation method to run in parallel and attempts to decrease the execution time. It also includes setting up a simple 3D rendering environment, however, with the author's existing pool of 3D projects this time is estimated to be low. During the adaptation of the algorithm, the support for generating TINs with multiple possible resolutions will be added. Research and reading published articles is also included in this time.

The second phase consists of less complex tasks, such as implementing error metrics, sending the height data to the graphics card and defining distance metrics for when the algorithm should be triggered. It also has time allocated to cover some of the necessary optimizations to reach the goals discussed earlier.

Finally, the experimentation covers measurements and experimenting with different optimization techniques and settings to produce quantifiable results. All the data that will be presented will be produced during this phase.

The deadlines and the writing can be seen in Figure \ref{fig:gantt}.

\section{Risk management}
This section lists the perceived risks and then proceeds to describing the impact and mitigation of these risks. The risks are listed without concern to internal order.

\begin{itemize}
\item Loss of context. During the development process, choices are made that moves the solution further away from real life applications, thereby reducing the value of the results.
\item Failure to restructure the algorithm for the architecture of the graphics card. If the structure of the algorithm cannot be made to work almost entirely in parallel then the benefits of implementing it on the graphics card may be lost.
\end{itemize}

Several possible strategies for keeping the solution relevant for real life applications exist, the most effective is to work alongside a company in order to use their pool of knowledge and experience. The received input, if understood and assimilated properly, would ensure that the result carried relevance to the field of study. Another way of gathering knowledge and intuitive thoughts is to seek the knowledge and experience of colleagues, mainly those also undergoing their thesis, to provide intellectual exchange in a symbiotic way.

Should the restructuring of the algorithm prove to be too difficult, then parts of it may be delegated to the CPU instead. Although this solution would take care of parts of the problem, it will also create new concerns regarding sending data between the graphics card and the CPU. If this risk cannot be mitigated properly it will still result in knowledge, and even though it is not in line with the aim of the algorithm, the relevance of the findings are not lost.

In the event that the goals cannot be met, the focus of the thesis can be shifted slightly so that the effort is put primarily into understanding and quantifying why the proposed algorithm proved inert. Details about what parts of the algorithm prevented it from achieving the desired behaviour and how these problems could be addressed, as well as explanations of why it happened will be described. The implementation would still be completed, but part of the time dedicated to measurements would instead be spent as described earlier in this paragraph.

\bibliographystyle{plain}
\bibliography{project-plan}
\end{document}