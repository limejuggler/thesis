//#include "helpFunctions.hlsl"

#define radiusStep 5
// (32 + 1) * (32 + 1) = 1089
// 32 + 1 due to the shared borders
#define arraySize 1089

#define PI 3.14159265359
#define PI_2 6.28318530718
#define PI_0_5 1.57079632679
#define PI_1_5 4.71238898038

// Look into append buffers
#define maxNeighbours 16

cbuffer cbTriangulationConstants : register(b0)
{
	uint	bufferOffset;
	float	patchDimX,
			patchDimZ,
			padding;
};


struct TriangleList
{
	uint3 n[maxNeighbours];
};

groupshared uint groupID;
//groupshared uint begin;
//groupshared uint end;

groupshared int lastPtr1[arraySize];
groupshared uint belongCount[arraySize];
groupshared uint triangleCount[arraySize];

groupshared bool isBorderPoint[arraySize];

// The index buffer
RWByteAddressBuffer buffer : register(u0);

RWStructuredBuffer<float2> pointList : register(u1);

// Start and end id. Two elements for each warp
RWStructuredBuffer<uint> sizeIndices : register(u2);
RWStructuredBuffer<TriangleList> belongList : register(u3);
RWStructuredBuffer<TriangleList> tempList : register(u4);
RWStructuredBuffer<uint> patchIndices : register(u5);

// Save first and second neighbour here instead of variables to save temp registers.
// 2 spots per thread
RWStructuredBuffer<int> neighBours : register(u6);

RWStructuredBuffer<uint> groupIDSorted : register(u7);

// Can only use 8 registers
RWStructuredBuffer<uint> belongCount2 : register(u8);

struct Circle
{
	float2	center;
	float	radius;

	void set(float2 pos, float r)
	{
		center = pos;
		radius = r;
	}
	void set(float2 p1, float2 p2)
	{
		center = (p1 + p2) / 2;
		radius = length(p1 - p2) / 2;
	}
	bool inside(float2 p)
	{
		return (p.x - center.x) * (p.x - center.x) + (p.y - center.y) * (p.y - center.y) < radius * radius;
	}
};

// Line (actually a vector)
struct Line
{
	float2		v0,
				v1;

	void setWithAngle(float2 p, float angle)
	{
		v0 = p;
		v1 = normalize(float2(p.x + cos(angle), p.y + sin(angle)) - p);
	}

	bool toTheRightOfLine(float2 p)
	{
		if(v1.x == 0.f)
			return	v1.y > 0.f && p.x > v0.x ||
					v1.y <= 0.f && p.x < v0.x;

		float det = (p.y - v0.y) - (v1.x == 0.f ? -99999999.f : v1.y / v1.x) * (p.x - v0.x);

		return		v1.x > 0.f && det < 0.f ||
					v1.x <= 0.f && det > 0.f;
	}
};

float getAngle(float2 p0, float2 p1, float2 p2)
{
	float angle;

	// p0 and p1
	if(p0.x == p1.x)
		angle = p1.y < p0.y ? PI_1_5 : PI_0_5;
	else
	{
		angle = atan((p1.y - p0.y) / (p1.x - p0.x));

		if(p0.x > p1.x)			angle += PI;
		else if(p0.y > p1.y)	angle += PI_2;
	}

	// p1 and p2
	if(p1.x == p2.x)
		angle = (p2.y < p1.y ? PI_1_5 : PI_0_5) - angle + PI;
	else
	{
		angle = atan((p2.y - p1.y) / (p2.x - p1.x)) - angle + PI;

		if(p1.x > p2.x)			angle += PI;
		else if(p1.y > p2.y)	angle += PI_2;
	}

	// Make sure the angle lies between 0 and 2 pi
	while(angle >= PI_2)		angle -= PI_2;
	while(angle < 0)			angle += PI_2;

	return angle;
}

int triangulateFromPoint(uint id, bool isBorderPoint)
{
	precise int ptr1, ptr2, temp;

	uint nNeighbours, i;
	bool ptr2InList;
	float maxAngle, radius, distance;
	uint3 tri;

	Circle circle;
	Line l;

	belongCount[id - sizeIndices[groupID * 2]] = 0;
	triangleCount[id - sizeIndices[groupID * 2]] = 0;

	maxAngle = 1.0e+10;

	// These conditions has to be stored in variables and not specified in the loop declaration, or else
	// the aggressive optimization distorts the result and (rather ironically) increases computation time
	tri.x = lastPtr1[id - sizeIndices[groupID * 2]] == -1 ? sizeIndices[groupID * 2] : (uint)lastPtr1[id - sizeIndices[groupID * 2]] < id ? id : sizeIndices[groupID * 2];;
	tri.y = lastPtr1[id - sizeIndices[groupID * 2]] == -1 ? sizeIndices[groupID * 2 + 1] : (uint)lastPtr1[id - sizeIndices[groupID * 2]] > id ? id : sizeIndices[groupID * 2 + 1];;

	for(i = tri.x; i < tri.y; i++)
	{
		//if(type == PointType::frame)
		//if(isBorderPoint)
		//{
		//	if(id == sizeIndices[groupID * 2] || id == sizeIndices[groupID * 2 + 1] - 1)
		//	{
		//		if(pointList[i].y != pointList[id].y)
		//			continue;
		//	}
		//	else if(pointList[i].y != pointList[id].y && (pointList[id].y != pointList[id + 1].y || pointList[id].y != pointList[id - 1].y))
		//	{
		//		if(pointList[i].x != pointList[id].x)
		//			continue;
		//	}
		//	else
		//	{
		//		if(pointList[i].y != pointList[id].y)
		//			continue;
		//	}
		//}
		// The values dealt with should not be big enough to require square root to ensure correct result
		radius =	(pointList[i].x - pointList[id].x) * (pointList[i].x - pointList[id].x) +
					(pointList[i].y - pointList[id].y) * (pointList[i].y - pointList[id].y);
	
		// If it is a border point, it requires a slightly different condition to be able to find its neighbours
		if(isBorderPoint && radius < maxAngle || !isBorderPoint && radius <= maxAngle)
		{
			// Will only be triggered once per thread and should not be examined during each iteration
			if(/*!isBorderPoint && */i == id)
				continue;
	
			maxAngle = radius;
			ptr1 = i;
		}
	}
	
	neighBours[(id - tri.x) * 2] = ptr1;
	neighBours[(id - tri.x) * 2 + 1] = -1;

	nNeighbours = 1;

	

	while(nNeighbours < maxNeighbours)
	{
		// Identify the triangle in which id is a vertex and is close to ptr1.
		// If the point is not in the neighbour list to id, and if it is also
		// on the right side of the two other points, the point is decided to
		// be the final point in the triangle.

		ptr2 = -1;
		ptr2InList = false;
		for(i = 0; i < belongCount[ptr1 - sizeIndices[groupID * 2]]; i++)
		{
			// temp is used in several areas to reduce the number of registers being used
			temp = -1;
		
			tri = belongList[ptr1].n[i];
		
			if(tri.x == id)
			{
				if(		(int)tri.y == ptr1)		temp = (int)tri.z;
				else if((int)tri.z == ptr1)		temp = (int)tri.y;
			}
			else if(tri.y == id)
			{
				if(		(int)tri.x == ptr1)		temp = (int)tri.z;
				else if((int)tri.z == ptr1)		temp = (int)tri.x;
			}
			else if(tri.z == id)
			{
				if(		(int)tri.x == ptr1)		temp = (int)tri.y;
				else if((int)tri.y == ptr1)		temp = (int)tri.x;
			}
		
			if(temp == -1)
				continue;
			
			// Check again to see if it is still redundant
			// Appears to be redundant
			//if(neighbour1 == temp && !(neighbour2 == ptr1))
			//if(neighBours[id * 2] == temp && !(neighBours[id * 2 + 1] == ptr1))
			//{
			//	ptr2InList = true;
			//	ptr2 = temp;
			//	break;
			//}
		
			if(getAngle(pointList[id], pointList[ptr1], pointList[temp]) < PI)
			{
				ptr2InList = true;
				ptr2 = temp;
				break;
			}
		}
		
		// If no point was located during the previous step, find it using geometrical conditions
		// Define a circle that runs through id and ptr1 and find the third point to complete the circle
		if(ptr2 == -1)
		{
			temp = 0;

			radius = length(pointList[id] - pointList[ptr1]) / 2;

			distance = 0.0;

			//ptr2 = -1;
			tri.x = (uint)ptr1;

			// 
			// This loop fucks up ptr1, and after the loop ptr1 is back to normal...
			// 
			[loop]
			while(ptr2 == -1)
			{
				//buffer.Store2((id - sizeIndices[groupID * 2]) * 2 * 4, uint2(id - sizeIndices[groupID * 2], (uint)tri.x - sizeIndices[groupID * 2]));
				////buffer.Store2(((id - sizeIndices[groupID * 2]) * 4 + 2) * 4, uint2(id, (uint)pointList[ptr1].x, (uint)pointList[ptr1].y));
				//return -1;

				distance += radius * radiusStep;

				maxAngle = getAngle(float2(pointList[id].x + 1000000.f, pointList[id].y), pointList[id], pointList[ptr1]) - PI_0_5;
				if(maxAngle - PI_0_5 < 0)
					maxAngle += PI_2;
				// The line used for defining the circle
				l.setWithAngle((pointList[id] + pointList[ptr1]) / 2, maxAngle);
				circle.set(l.v0 + (l.v1 * distance), length(pointList[id] - l.v0 - (l.v1 * distance)));

				// Reset the line to handle the calculations for deciding points in the loop below
				//l.set(pointList[id], pointList[ptr1]);
				l.v0 = pointList[id];
				l.v1 = normalize(pointList[ptr1] - pointList[id]);

				// Look for the point that is to the right vector and included in the circle defined outside the loop.
				// Among these points, selects the one which minimizes the angle between id, ptr1 and the new point

				// Angles received from getAngle method ranges from 0 <= angle < PI * 2
				// This means that the first itteration will always trigger the if statement
				maxAngle = -PI;
				ptr2 = -1;

				//tri.x = sizeIndices[groupID * 2];
				//tri.y = sizeIndices[groupID * 2 + 1];
				//for(i = tri.x; i < tri.y; i++)
				for(i = sizeIndices[groupID * 2]; i < sizeIndices[groupID * 2 + 1]; i++)
				//for(i = begin; i < end; i++)
				{
					if(!l.toTheRightOfLine(pointList[i]))
						continue;

					//if(l.toTheRightOfLine(pointList[i]))
					//{
					// These conditions were examined during each itteration and only triggered twice.
					// They were subsequently moved here to reduce time wasted on if statments
					//if(i == id || i == (uint)ptr1)
					//	continue;

					temp++;

					if(!circle.inside(pointList[i]) || i == id || i == (uint)ptr1)
						continue;

					// If a thread reaches this point, then there will be no other iteration of the while loop
					// The radius varible is therefore not needed anymore, so resuse it and save a register

					//radius = getAngle(pointList[ptr1], pointList[i], pointList[id]);
					//if((radius >= maxAngle && isBorderPoint) || (radius > maxAngle && !isBorderPoint))
					//{
					//	ptr2 = i;
					//	maxAngle = radius;
					//}
					if(getAngle(pointList[ptr1], pointList[i], pointList[id]) > maxAngle)
					{
						ptr2 = i;
						maxAngle = getAngle(pointList[ptr1], pointList[i], pointList[id]);
					}
				}
				// No points are found that are to the right of the created line, the loop is terminated
				if(temp == 0)
					break;
			}

			// If there are no points on the right side of ptr0 -> ptr1, then all triangles has been located
			if(temp == 0)
				break;
		}

		// If the connected point is the second point of the neighbour list, then stop and exit
		if(!ptr2InList)
		{
			// Create a new triangle with ptr0, ptr1 and ptr2
			temp = triangleCount[id - sizeIndices[groupID * 2]];
			tempList[id].n[temp] = uint3(id, ptr1, ptr2);

			belongList[id].n[belongCount[id - sizeIndices[groupID * 2]]++] = tempList[id].n[temp];
			belongList[ptr1].n[belongCount[ptr1 - sizeIndices[groupID * 2]]++] = tempList[id].n[temp];
			belongList[ptr2].n[belongCount[ptr2 - sizeIndices[groupID * 2]]++] = tempList[id].n[temp];

			triangleCount[id - sizeIndices[groupID * 2]]++;

			if(neighBours[(id - sizeIndices[groupID * 2]) * 2] == ptr2)
				break;
		}

		if(neighBours[(id - sizeIndices[groupID * 2]) * 2 + 1] == -1)
			neighBours[(id - sizeIndices[groupID * 2]) * 2 + 1] = ptr2;

		nNeighbours++;
		ptr1 = ptr2;
	}

	//temp = 0;
	//tri.x = id - sizeIndices[groupID * 2];
	//for(i = 0; i < tri.x; i++)
	//	temp += triangleCount[i];
	//
	//tri.y = triangleCount[id - sizeIndices[groupID * 2]];
	//for(i = 0; i < tri.y; i++)
	//	buffer.Store3((temp + i + groupID * bufferOffset) * 3 * 4, uint3(patchIndices[tempList[id].n[i].x], patchIndices[tempList[id].n[i].y], patchIndices[tempList[id].n[i].z]));

	return isBorderPoint ? neighBours[(id - sizeIndices[groupID * 2]) * 2] : -1;
}

// Possible other solution:
// Change patch dimension to 32x16 (512 points, 561 maximum) and compute 2 patches per dispatch

// This reduces the possibility of more than 1024 points per dispatch and can extended into
// pairing together offsets so that small point count is paired up with large point count

// Can try to have threads responsible for an entire patch instead of just one element
// in each patch

[numthreads(1024, 1, 1)]
void main(uint threadInGroupID : SV_GroupIndex, uint groupIDReceived : SV_GroupID)
{
	if(threadInGroupID.x == 0)
	{
		groupID = groupIDReceived;
		//begin = sizeIndices[groupID * 2];
		//end = sizeIndices[groupID * 2 + 1];
	}

	// Clear all values from previous dispatch calls
	isBorderPoint[threadInGroupID.x] = false;
	belongCount[threadInGroupID.x] = 0;
	triangleCount[threadInGroupID.x] = 0;
	lastPtr1[threadInGroupID.x] = -1;

	if(threadInGroupID.x + 1024 < arraySize)
	{
		isBorderPoint[threadInGroupID.x + 1024] = false;
		belongCount[threadInGroupID.x + 1024] = 0;
		triangleCount[threadInGroupID.x + 1024] = 0;
		lastPtr1[threadInGroupID.x + 1024] = -1;
	}

	isBorderPoint[threadInGroupID.x] = (uint)pointList[threadInGroupID.x + sizeIndices[groupID * 2]].x % patchDimX == 0 ||
									   (uint)pointList[threadInGroupID.x + sizeIndices[groupID * 2]].y % patchDimZ == 0;


	if(threadInGroupID.x < sizeIndices[groupID * 2 + 1] - sizeIndices[groupID * 2])
	{
		if(triangulateFromPoint(threadInGroupID.x + sizeIndices[groupID * 2], isBorderPoint[threadInGroupID.x]) != -1)
			triangulateFromPoint(threadInGroupID.x + sizeIndices[groupID * 2], false);
	}

	// In the event that a patch consists of more than 1024 points (can occur due to the addition of the borders)
	if(sizeIndices[groupID * 2 + 1] - sizeIndices[groupID * 2] >= 1024)
	{
		if(threadInGroupID.x + 1024 < sizeIndices[groupID * 2 + 1] - sizeIndices[groupID * 2])
		{
			isBorderPoint[threadInGroupID.x + 1024] =	(uint)pointList[threadInGroupID.x + sizeIndices[groupID * 2]].x % patchDimX == 0 ||
														(uint)pointList[threadInGroupID.x + sizeIndices[groupID * 2]].y % patchDimZ == 0;

			if(triangulateFromPoint(threadInGroupID.x + sizeIndices[groupID * 2] + 1024, isBorderPoint[threadInGroupID.x + 1024]) != -1)
				triangulateFromPoint(threadInGroupID.x + sizeIndices[groupID * 2] + 1024, false);
		}
	}

	// Only allow the first thread to write into the buffer
	if(threadInGroupID.x == 0)
	{
		int counter = 0, id;
		for(int i = 0; i < arraySize; i++)
		{
			id = i + sizeIndices[groupID * 2];
			for(uint j = 0; j < triangleCount[i]; j++)
			{
				buffer.Store3(((j + counter) * 3 + groupIDSorted[groupID] * bufferOffset) * 4, uint3(	patchIndices[tempList[id].n[j].x],
																						patchIndices[tempList[id].n[j].y],
																						patchIndices[tempList[id].n[j].z]));
			}
			counter += triangleCount[i];
		}
	}
}

int triangulateFromPoint2(uint id, uint ptr0, bool isBorderPoint)
{
	precise int ptr1, ptr2, temp;

	uint nNeighbours, i;
	bool ptr2InList;
	float maxAngle, radius, distance;
	uint3 tri;

	Circle circle;
	Line l;

	//belongCount2[ptr0] = 0;
	triangleCount[id] = 0;

	maxAngle = 1.0e+10;

	// These conditions has to be stored in variables and not specified in the loop declaration, or else
	// the aggressive optimization distorts the result and (rather ironically) increases computation time
	tri.x = lastPtr1[ptr0 - sizeIndices[id * 2]] == -1 ? sizeIndices[id * 2] : (uint)lastPtr1[ptr0 - sizeIndices[id * 2]] < ptr0 ? ptr0 : sizeIndices[id * 2];;
	tri.y = lastPtr1[ptr0 - sizeIndices[id * 2]] == -1 ? sizeIndices[id * 2 + 1] : (uint)lastPtr1[ptr0 - sizeIndices[id * 2]] > ptr0 ? ptr0 : sizeIndices[id * 2 + 1];;

	for(i = tri.x; i < tri.y; i++)
	//for(i = sizeIndices[id * 2]; i < sizeIndices[id * 2 + 1]; i++)
	{
		//if(type == PointType::frame)
		//if(isBorderPoint)
		//{
		//	if(id == sizeIndices[id * 2] || id == sizeIndices[id * 2 + 1] - 1)
		//	{
		//		if(pointList[i].y != pointList[id].y)
		//			continue;
		//	}
		//	else if(pointList[i].y != pointList[id].y && (pointList[id].y != pointList[id + 1].y || pointList[id].y != pointList[id - 1].y))
		//	{
		//		if(pointList[i].x != pointList[id].x)
		//			continue;
		//	}
		//	else
		//	{
		//		if(pointList[i].y != pointList[id].y)
		//			continue;
		//	}
		//}
		// The values dealt with should not be big enough to require square root to ensure correct result
		radius =	(pointList[i].x - pointList[ptr0].x) * (pointList[i].x - pointList[ptr0].x) +
					(pointList[i].y - pointList[ptr0].y) * (pointList[i].y - pointList[ptr0].y);
		
		// If it is a border point, it requires a slightly different condition to be able to find its neighbours
		if(isBorderPoint && radius < maxAngle || !isBorderPoint && radius <= maxAngle)
		{
			// Will only be triggered once per thread and should not be examined during each iteration
			if(/*!isBorderPoint && */i == ptr0)
				continue;
		
			maxAngle = radius;
			ptr1 = i;
		}
	}

	neighBours[(ptr0 - tri.x) * 2] = ptr1;
	neighBours[(ptr0 - tri.x) * 2 + 1] = -1;

	nNeighbours = 1;

	//while(nNeighbours < maxNeighbours)
	//{
	//	// Identify the triangle in which id is a vertex and is close to ptr1.
	//	// If the point is not in the neighbour list to id, and if it is also
	//	// on the right side of the two other points, the point is decided to
	//	// be the final point in the triangle.
	//
	//	ptr2 = -1;
	//	ptr2InList = false;
	//	for(i = 0; i < belongCount2[ptr1]; i++)
	//	{
	//		// temp is used in several areas to reduce the number of registers being used
	//		temp = -1;
	//
	//		tri = belongList[ptr1].n[i];
	//
	//		if(tri.x == ptr0)
	//		{
	//			if(		(int)tri.y == ptr1)		temp = (int)tri.z;
	//			else if((int)tri.z == ptr1)		temp = (int)tri.y;
	//		}
	//		else if(tri.y == ptr0)
	//		{
	//			if(		(int)tri.x == ptr1)		temp = (int)tri.z;
	//			else if((int)tri.z == ptr1)		temp = (int)tri.x;
	//		}
	//		else if(tri.z == ptr0)
	//		{
	//			if(		(int)tri.x == ptr1)		temp = (int)tri.y;
	//			else if((int)tri.y == ptr1)		temp = (int)tri.x;
	//		}
	//
	//		if(temp == -1)
	//			continue;
	//
	//		if(getAngle(pointList[ptr0], pointList[ptr1], pointList[temp]) < PI)
	//		{
	//			ptr2InList = true;
	//			ptr2 = temp;
	//			break;
	//		}
	//	}
	//
	//	// If no point was located during the previous step, find it using geometrical conditions
	//	// Define a circle that runs through id and ptr1 and find the third point to complete the circle
	//	if(ptr2 == -1)
	//	{
	//		temp = 0;
	//
	//		radius = length(pointList[ptr0] - pointList[ptr1]) / 2;
	//
	//		distance = 0.0;
	//
	//
	//		// 
	//		// This loop fucks up ptr1, and after the loop ptr1 is back to normal...
	//		// 
	//		//[loop]
	//		while(ptr2 == -1)
	//		{
	//			//buffer.Store2((id - sizeIndices[id * 2]) * 2 * 4, uint2(id - sizeIndices[id * 2], (uint)tri.x - sizeIndices[id * 2]));
	//			////buffer.Store2(((id - sizeIndices[id * 2]) * 4 + 2) * 4, uint2(id, (uint)pointList[ptr1].x, (uint)pointList[ptr1].y));
	//			//return -1;
	//
	//			distance += radius * radiusStep;
	//
	//			maxAngle = getAngle(float2(pointList[ptr0].x + 1000000.f, pointList[ptr0].y), pointList[ptr0], pointList[ptr1]) - PI_0_5;
	//			if(maxAngle - PI_0_5 < 0)
	//				maxAngle += PI_2;
	//
	//			// The line used for defining the circle
	//			l.setWithAngle((pointList[ptr0] + pointList[ptr1]) / 2, maxAngle);
	//			circle.set(l.v0 + (l.v1 * distance), length(pointList[ptr0] - l.v0 - (l.v1 * distance)));
	//
	//			// Reset the line to handle the calculations for deciding points in the loop below
	//			l.v0 = pointList[ptr0];
	//			l.v1 = normalize(pointList[ptr1] - pointList[ptr0]);
	//
	//			// Look for the point that is to the right vector and included in the circle defined outside the loop.
	//			// Among these points, selects the one which minimizes the angle between id, ptr1 and the new point
	//
	//			// Angles received from getAngle method ranges from 0 <= angle < PI * 2
	//			// This means that the first itteration will always trigger the if statement
	//			maxAngle = -PI;
	//
	//			for(i = sizeIndices[id * 2]; i < sizeIndices[id * 2 + 1]; i++)
	//				//for(i = begin; i < end; i++)
	//			{
	//				if(!l.toTheRightOfLine(pointList[i]) || i == ptr0 || i == (uint)ptr1)
	//					continue;
	//
	//				temp++;
	//
	//				if(!circle.inside(pointList[i]))
	//					continue;
	//
	//				// If a thread reaches this point, then there will be no other iteration of the while loop
	//				// The radius varible is therefore not needed anymore, so resuse it and save a register
	//
	//				if(getAngle(pointList[ptr1], pointList[i], pointList[ptr0]) > maxAngle)
	//				{
	//					ptr2 = i;
	//					maxAngle = getAngle(pointList[ptr1], pointList[i], pointList[ptr0]);
	//				}
	//			}
	//			// No points are found that are to the right of the created line, the loop is terminated
	//			if(temp == 0)
	//				break;
	//		}
	//
	//		// If there are no points on the right side of ptr0 -> ptr1, then all triangles has been located
	//		if(temp == 0)
	//			break;
	//	}
	//
	//	// If the connected point is the second point of the neighbour list, then stop and exit
	//	if(!ptr2InList)
	//	{
	//		// Create a new triangle with ptr0, ptr1 and ptr2
	//		temp = triangleCount[id];
	//		//tempList[ptr0].n[temp] = uint3(id, ptr1, ptr2);
	//		tri = uint3(ptr0, ptr1, ptr2);
	//
	//		belongList[ptr0].n[belongCount2[ptr0++]] = tri;
	//		belongList[ptr1].n[belongCount2[ptr1++]] = tri;
	//		belongList[ptr2].n[belongCount2[ptr2++]] = tri;
	//
	//		buffer.Store3((temp * 3 + groupIDSorted[id] * bufferOffset) * 4, tri);
	//
	//		triangleCount[id]++;
	//
	//		if(neighBours[(ptr0 - sizeIndices[id * 2]) * 2] == ptr2)
	//			break;
	//	}
	//
	//	if(neighBours[(ptr0 - sizeIndices[id * 2]) * 2 + 1] == -1)
	//		neighBours[(ptr0 - sizeIndices[id * 2]) * 2 + 1] = ptr2;
	//
	//	nNeighbours++;
	//	ptr1 = ptr2;
	//}

	return isBorderPoint ? neighBours[(ptr0 - sizeIndices[id * 2]) * 2] : -1;
}

[numthreads(1024, 1, 1)]
void main2(uint threadInGroupID : SV_GroupIndex)
{
	for(uint i = 0; i < sizeIndices[threadInGroupID * 2 + 1] - sizeIndices[threadInGroupID * 2]; i++)
	{
		if(triangulateFromPoint2(threadInGroupID, i + sizeIndices[threadInGroupID * 2], (uint)pointList[i + sizeIndices[threadInGroupID * 2]].x % patchDimX == 0 ||
																						(uint)pointList[i + sizeIndices[threadInGroupID * 2]].y % patchDimZ == 0) != -1)
			triangulateFromPoint2(threadInGroupID, i + sizeIndices[threadInGroupID * 2], false);
	}

	//int counter = 0, id;
	//for(int i = 0; i < arraySize; i++)
	//{
	//	id = i + sizeIndices[groupID * 2];
	//	for(uint j = 0; j < triangleCount[i]; j++)
	//	{
	//		buffer.Store3(((j + counter) * 3 + groupIDSorted[groupID] * bufferOffset) * 4, uint3(patchIndices[tempList[id].n[j].x],
	//			patchIndices[tempList[id].n[j].y],
	//			patchIndices[tempList[id].n[j].z]));
	//	}
	//	counter += triangleCount[i];
	//}
}