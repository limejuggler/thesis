//#include "helpFunctions.hlsl"

#define radiusStep 5
// (32 + 1) * (32 + 1) = 1089
// 32 + 1 due to the shared borders
#define arraySize 1089

#define PI 3.14159265359
#define PI_2 6.28318530718
#define PI_0_5 1.57079632679
#define PI_1_5 4.71238898038

// Look into append buffers
#define maxNeighbours 16

cbuffer cbTriangulationConstants : register(b0)
{
	uint	bufferOffset;
	float	patchDimX,
			patchDimZ,
			padding;
};


struct TriangleList
{
	uint3 n[maxNeighbours];
};

groupshared int lastPtr1[arraySize];
groupshared uint triangleCount[arraySize];

// The index buffer
RWByteAddressBuffer buffer : register(u0);

RWStructuredBuffer<float2> pointList : register(u1);

// Start and end id. Two elements for each warp
RWStructuredBuffer<uint> sizeIndices : register(u2);
RWStructuredBuffer<TriangleList> belongList : register(u3);
RWStructuredBuffer<uint> patchIndices : register(u4);

// Save first and second neighbour here instead of variables to save temp registers.
// 2 spots per thread
RWStructuredBuffer<int> neighBours : register(u5);

RWStructuredBuffer<uint> groupIDSorted : register(u6);

// Can only use 8 registers
RWStructuredBuffer<uint> belongCount : register(u7);

struct Circle
{
	float2	center;
	float	radius;

	void set(float2 pos, float r)
	{
		center = pos;
		radius = r;
	}
	void set(float2 p1, float2 p2)
	{
		center = (p1 + p2) / 2;
		radius = length(p1 - p2) / 2;
	}
	bool inside(float2 p)
	{
		return (p.x - center.x) * (p.x - center.x) + (p.y - center.y) * (p.y - center.y) < radius * radius;
	}
};

// Line (actually a vector)
struct Line
{
	float2		v0,
				v1;

	void setWithAngle(float2 p, float angle)
	{
		v0 = p;
		v1 = normalize(float2(p.x + cos(angle), p.y + sin(angle)) - p);
	}

	bool toTheRightOfLine(float2 p)
	{
		if(v1.x == 0.f)
			return	v1.y > 0.f && p.x > v0.x ||
					v1.y <= 0.f && p.x < v0.x;

		float det = (p.y - v0.y) - (v1.x == 0.f ? -99999999.f : v1.y / v1.x) * (p.x - v0.x);

		return		v1.x > 0.f && det < 0.f ||
					v1.x <= 0.f && det > 0.f;
	}
};

float getAngle(float2 p0, float2 p1, float2 p2)
{
	float angle;

	// p0 and p1
	if(p0.x == p1.x)
		angle = p1.y < p0.y ? PI_1_5 : PI_0_5;
	else
	{
		angle = atan((p1.y - p0.y) / (p1.x - p0.x));

		if(p0.x > p1.x)			angle += PI;
		else if(p0.y > p1.y)	angle += PI_2;
	}

	// p1 and p2
	if(p1.x == p2.x)
		angle = (p2.y < p1.y ? PI_1_5 : PI_0_5) - angle + PI;
	else
	{
		angle = atan((p2.y - p1.y) / (p2.x - p1.x)) - angle + PI;

		if(p1.x > p2.x)			angle += PI;
		else if(p1.y > p2.y)	angle += PI_2;
	}

	// Make sure the angle lies between 0 and 2 pi
	while(angle >= PI_2)		angle -= PI_2;
	while(angle < 0)			angle += PI_2;

	return angle;
}

int triangulateFromPoint(uint id, uint ptr0, bool isBorderPoint)
{
	int ptr2, temp;

	uint nNeighbours, i, ptr1;
	bool ptr2InList;
	float maxAngle, radius, distance;
	uint3 tri;

	Circle circle;
	Line l;

	belongCount[ptr0] = 0;
	triangleCount[id] = 0;

	maxAngle = 1.0e+10;

	// These conditions has to be stored in variables and not specified in the loop declaration, or else
	// the aggressive optimization distorts the result and (rather ironically) increases computation time
	tri.x = lastPtr1[id] == -1 ? sizeIndices[id * 2] : (uint)lastPtr1[id] < ptr0 ? ptr0 : sizeIndices[id * 2];
	tri.y = lastPtr1[id] == -1 ? sizeIndices[id * 2 + 1] : (uint)lastPtr1[id] > ptr0 ? ptr0 : sizeIndices[id * 2 + 1];

	for(i = tri.x; i < tri.y; i++)
	{
		//if(type == PointType::frame)
		//if(isBorderPoint)
		//{
		//	if(id == sizeIndices[id * 2] || id == sizeIndices[id * 2 + 1] - 1)
		//	{
		//		if(pointList[i].y != pointList[id].y)
		//			continue;
		//	}
		//	else if(pointList[i].y != pointList[id].y && (pointList[id].y != pointList[id + 1].y || pointList[id].y != pointList[id - 1].y))
		//	{
		//		if(pointList[i].x != pointList[id].x)
		//			continue;
		//	}
		//	else
		//	{
		//		if(pointList[i].y != pointList[id].y)
		//			continue;
		//	}
		//}
		// The values dealt with should not be big enough to require square root to ensure correct result
		radius =	(pointList[i].x - pointList[ptr0].x) * (pointList[i].x - pointList[ptr0].x) +
					(pointList[i].y - pointList[ptr0].y) * (pointList[i].y - pointList[ptr0].y);
		
		// If it is a border point, it requires a slightly different condition to be able to find its neighbours
		if(isBorderPoint && radius < maxAngle || !isBorderPoint && radius <= maxAngle)
		{
			// Will only be triggered once per thread and should not be examined during each iteration
			if(i == ptr0)
				continue;
		
			maxAngle = radius;
			ptr1 = i;
		}
	}

	neighBours[ptr0 * 2] = ptr1;
	neighBours[ptr0 * 2 + 1] = -1;
	
	nNeighbours = 1;

	while(nNeighbours < maxNeighbours)
	{
		// Identify the triangle in which id is a vertex and is close to ptr1.
		// If the point is not in the neighbour list to id, and if it is also
		// on the right side of the two other points, the point is decided to
		// be the final point in the triangle.
	
		ptr2 = -1;
		ptr2InList = false;
		for(i = 0; i < belongCount[ptr1]; i++)
		{
			// temp is used in several areas to reduce the number of registers being used
			temp = -1;
		
			tri = belongList[ptr1].n[i];
		
			if(tri.x == ptr0)
			{
				if(		tri.y == ptr1)		temp = (int)tri.z;
				else if(tri.z == ptr1)		temp = (int)tri.y;
			}
			else if(tri.y == ptr0)
			{
				if(		tri.x == ptr1)		temp = (int)tri.z;
				else if(tri.z == ptr1)		temp = (int)tri.x;
			}
			else if(tri.z == ptr0)
			{
				if(		tri.x == ptr1)		temp = (int)tri.y;
				else if(tri.y == ptr1)		temp = (int)tri.x;
			}
		
			if(temp == -1)
				continue;
		
			if(getAngle(pointList[ptr0], pointList[ptr1], pointList[temp]) < PI)
			{
				ptr2InList = true;
				ptr2 = temp;
				break;
			}
		}
	
		// If no point was located during the previous step, find it using geometrical conditions
		// Define a circle that runs through id and ptr1 and find the third point to complete the circle
		if(ptr2 == -1)
		{
			temp = 0;
		
			radius = length(pointList[ptr0] - pointList[ptr1]) / 2;
		
			distance = 0.0;
		
		
			// 
			// This loop fucks up ptr1, and after the loop ptr1 is back to normal...
			// 
			//[loop]
			while(ptr2 == -1)
			{
				//buffer.Store2((id - sizeIndices[id * 2]) * 2 * 4, uint2(id - sizeIndices[id * 2], (uint)tri.x - sizeIndices[id * 2]));
				////buffer.Store2(((id - sizeIndices[id * 2]) * 4 + 2) * 4, uint2(id, (uint)pointList[ptr1].x, (uint)pointList[ptr1].y));
				//return -1;
		
				distance += radius * radiusStep;
		
				maxAngle = getAngle(float2(pointList[ptr0].x + 1000000.f, pointList[ptr0].y), pointList[ptr0], pointList[ptr1]) - PI_0_5;
				if(maxAngle - PI_0_5 < 0)
					maxAngle += PI_2;
		
				// The line used for defining the circle
				l.setWithAngle((pointList[ptr0] + pointList[ptr1]) / 2, maxAngle);
				circle.set(l.v0 + (l.v1 * distance), length(pointList[ptr0] - l.v0 - (l.v1 * distance)));
		
				// Reset the line to handle the calculations for deciding points in the loop below
				l.v0 = pointList[ptr0];
				l.v1 = normalize(pointList[ptr1] - pointList[ptr0]);
		
				// Look for the point that is to the right vector and included in the circle defined outside the loop.
				// Among these points, selects the one which minimizes the angle between id, ptr1 and the new point
		
				// Angles received from getAngle method ranges from 0 <= angle < PI * 2
				// This means that the first itteration to pass the conditions will always trigger the if statement
				maxAngle = -PI;
		
				for(i = sizeIndices[id * 2]; i < sizeIndices[id * 2 + 1]; i++)
				{
					if(!l.toTheRightOfLine(pointList[i]) || i == ptr0 || i == ptr1)
						continue;
				
					temp++;
				
					if(!circle.inside(pointList[i]))
						continue;
				
					// If a thread reaches this point, then there will be no other iteration of the while loop
					// The radius varible is therefore not needed anymore, so resuse it and save a register
				
					if(getAngle(pointList[ptr1], pointList[i], pointList[ptr0]) > maxAngle)
					{
						ptr2 = i;
						maxAngle = getAngle(pointList[ptr1], pointList[i], pointList[ptr0]);
					}
				}
				// No points are found that are to the right of the created line, the loop is terminated
				if(temp == 0)
					break;
			}
		
			// If there are no points on the right side of ptr0 -> ptr1, then all triangles has been located
			if(temp == 0)
				break;
		}
	
		// If the connected point is the second point of the neighbour list, then stop and exit
		if(!ptr2InList)
		{
			// Create a new triangle with ptr0, ptr1 and ptr2
			temp = triangleCount[id];
			//tempList[ptr0].n[temp] = uint3(id, ptr1, ptr2);
			tri = uint3(ptr0, ptr1, ptr2);

			belongList[ptr0].n[belongCount[ptr0]++] = tri;
			belongList[ptr1].n[belongCount[ptr1]++] = tri;
			belongList[ptr2].n[belongCount[ptr2]++] = tri;

			//buffer.Store3((temp * 3 + groupIDSorted[id] * bufferOffset) * 4, uint3(patchIndices[tri.x], patchIndices[tri.y], patchIndices[tri.z]));
		
			triangleCount[id]++;
		
			if(neighBours[ptr0 * 2] == ptr2)
				break;
		}
	
		if(neighBours[ptr0 * 2 + 1] == -1)
			neighBours[ptr0 * 2 + 1] = ptr2;
	
		nNeighbours++;
		ptr1 = ptr2;
	}

	return isBorderPoint ? neighBours[ptr0 * 2] : -1;
}

[numthreads(8, 1, 1)]
void main(uint threadInGroupID : SV_GroupIndex)
{
	lastPtr1[threadInGroupID] = -1;
	for(uint i = 0; i < sizeIndices[threadInGroupID * 2 + 1] - sizeIndices[threadInGroupID * 2]; i++)
	{
		lastPtr1[threadInGroupID] = triangulateFromPoint(threadInGroupID, i + sizeIndices[threadInGroupID * 2],	(uint)pointList[i + sizeIndices[threadInGroupID * 2]].x % patchDimX == 0 ||
																												(uint)pointList[i + sizeIndices[threadInGroupID * 2]].y % patchDimZ == 0);
		if(lastPtr1[threadInGroupID] != -1)
			triangulateFromPoint(threadInGroupID, i + sizeIndices[threadInGroupID * 2], false);
	}

	int counter = 0, id;
	for(int i = sizeIndices[threadInGroupID * 2]; i < sizeIndices[threadInGroupID * 2 + 1]; i++)
	{
		//id = i + sizeIndices[threadInGroupID * 2];
		for(uint j = 0; j < belongCount[i]; j++)
		{
			buffer.Store3(((j + counter) * 3 + /*groupIDSorted[*/threadInGroupID/*]*/ * bufferOffset) * 4, uint3(patchIndices[belongList[i].n[j].x],
				patchIndices[belongList[i].n[j].y],
				patchIndices[belongList[i].n[j].z]));
		}
		counter += belongCount[i];
	}
}