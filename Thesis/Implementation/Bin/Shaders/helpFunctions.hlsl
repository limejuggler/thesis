#define PI 3.14159265359
#define PI_2 6.28318530718
#define PI_0_5 1.57079632679
#define PI_1_5 4.71238898038

#define maxNeighbours 16

struct Triangle
{
	int p1;
	int p2;
	int p3;

	void set(int point1, int point2, int point3) { p1 = point1; p2 = point2; p3 = point3; }
};

struct TriangleList
{
	Triangle n[maxNeighbours];
};

struct Circle
{
	float2	center;
	float	radius;

	void set(float2 pos, float r)
	{
		center = pos;
		radius = r;
	}
	void set(float2 p1, float2 p2)
	{
		center = (p1 + p2) / 2;
		radius = length(p1 - p2) / 2;
	}
	bool inside(float2 p)
	{
		return (p.x - center.x) * (p.x - center.x) + (p.y - center.y) * (p.y - center.y) < radius * radius;
	}	
};

// Line (actually a vector)
struct Line
{
	float		gg_ff;
	float2		v0,
				v1;

	void setWithAngle(float2 p, float angle)
	{
		v0 = p;
		v1 = normalize(float2(p.x + cos(angle), p.y + sin(angle)) - p);

		gg_ff = v1.x == 0.f ? -99999999.f : v1.y / v1.x;
	}

	void set(float2 plane1, float2 plane2)
	{
		v0 = plane1;
		v1 = normalize(plane2 - plane1);

		gg_ff = v1.x == 0.f ? -99999999.f : v1.y / v1.x;
	}

	bool toTheRightOfLine(float2 p)
	{
		if(v1.x == 0.f)
			return	v1.y > 0.f && p.x > v0.x ||
					v1.y <= 0.f && p.x < v0.x;

		float det = (p.y - v0.y) - gg_ff * (p.x - v0.x);

		return		v1.x > 0.f && det < 0.f ||
					v1.x <= 0.f && det > 0.f;
	}
};

float getAngle(float2 p0, float2 p1, float2 p2)
{
	float angle;

	// p0 and p1
	if(p0.x == p1.x)
		angle = p1.y < p0.y ? PI_1_5 : PI_0_5;
	else
	{
		angle = atan((p1.y - p0.y) / (p1.x - p0.x));

		if(p0.x > p1.x)			angle += PI;
		else if(p0.y > p1.y)	angle += PI_2;
	}

	// p1 and p2
	if(p1.x == p2.x)
		angle = (p2.y < p1.y ? PI_1_5 : PI_0_5) - angle + PI;
	else
	{
		angle = atan((p2.y - p1.y) / (p2.x - p1.x)) - angle + PI;

		if(p1.x > p2.x)			angle += PI;
		else if(p1.y > p2.y)	angle += PI_2;
	}

	// Make sure the angle lies between 0 and 2 pi
	while(angle >= PI_2)		angle -= PI_2;
	while(angle < 0)			angle += PI_2;

	return angle;
}

int findClosestSortedPoint(int id, StructuredBuffer<float2> points, int begin, int end)
{
	unsigned int closestID = begin;

	double closestDist = 1.0e+10, dist;

	for(int i = begin; i < end; i++)
	{
		// The values dealt with should not be big enough to require square root to ensure correct result
		dist =	(points[i].x - points[id].x) * (points[i].x - points[id].x) +
				(points[i].y - points[id].y) * (points[i].y - points[id].y);
		if(dist < closestDist)
		{
			// Will only be triggered once per thread and should not be examined during each iteration
			if(i == id)
				continue;

			closestDist = dist;
			closestID = i;
		}
	}

	return closestID;
}