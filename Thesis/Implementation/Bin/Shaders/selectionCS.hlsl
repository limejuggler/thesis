#define reducBorder 0.75
#define reducNormal 0.875

cbuffer cbVariables : register(b1)
{
	uint width;
	uint height;
	uint nSplitsX;
	uint nSplitsZ;
	uint chunkX;
	uint chunkZ;

	float pointStep;
};

groupshared uint groupID;

// Fill the indices list and 2D pointList here, use in triangulation step
RWStructuredBuffer<float2> pointList : register(u1);
RWStructuredBuffer<uint> sizeIndices : register(u2);

// The vertex buffer
//RWByteAddressBuffer buffer : register(u6);

// Height data from some source (heightmap for instance)
RWStructuredBuffer<float> points : register(u6);
RWStructuredBuffer<float> pixelErrors : register(u7);
RWStructuredBuffer<int> selectedIndices : register(u8);

void pointSelection(uint x, uint z)
{
	uint i, j, k, prevID[3];
	float heightDiff[3], prevHeightDiff[3];

	for(i = z * (chunkZ + 1); i < (z + 1) * (chunkZ + 1); i++)
	{
		for(j = x * (chunkX + 1); j < (x + 1) * (chunkX + 1); j++)
		{
			selectedIndices[i * (width + nSplitsX) + j] = -1;
		}
	}

	// TODO: test whether or not the average pixel error should be used, or the maximum of the two
	// This only applies for border points

	float condUp = z < nSplitsZ - 1 ? (pixelErrors[(z + 1) * nSplitsX + x] + pixelErrors[z * nSplitsX + x]) / 2 : pixelErrors[z * nSplitsX + x];
	float condDown = z > 0 ? (pixelErrors[(z - 1) * nSplitsX + x] + pixelErrors[z * nSplitsX + x]) / 2 : pixelErrors[z * nSplitsX + x];
	float condLeft = x > 0 ? (pixelErrors[z * nSplitsX + x - 1] + pixelErrors[z * nSplitsX + x]) / 2 : pixelErrors[z * nSplitsX + x];
	float condRight = x < nSplitsX - 1 ? (pixelErrors[z * nSplitsX + x + 1] + pixelErrors[z * nSplitsX + x]) / 2 : pixelErrors[z * nSplitsX + x];
	

	/****************************************************
						Bottom frame
	****************************************************/
	// Lower left edge point
	prevID[1] = prevID[0] = z * chunkZ * width + x * chunkX;
	selectedIndices[z * (width + nSplitsX) + x * (chunkX + 1)] = z * chunkZ * width + x * chunkX;

	prevHeightDiff[0] = (points[z * width + x * chunkX] - points[z * width + x * chunkX + 1]) / pointStep;
	//for(i = z * (width + nSplitsX) + x * (chunkX + 1) + 2; i < (x + 1) * (chunkX + 1); i++)
	for(i = x * chunkX + 2; i < (x + 1) * chunkX; i++)
	{
		heightDiff[0] = (points[z * chunkZ * width + i] - points[z * chunkZ * width + i - 1]) / pointStep;
		if(	abs(heightDiff[0] - prevHeightDiff[0]) > condDown || 
			abs(heightDiff[0] - (points[z * chunkZ * width + i] - points[prevID[0]])) / 
			((z * chunkZ * width + i - prevID[0]) * pointStep * pow(reducBorder, z * chunkZ * width + i - prevID[0])) > condDown)
		{
			prevID[0] = z * chunkZ * width + i - 1;
			selectedIndices[z * (chunkZ + 1) * (width + nSplitsX) + x * (chunkX + 1) + i - x * chunkX - 2] = prevID[0];
		}

		prevHeightDiff[0] = heightDiff[0];
	}

	// Lower right edge point
	selectedIndices[z * (chunkZ + 1) * (width + nSplitsX) + (x + 1) * (chunkX + 1) - 1] = z * chunkZ * width + (x + 1) * chunkX - 1;
	/*-------------------------------------------------*/

	prevID[2] = z * chunkZ * width + (x + 1) * chunkX - 1;
	// Previous point is the upper left edge point
	prevHeightDiff[1] = (points[(z + 1) * width + x * chunkX] - points[z * width + x * chunkX]) / pointStep;
	for(i = z * (chunkZ + 1) + 2; i < (z + 1) * (chunkZ + 1); i++)
	{
		/****************************************************
							Left frame
		****************************************************/
		heightDiff[1] = (points[i * width + x * chunkX] - points[(i - 1) * width + x * chunkX]) / pointStep;
		if(	abs(heightDiff[1] - prevHeightDiff[1]) > condLeft ||
			abs(heightDiff[1] - (points[i * width + x * chunkX] - points[prevID[1]])) / 
			((i * width + x * chunkX - prevID[1]) * pointStep * pow(reducBorder, i * width + x * chunkX - prevID[1])) > condLeft)
		{
			prevID[1] = (i - 1) * width + x * chunkX;
			selectedIndices[(z * chunkZ + i - 1 - z * (chunkZ + 1) - 2) * width + x * (chunkX + 1)] = prevID[1];
		}

		prevHeightDiff[1] = heightDiff[1];
		/*-------------------------------------------------*/

		/****************************************************
				All points not part of the frame
		****************************************************/
		for(j = x * (chunkX + 1) + 2; j < (x + 1) * (chunkX + 1); j++)
		{
			for(k = (z * (chunkZ + 1) + i - z * (chunkZ + 1) - 2) * (width + nSplitsX) + x * (chunkX); k > z * (chunkZ + 1) * width + x * (chunkX + 1); k--)
			{
				if(selectedIndices[k] == -1)
					continue;
			//	if(points[currentPatch[k]].x == xPoint)
			//		continue;
			//
			//	p1 = points[currentPatch[k]].x - xPoint;
			//	p2 = points[currentPatch[k]].z - zPoint;
				// Don't use square root, as it is a slow operation (around 30% increase in computation time (debug))

			//	dist = p1 * p1 + p2 * p2;
			//	if(dist <= closestDist)
			//	{
			//		//if(dist == closestDist && abs(p2 - points[closestPointID1].y) > abs(p2 - points[k].y))
			//		//	closestPointID1 = k;
			//		//else if(dist < closestDist)
			//		//	closestPointID1 = k;
			//		dist < closestDist ? closestPointID1 = k : closestPointID2 = k;
			//
			//		closestDist = dist;
			//	}
			//
			//	// Problem identified: loop breaks too early, thereby returning the wrong point
			//
			//	// closestDist * closestDist seams to take care of the problem, but it is not an optimal solution
			//	if(dist > closestDist * closestDist)
			//		break;
			}
		}
		/*-------------------------------------------------*/

		/****************************************************
							Right frame
		****************************************************/
		heightDiff[2] = (points[i * width + (x + 1) * chunkX - 1] - points[(i - 1) * width + (x + 1) * chunkX - 1]) / pointStep;
		if(	abs(heightDiff[2] - prevHeightDiff[2]) > condRight ||
			abs(heightDiff[2] - (points[i * width + (x + 1) * chunkX - 1] - points[prevID[2]])) /
			((i * width + (x + 1) * chunkX - 1 - prevID[2]) * pointStep * pow(reducBorder, i * width + (x + 1) * chunkX - 1 - prevID[2])) > condRight)
		{
			prevID[2] = (i - 1) * width + (x + 1) * chunkX - 1;
			selectedIndices[(z * chunkZ + i - 1 - z * (chunkZ + 1) - 2) * width + (x + 1) * (chunkX + 1) - 1] = prevID[2];
		}

		prevHeightDiff[2] = heightDiff[2];
		/*-------------------------------------------------*/
	}

	/****************************************************
						Top frame
	****************************************************/
	// Upper left edge point
	prevID[0] = (z + 1) * chunkZ * width - width + x * chunkX;
	selectedIndices[(z + 1) * (chunkZ + 1) * (width + nSplitsX) - width - nSplitsX + x * (chunkX + 1)] = z * chunkZ * width + x * chunkX;

	prevHeightDiff[0] = (points[z * width + x * chunkX] - points[z * width + x * chunkX + 1]) / pointStep;
	for(i = x * chunkX + 2; i < (x + 1) * chunkX; i++)
	{
		heightDiff[0] = (points[(z + 1) * chunkZ * width - width + i] - points[(z + 1) * chunkZ * width - width + i - 1]) / pointStep;
		if(	abs(heightDiff[0] - prevHeightDiff[0]) > condUp ||
			abs(heightDiff[0] - (points[z * chunkZ * width + i] - points[prevID[0]])) /
			(((z + 1) * chunkZ * width - width + i - prevID[0]) * pointStep * pow(reducBorder, (z + 1) * chunkZ * width - width + i - prevID[0])) > condUp)
		{
			prevID[0] = (z + 1) * chunkZ * width - width + i - 1;
			selectedIndices[z * (chunkZ + 1) * (width + nSplitsX) + x * (chunkX + 1) + i - x * chunkX - 2] = prevID[0];
		}

		prevHeightDiff[0] = heightDiff[0];
	}

	// Upper right edge point
	selectedIndices[(z + 1) * (chunkZ + 1) * (width + nSplitsX) - width - nSplitsX + (x + 1) * (chunkX + 1) - 1] = width * height - 1;
	/*-------------------------------------------------*/
}

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID, uint groupIDReceived : SV_GroupID)
{
	if(threadID.x == 0 && threadID.y == 0)
		groupID = groupIDReceived;

	if(threadID.x < nSplitsX && threadID.y < nSplitsZ)
		pointSelection(threadID.x, threadID.y);
}