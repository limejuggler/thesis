Texture2D txTexture1	: register(t0);
Texture2D txTexture2	: register(t1);
Texture2D txTexture3	: register(t2);
Texture2D txBlendTer	: register(t3);

SamplerState samLinear	: register(s0);

cbuffer cbColour : register(b0)
{
	float3 col;
};

struct PS_INPUT
{
	float4 position : SV_POSITION;
	float2 tex		: TEXCOORD0;
	float2 texBlend : TEXCOORD1;
	float3 lightInt : LIGHT;
};

float4 main(PS_INPUT input) : SV_TARGET0
{
	//float4 blendTexCol = txBlendTer.Sample(samLinear, input.texBlend);
	//float inverseTotal = 1.f / (blendTexCol.x + blendTexCol.y + blendTexCol.z);
	//
	//float4 terCol0 = txTexture1.Sample(samLinear, input.tex);
	//float4 terCol1 = txTexture2.Sample(samLinear, input.tex);
	//float4 terCol2 = txTexture3.Sample(samLinear, input.tex);
	//
	//terCol0 *= blendTexCol.x * inverseTotal;
	//terCol1 *= blendTexCol.y * inverseTotal;
	//terCol2 *= blendTexCol.z * inverseTotal;
	//
	//return float4((terCol0 + terCol1 + terCol2).xyz * input.lightInt, 1);
	return float4(txBlendTer.Sample(samLinear, input.texBlend).xyz * input.lightInt, 1);
}

float4 patchColours(PS_INPUT input) : SV_TARGET0
{
	return float4(col * input.lightInt, 1);
}