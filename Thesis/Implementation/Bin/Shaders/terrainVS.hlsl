cbuffer cbOnce : register(b0)
{
	float4x4 projection;
};

cbuffer cbOnMove : register(b1)
{
	float4x4 view;
	float4 cameraPos;
};

struct VS_Input
{
	float3 position : POSITION;
	float3 normal	: NORMAL;
};

struct VS_Output
{
	float4 position : SV_POSITION;
	float2 tex		: TEXCOORD0;
	float2 texBlend : TEXCOORD1;
	float3 lightInt : LIGHT;
};

VS_Output main(VS_Input input)
{
	VS_Output output = (VS_Output)0;

	output.position = mul(projection, mul(view, float4(input.position, 1.0)));

	float3 s = normalize(float3(5, 2, 10));
	float3 tnorm = input.normal;
	output.lightInt = float3(0.9, 0.9, 0.9) * max(dot(s, tnorm), 0.0) + float3(0.65, 0.65, 0.65);

	float2 tile;
	tile.x = (input.position.x + 8 * pointStep * 1.5) / (8 * pointStep * 1.5);
	tile.y = (input.position.z - 8 * pointStep * 1.5) / (-8 * pointStep * 1.5);
	output.tex = tile;
	
	// Send in width and height, as well as the point step
	output.texBlend.x = (input.position.x + width * pointStep) / (width * pointStep);
	output.texBlend.y = 1 - ((input.position.z - width * pointStep) / (-width * pointStep));

	return output;
}