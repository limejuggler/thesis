#define arraySize 4225

#include "CPUTriangulation.h"

struct Circle
{
	elm::vec2	center;
	float 		radius;

	void set(elm::vec2 pos, float r)
	{
		center = pos;
		radius = r;
	}
	void set(elm::vec2 p1, elm::vec2 p2)
	{
		center = (p1 + p2) / 2;
		radius = vecLength(p1 - p2) / 2;
	}
	bool inside(elm::vec2 p)
	{
		return (p.x - center.x) * (p.x - center.x) + (p.y - center.y) * (p.y - center.y) < radius * radius;
	}
};

struct Line
{
	float		gg_ff;
	elm::vec2	v0,
				v1;

	void setWithAngle(elm::vec2 p, float angle)
	{
		v0 = p;
		v1 = normalize(elm::vec2(p.x + cos(angle), p.y + sin(angle)) - p);

		gg_ff = v1.x == 0.f ? -99999999.f : v1.y / v1.x;
	}
	void set(elm::vec2 plane1, elm::vec2 plane2)
	{
		v0 = plane1;
		v1 = normalize(plane2 - plane1);

		gg_ff = v1.x == 0.f ? -99999999.f : v1.y / v1.x;
	}
	bool toTheRightOfLine(const elm::vec2 &p)
	{
		if(v1.x == 0.f)
			return	v1.y > 0.f && p.x > v0.x ||
					v1.y <= 0.f && p.x < v0.x;

		float det = (p.y - v0.y) - gg_ff * (p.x - v0.x);

		return		v1.x > 0.f && det < 0.f ||
					v1.x <= 0.f && det > 0.f;
	}
};

float getAngle(elm::vec2 p0, elm::vec2 p1, elm::vec2 p2)
{
	float angle;

	// p0 and p1
	if(p0.x == p1.x)
		angle = p1.y < p0.y ? ELM_PI * 1.5f : ELM_PI * 0.5f;
	else
	{
		angle = atan((p1.y - p0.y) / (p1.x - p0.x));

		if(p0.x > p1.x)			angle += ELM_PI;
		else if(p0.y > p1.y)	angle += ELM_PI * 2;
	}

	// p1 and p2
	if(p1.x == p2.x)
		angle = (p2.y < p1.y ? ELM_PI * 1.5f : ELM_PI * 0.5f) - angle + ELM_PI;
	else
	{
		angle = atan((p2.y - p1.y) / (p2.x - p1.x)) - angle + ELM_PI;

		if(p1.x > p2.x)			angle += ELM_PI;
		else if(p1.y > p2.y)	angle += ELM_PI * 2;
	}

	// Make sure the angle lies between 0 and 2 pi
	while(angle >= ELM_PI * 2)	angle -= ELM_PI * 2;
	while(angle < 0)			angle += ELM_PI * 2;

	return angle;
}

void triangulate(int id, const std::vector<elm::vec2> &pointList, uint begin, uint end, std::vector<uint> &triVector, std::vector<std::vector<int>> &belongList, bool isBorderPoint, int lastPtr1)
{
	int nNeibours, ptr0, ptr1, ptr2, temp, neighbour1, neighbour2, nRight;
	bool ptr2InList;
	float maxAngle, radiusStep, radius, distance, angle;

	int tri[3];

	int pointsToTheRight[arraySize];

	Circle	circle;
	Line tempLine, line;

	ptr0 = id;

	double closestDist = 1.0e+10, dist;
	float xPoint = pointList[id].x;
	float yPoint = pointList[id].y;

	uint startCond = lastPtr1 == -1 ? begin : lastPtr1 < id ? id : begin;
	uint endCond = lastPtr1 == -1 ? end : lastPtr1 > id ? id : end;
	for(uint i = startCond; i < endCond; i++)
	{
		//if(type == PointType::frame)
		//if(isBorderPoint)
		//{
		//	if(id == begin || id == end - 1)
		//	{
		//		if(pointList[i].y != yPoint)
		//			continue;
		//	}
		//	else if(pointList[i].y != yPoint && (pointList[id].y != pointList[id + 1].y || pointList[id].y != pointList[id - 1].y))
		//	{
		//		if(pointList[i].x != xPoint)
		//			continue;
		//	}
		//	else
		//	{
		//		if(pointList[i].y != yPoint)
		//			continue;
		//	}
		//}
		// The values dealt with should not be big enough to require square root to ensure correct result
		dist =	(pointList[i].x - xPoint) * (pointList[i].x - xPoint) +
				(pointList[i].y - yPoint) * (pointList[i].y - yPoint);

		// If it is a border point, it requires a slightly different condition to be able to find its neighbours
		if(isBorderPoint && dist < closestDist || !isBorderPoint && dist <= closestDist)
		{
			// Will only be triggered once per thread and should not be examined during each iteration
			if(i == id)
				continue;

			closestDist = dist;
			ptr1 = i;
		}
	}

	neighbour1 = ptr1;
	neighbour2 = -1;

	nNeibours = 1;
	radiusStep = 5.f;

	while(nNeibours < maxNeighbours)
	{
		ptr2 = -1;
		ptr2InList = false;
		for(uint i = 0; i < belongList[ptr1 - begin].size(); i++)
		{
			temp = -1;

			tri[0] = triVector[belongList[ptr1 - begin][i]];
			tri[1] = triVector[belongList[ptr1 - begin][i] + 1];
			tri[2] = triVector[belongList[ptr1 - begin][i] + 2];

			if(tri[0] == id)
			{
				if(		(int)tri[1] == ptr1)	temp = (int)tri[2];
				else if((int)tri[2] == ptr1)	temp = (int)tri[1];
			}
			else if(tri[1] == id)
			{
				if(		(int)tri[0] == ptr1)	temp = (int)tri[2];
				else if((int)tri[2] == ptr1)	temp = (int)tri[0];
			}
			else if(tri[2] == id)
			{
				if(		(int)tri[0] == ptr1)	temp = (int)tri[1];
				else if((int)tri[1] == ptr1)	temp = (int)tri[0];
			}

			if(temp == -1)
				continue;

			if(getAngle(pointList[id], pointList[ptr1], pointList[temp]) < ELM_PI)
			{
				ptr2InList = true;
				ptr2 = temp;
				break;
			}
		}

		// If no point was located during the previous step, find it using geometrical conditions
		// Define a circle that runs through id and ptr1 and find the third point to complete the circle
		if(ptr2 == -1)
		{
			nRight = 0;
			distance = 0.f;

			radius = elm::vecLength(pointList[id] - pointList[ptr1]) / 2;

			line.set(pointList[id], pointList[ptr1]);
			
			uint _begin = _min((uint)ptr1, id);
			_begin = _min(_begin, (uint)_max((int)_begin - 32, begin));
			for(uint i = _begin; i < end; i++)
			{
				if(line.toTheRightOfLine(pointList[i]))
				{
					if(i == id || i == (uint)ptr1)
						continue;

					pointsToTheRight[nRight++] = i;
				}
			}

			if(nRight == 0)
				break;

			while(ptr2 == -1)
			{
				distance += radius * radiusStep;

				// The line used for defining the circle
				angle = getAngle(elm::vec2(pointList[id].x + 1000000.f, pointList[id].y), pointList[id], pointList[ptr1]);
				if(angle - ELM_PI * 0.5f < 0)
					angle += ELM_PI * 2;
				tempLine.setWithAngle((pointList[id] + pointList[ptr1]) / 2, angle - ELM_PI * 0.5f);
				circle.set(tempLine.v0 + (tempLine.v1 * distance), elm::vecLength(pointList[id] - tempLine.v0 - (tempLine.v1 * distance)));

				// Look for the point that is to the right vector and included in the circle defined outside the loop.
				// Among these points, selects the one which minimizes the angle between id, ptr1 and the new point

				// Angles received from getAngle method ranges from 0 <= angle < PI * 2
				// This means that the first itteration will always trigger the if statement
				maxAngle = -ELM_PI;

				for(int i = 0; i < nRight; i++)
				{
					// Can be commented out, but will cause an increased execution time (but will not affect result)
					if(!circle.inside(pointList[pointsToTheRight[i]]))
						continue;

					radius = getAngle(pointList[ptr1], pointList[pointsToTheRight[i]], pointList[id]);
					//if((radius > maxAngle && isBorderPoint) || (radius > maxAngle && !isBorderPoint))
					if(radius > maxAngle)
					{
						ptr2 = pointsToTheRight[i];
						maxAngle = radius;
					}
				}
			}
		}

		if(!ptr2InList)
		{
			uint triangleID = triVector.size();
			triVector.push_back(ptr0);
			triVector.push_back(ptr1);
			triVector.push_back(ptr2);

			belongList[ptr0 - begin].push_back(triangleID);
			belongList[ptr1 - begin].push_back(triangleID);
			belongList[ptr2 - begin].push_back(triangleID);

			if(neighbour1 == ptr2)
				break;
		}

		if(neighbour2 == -1)
			neighbour2 = ptr2;

		nNeibours++;
		ptr1 = ptr2;
	}

	
	if(isBorderPoint && lastPtr1 == -1 && id != end - 1 && id != begin)
		triangulate(id, pointList, begin, end, triVector, belongList, true, neighbour1);
}