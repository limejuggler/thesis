#ifndef _CPU_TRIANGULATION_
#define _CPU_TRIANGULATION_

#include "settings.h"

void triangulate(int id, const std::vector<elm::vec2> &pointList, uint begin, uint end, std::vector<uint> &triVector, std::vector<std::vector<int>> &belongList, bool isBorderPoint, int lastPtr1 = -1);
//void triangulate2(int id, const std::vector<elm::vec2> &pointList, uint begin, uint end, std::vector<int> &triVector, std::vector<std::vector<int>> &belongList);

#endif