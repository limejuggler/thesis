#include <sstream>

#include "camera.h"
#include "settings.h"
#include "graphicsDX11.h"

CBOnMove cb;
Camera::Camera()
{
	vEye		= elm::vec3(4256, 857, 2925);
	//vEye		= elm::vec3(2500, 200, 2500);
	vLookAt		= elm::vec3(0, 0, 1);
	vUp			= elm::vec3(0, 1, 0);
	vRight		= elm::vec3(1, 0, 0);

	//yawAngle = pitchAngle = 0.f;
	pitchAngle = -1.08f;
	yawAngle = -3.61f;
	//pitchAngle = -0.698131800f;

	cameraSate = 0;

	forwards	= Key(0x57);
	backwards	= Key(0x53);
	left		= Key(0x41);
	right		= Key(0x44);
	boost		= Key(0x10);
	state		= Key(0x43);

	rotateCamera(0, 0);

	elm::lookAtLH(mView, vLookAt, vUp, vEye);

	elm::perspectiveFovLH(mProj, ELM_PI * .3f, (float)screenWidth / screenHeight, 0.01f, 10000);
	//mProj = transpose(mProj);

	frustum = elm::buildViewFrustum(mView, mProj);

	CBOnce cbOnce;
	cbOnce.projection = mProj;

	cb.cameraPos = elm::vec4(vEye, 1);
	cb.view = mView;

	GraphicsDX11::getInstance()->updateCBuffer(&cb);
	GraphicsDX11::getInstance()->setCBufferOnce(&cbOnce);
}

void Camera::setMouseLock(int screenLockX, int screenLockY)
{
	mouseX = screenLockX + screenWidth / 2;
	mouseY = screenLockY + screenHeight / 2;

	SetCursorPos(mouseX, mouseY);
}

void Camera::followTerrain(elm::vec2 theoreticalMove)
{
	Settings *settings = Settings::getInstance();

	if(theoreticalMove.x > settings->width * settings->pointStep)
		theoreticalMove.x = settings->width * settings->pointStep;
	else if(theoreticalMove.x < 0)
		theoreticalMove.x = 0;
	if(theoreticalMove.y > settings->height * settings->pointStep)
		theoreticalMove.y = settings->height * settings->pointStep;
	else if(theoreticalMove.y < 0)
		theoreticalMove.y = 0;

	uint width = settings->width;
	float pointStep = settings->pointStep;

	// Index of the point closest to the player
	int index = (int)(theoreticalMove.y / pointStep) * width + (int)(theoreticalMove.x / pointStep);

	elm::vec2 point1 = elm::vec2(	(int)(theoreticalMove.y / pointStep) * width + pointStep, 
									(float)(int)(theoreticalMove.x / pointStep));
	elm::vec2 point2 = elm::vec2(point1.x - pointStep, point1.y + pointStep);

	float terrainY;
	float xmod = (float)((int)theoreticalMove.x % (int)pointStep) + theoreticalMove.x - (int)theoreticalMove.x;
	float zmod = (float)((int)theoreticalMove.y % (int)pointStep) + theoreticalMove.y - (int)theoreticalMove.y;
	float xAxis = (xmod < 0 ? pointStep - abs(xmod) : xmod) / pointStep;
	float zAxis = (zmod < 0 ? pointStep - abs(zmod) : zmod) / pointStep;

	float xLower = terrainHeights->at(index) * (1 - xAxis) + terrainHeights->at(index + 1) * xAxis;
	float xUpper = terrainHeights->at(index + width) * (1 - xAxis) + terrainHeights->at(index + width + 1) * xAxis;

	// Add a slight offset to the terrain so that the camera is not inside it
	terrainY = xLower * (1 - zAxis) + xUpper * zAxis + 15;

	vEye.xz = theoreticalMove;
	vEye.y = terrainY;
}

void Camera::rotateCamera(long deltaX, long deltaY)
{
	float fRotX = (ELM_PI / 180) * (deltaX / 2), fRotY = (ELM_PI / 180) * (deltaY / 2);

	yawAngle -= fRotX;
	elm::rotationAxis(mRot, elm::vec3(0, 1, 0), yawAngle);
	vRight = mRot * elm::vec3(1, 0, 0);
	vLookAt = mRot * elm::vec3(0, 0, 1);

	pitchAngle -= fRotY;
	if(pitchAngle > ELM_PI / 2.1f || pitchAngle < -ELM_PI / 2.1f)
		pitchAngle = pitchAngle > ELM_PI / 2.1f ? ELM_PI / 2.1f : -ELM_PI / 2.1f;

	elm::rotationAxis(mRot, vRight, pitchAngle);
	vUp = mRot * elm::vec3(0, 1, 0);
	vLookAt = mRot * vLookAt;
}

void Camera::update(double dt)
{
	POINT mousePoint;
	GetCursorPos(&mousePoint);
	SetCursorPos(mouseX, mouseY);
	if(mousePoint.x != mouseX || mousePoint.y != mouseY)
		rotateCamera(mousePoint.x - mouseX, mousePoint.y - mouseY);

	elm::vec3 rSpeed;
	float speed = boost.isDown() ? 500.f : 100.f;
	if(forwards.isDown())		rSpeed += vLookAt;
	if(backwards.isDown())		rSpeed -= vLookAt;
	if(left.isDown())			rSpeed += cross(vLookAt, vUp);
	if(right.isDown())			rSpeed -= cross(vLookAt, vUp);

	if(rSpeed != 0)
	{
		if(cameraSate == 0)
			vEye += normalize(rSpeed) * speed * (float)dt;
		else
			followTerrain(vEye.xz + normalize(rSpeed.xz) * speed * (float)dt);
	}

	if(state.isDown(true))
	{
		cameraSate = cameraSate == 0 ? 1 : 0;
		if(cameraSate == 1)
			followTerrain(vEye.xz);
	}

	//std::stringstream ss;
	////ss << "Camera pos (" << (int)vEye.x << ", " << (int)vEye.y << ", " << (int)vEye.z << ")";
	//ss << cameraSate;
	//
	//SetWindowTextA(GraphicsDX11::getInstance()->getHwnd(), ss.str().c_str());

	elm::lookAtLH(mView, vLookAt, vUp, vEye);

	frustum = elm::buildViewFrustum(mView, mProj);

	cb.cameraPos = elm::vec4(vEye, 1);
	cb.view = mView;

	GraphicsDX11::getInstance()->updateCBuffer(&cb);
}

Camera::~Camera()
{
}