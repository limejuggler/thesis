#ifndef _CAMERA_
#define _CAMERA_

#include "stdafx.h"
#include "key.h"

class Camera
{
private:
	int cameraSate, mouseX, mouseY;
	float yawAngle, pitchAngle;

	Key forwards, backwards, left, right, state, boost;

	elm::mat4 mView, mProj, mRot;
	elm::vec3 vEye, vLookAt, vUp, vRight;

	// A pointer to the stored height values (in this case, derrived from the height map)
	std::vector<float> *terrainHeights;

	std::vector<elm::vec4> frustum;

	void followTerrain(elm::vec2 theoreticalMove);
	void rotateCamera(long deltaX, long deltaY);

public:
	Camera();
	~Camera();

	const elm::vec3 *getPos()								{ return &vEye; }
	std::vector<elm::vec4> getFrustum()						{ return frustum; }

	void setMouseLock(int screenLockX, int screenLockY);
	void setTerrainHeights(std::vector<float> *heights)		{ terrainHeights = heights; }

	void update(double dt);
};

#endif