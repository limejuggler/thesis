#include <tchar.h>

#include "computeHelp.h"

ComputeShader::ComputeShader()
: device(nullptr), deviceContext(nullptr), cShader(nullptr)
{

}

bool ComputeShader::Init(TCHAR* shaderName, TCHAR* blobFileAppendix, char* pFunctionName, D3D10_SHADER_MACRO* pDefines,
	ID3D11Device* d3dDevice, ID3D11DeviceContext*d3dContext)
{
	HRESULT hr = S_OK;
	device = d3dDevice;
	deviceContext = d3dContext;

	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;

	FILE* shaderFile = nullptr;

	TCHAR blobFilename[300];
	if(blobFileAppendix != nullptr)
	{
		_tcsncpy_s(blobFilename, shaderName, _tcslen(shaderName) - _tcslen(_tcsrchr(shaderName, _T('.'))));

		_tcscat_s(blobFilename, _T("_"));
		_tcscat_s(blobFilename, blobFileAppendix);
		_tcscat_s(blobFilename, _T(".blob"));

		_tfopen_s(&shaderFile, blobFilename, _T("rb"));
	}

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS |
		D3DCOMPILE_IEEE_STRICTNESS |
		D3DCOMPILE_PREFER_FLOW_CONTROL;

#if defined(DEBUG) || defined(_DEBUG)
	dwShaderFlags |= D3DCOMPILE_DEBUG;
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	dwShaderFlags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

	if(shaderFile == nullptr)
	{
		hr = D3DCompileFromFile(shaderName, pDefines, nullptr, pFunctionName, "cs_5_0",
			dwShaderFlags, NULL, &shaderBlob, &errorBlob);

		if(hr == S_OK)
		{
			if(blobFileAppendix != nullptr)
			{
				_tfopen_s(&shaderFile, blobFilename, _T("wb"));

				if(shaderFile != nullptr)
				{
					size_t size = shaderBlob->GetBufferSize();
					fwrite(&size, sizeof(size_t), 1, shaderFile);
					fwrite(shaderBlob->GetBufferPointer(), size, 1, shaderFile);
					fclose(shaderFile);
				}
			}
		}
	}
	else
	{
		int size;
		fread_s(&size, sizeof(int), sizeof(int), 1, shaderFile);

		if(D3DCreateBlob(size, &shaderBlob) == S_OK)
			fread_s(shaderBlob->GetBufferPointer(), size, size, 1, shaderFile);
		
		fclose(shaderFile);
	}
	if(errorBlob)
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());

	if(hr == S_OK)
		hr = device->CreateComputeShader(shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(), nullptr, &cShader);

	SAFE_RELEASE(errorBlob);
	SAFE_RELEASE(shaderBlob);

	return (hr == S_OK);
}

void ComputeShader::Set()
{
	deviceContext->CSSetShader(cShader, nullptr, 0);
}

void ComputeShader::Unset()
{
	deviceContext->CSSetShader(nullptr, nullptr, 0);
}

ComputeShader::~ComputeShader()
{
	SAFE_RELEASE(cShader);
}

//ComputeBuffer* ComputeWrap::CreateBuffer(COMPUTE_BUFFER_TYPE uType,
//	UINT uElementSize, UINT uCount, bool bSRV, bool bUAV, VOID* pInitData, bool bCreateStaging, char* debugName)
//{
//	ComputeBuffer* buffer = new ComputeBuffer();
//	buffer->_D3DContext = mD3DDeviceContext;
//
//	if(uType == STRUCTURED_BUFFER)
//		buffer->_Resource = CreateStructuredBuffer(uElementSize, uCount, bSRV, bUAV, pInitData);
//	else if(uType == RAW_BUFFER)
//		buffer->_Resource = CreateRawBuffer(uElementSize * uCount, pInitData);
//
//	if(buffer->_Resource != nullptr)
//	{
//		if(bSRV)
//			buffer->_ResourceView = CreateBufferSRV(buffer->_Resource);
//		if(bUAV)
//			buffer->_UnorderedAccessView = CreateBufferUAV(buffer->_Resource);
//
//		if(bCreateStaging)
//			buffer->_Staging = CreateStagingBuffer(uElementSize * uCount);
//	}
//
//	if(debugName)
//	{
//		if(buffer->_Resource)				SetDebugName(buffer->_Resource, debugName);
//		if(buffer->_Staging)				SetDebugName(buffer->_Staging, debugName);
//		if(buffer->_ResourceView)			SetDebugName(buffer->_ResourceView, debugName);
//		if(buffer->_UnorderedAccessView)	SetDebugName(buffer->_UnorderedAccessView, debugName);
//	}
//
//	return buffer; //return shallow copy
//}

ComputeShader* ComputeWrap::CreateComputeShader(TCHAR* shaderFile, TCHAR* blobFileAppendix, char* pFunctionName, D3D10_SHADER_MACRO* pDefines)
{
	ComputeShader* cs = new ComputeShader();

	if(cs && !cs->Init(
		shaderFile,
		blobFileAppendix,
		pFunctionName,
		pDefines,
		device,
		deviceContext))
	{
		SAFE_DELETE(cs);
	}

	return cs;
}

ID3D11Buffer* ComputeWrap::CreateStructuredBuffer(UINT uElementSize, UINT uCount,
	bool bSRV, bool bUAV, VOID* pInitData)
{
	ID3D11Buffer* pBufOut = nullptr;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BindFlags = 0;

	if(bUAV)	desc.BindFlags |= D3D11_BIND_UNORDERED_ACCESS;
	if(bSRV)	desc.BindFlags |= D3D11_BIND_SHADER_RESOURCE;

	UINT bufferSize = uElementSize * uCount;
	desc.ByteWidth = bufferSize < 16 ? 16 : bufferSize;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	desc.StructureByteStride = uElementSize;

	if(pInitData)
	{
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = pInitData;
		device->CreateBuffer(&desc, &InitData, &pBufOut);
	}
	else
	{
		device->CreateBuffer(&desc, nullptr, &pBufOut);
	}

	return pBufOut;
}

ID3D11Buffer *ComputeWrap::CreateRawBuffer(UINT uSize, void* pInitData)
{
	ID3D11Buffer *pBufOut = nullptr;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_INDEX_BUFFER | D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = uSize;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;

	if(pInitData)
	{
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = pInitData;
		device->CreateBuffer(&desc, &InitData, &pBufOut);
	}
	else
	{
		device->CreateBuffer(&desc, nullptr, &pBufOut);
	}

	return pBufOut;
}

ID3D11ShaderResourceView *ComputeWrap::CreateBufferSRV(ID3D11Buffer *pBuffer)
{
	ID3D11ShaderResourceView *pSRVOut = nullptr;

	D3D11_BUFFER_DESC descBuf;
	ZeroMemory(&descBuf, sizeof(descBuf));
	pBuffer->GetDesc(&descBuf);

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
	desc.BufferEx.FirstElement = 0;

	if(descBuf.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS)
	{
		// This is a Raw Buffer
		desc.Format = DXGI_FORMAT_R32_TYPELESS;
		desc.BufferEx.Flags = D3D11_BUFFEREX_SRV_FLAG_RAW;
		desc.BufferEx.NumElements = descBuf.ByteWidth / 4;
	}
	else if(descBuf.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_STRUCTURED)
	{
		// This is a Structured Buffer
		desc.Format = DXGI_FORMAT_UNKNOWN;
		desc.BufferEx.NumElements = descBuf.ByteWidth / descBuf.StructureByteStride;
	}
	else
		return nullptr;

	device->CreateShaderResourceView(pBuffer, &desc, &pSRVOut);

	return pSRVOut;
}

ID3D11UnorderedAccessView *ComputeWrap::CreateBufferUAV(ID3D11Buffer *pBuffer)
{
	ID3D11UnorderedAccessView *pUAVOut = nullptr;

	D3D11_BUFFER_DESC descBuf;
	ZeroMemory(&descBuf, sizeof(descBuf));
	pBuffer->GetDesc(&descBuf);

	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	desc.Buffer.FirstElement = 0;

	if(descBuf.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS)
	{
		// This is a Raw Buffer
		desc.Format = DXGI_FORMAT_R32_TYPELESS; // Format must be DXGI_FORMAT_R32_TYPELESS, when creating Raw Unordered Access View
		desc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
		desc.Buffer.NumElements = descBuf.ByteWidth / 4;
	}
	else if(descBuf.MiscFlags & D3D11_RESOURCE_MISC_BUFFER_STRUCTURED)
	{
		// This is a Structured Buffer
		desc.Format = DXGI_FORMAT_UNKNOWN;      // Format must be must be DXGI_FORMAT_UNKNOWN, when creating a View of a Structured Buffer
		desc.Buffer.NumElements = descBuf.ByteWidth / descBuf.StructureByteStride;
	}
	else
	{
		return nullptr;
	}

	device->CreateUnorderedAccessView(pBuffer, &desc, &pUAVOut);

	return pUAVOut;
}

ID3D11Buffer* ComputeWrap::CreateStagingBuffer(UINT uSize)
{
	ID3D11Buffer* debugbuf = nullptr;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.ByteWidth = uSize;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	desc.Usage = D3D11_USAGE_STAGING;
	desc.BindFlags = 0;
	desc.MiscFlags = 0;

	device->CreateBuffer(&desc, nullptr, &debugbuf);

	return debugbuf;
}