#pragma once

#include "stdafx.h"

#include <d3dcommon.h>
#include <d3d11.h>
#include <d3dcompiler.h>

class ComputeShader
{
private:
	friend class ComputeWrap;

	ID3D11Device*               device;
	ID3D11DeviceContext*        deviceContext;

	ID3D11ComputeShader*		cShader;
	explicit ComputeShader();

public:
	bool Init(TCHAR* shaderName, TCHAR* blobFileAppendix, char* pFunctionName, D3D10_SHADER_MACRO* pDefines,
		ID3D11Device* d3dDevice, ID3D11DeviceContext*d3dContext);

	~ComputeShader();

	void Set();
	void Unset();
};

class ComputeWrap
{
	ID3D11Device*               device;
	ID3D11DeviceContext*        deviceContext;

public:
	ComputeWrap(ID3D11Device* d3dDevice, ID3D11DeviceContext* d3dContext)
	{
		device = d3dDevice;
		deviceContext = d3dContext;
	}

	ComputeShader* CreateComputeShader(TCHAR* shaderFile, TCHAR* blobFileAppendix, char* pFunctionName, D3D10_SHADER_MACRO* pDefines);

	ID3D11Buffer* CreateConstantBuffer(UINT uSize, void* pInitData, char* debugName = nullptr);

	//ComputeBuffer* CreateBuffer(COMPUTE_BUFFER_TYPE uType, UINT uElementSize,
	//	UINT uCount, bool bSRV, bool bUAV, VOID* pInitData, bool bCreateStaging = false, char* debugName = nullptr);
	//
	//ComputeTexture* CreateTexture(DXGI_FORMAT dxFormat, UINT uWidth,
	//	UINT uHeight, UINT uRowPitch, VOID* pInitData, bool bCreateStaging = false, char* debugName = nullptr);
	//
	//ComputeTexture* CreateTexture(TCHAR* textureFilename, char* debugName = nullptr);

	ID3D11ShaderResourceView* CreateBufferSRV(ID3D11Buffer* pBuffer);

private:
	ID3D11Buffer* CreateStructuredBuffer(UINT uElementSize, UINT uCount, bool bSRV, bool bUAV, VOID* pInitData);
	ID3D11Buffer* CreateRawBuffer(UINT uSize, VOID* pInitData);
	ID3D11UnorderedAccessView* CreateBufferUAV(ID3D11Buffer* pBuffer);
	ID3D11Buffer* CreateStagingBuffer(UINT uSize);
	//
	////texture functions
	//ID3D11Texture2D* CreateTextureResource(DXGI_FORMAT dxFormat,
	//	UINT uWidth, UINT uHeight, UINT uRowPitch, VOID* pInitData);
	////ID3D11Buffer* CreateRawBuffer(UINT uSize, VOID* pInitData);
	//ID3D11ShaderResourceView* CreateTextureSRV(ID3D11Texture2D* pTexture);
	//ID3D11UnorderedAccessView* CreateTextureUAV(ID3D11Texture2D* pTexture);
	//ID3D11Texture2D* CreateStagingTexture(ID3D11Texture2D* pTexture);
	//
	//void SetDebugName(ID3D11DeviceChild* object, char* debugName);
};