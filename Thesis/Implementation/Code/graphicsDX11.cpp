#include <sstream>
#include <D3D/D3DX11.h>
#include <D3D/D3DX11tex.h>

#include "graphicsDX11.h"
#include "settings.h"

GraphicsDX11 *GraphicsDX11::instance = nullptr;

GraphicsDX11 *GraphicsDX11::getInstance()
{
	if(!instance)
		instance = new GraphicsDX11();

	return instance;
}

GraphicsDX11::GraphicsDX11()
{
	hInst						= nullptr;
//	swapChain					= nullptr;
//	device						= nullptr;
//	deviceContext1				= nullptr;
//	deviceContext2				= nullptr;
//
//	indexBuffer1				= nullptr;
//	indexBuffer2				= nullptr;
//	dispatchIndexBuffer			= nullptr;
//	vertexBuffer				= nullptr;
//	cbOnMove					= nullptr;
//	cbOnce						= nullptr;
//	cbTriangulationConstants	= nullptr;
//
//	computeSys1					= nullptr;
//	computeSys2					= nullptr;
//	delaunay1					= nullptr;
//	delaunay2					= nullptr;
//
//	timer1						= nullptr;
//	timer2						= nullptr;

	fpsFreq = dispatchFreq = -3;
	lastDispatchTime = 0.0;
	fps = 0;

	activeContext = 2;

	colourState = renderState = 0;

	toggleWireframe = Key(0x31);
	togglePatchColour = Key(0x32);

	if(FAILED(init(renderData1)))
		PostQuitMessage(1);

	if(FAILED(init(renderData2)))
		PostQuitMessage(1);
}

//HRESULT GraphicsDX11::init(RenderStruct data)
//{
//	HRESULT hr = S_OK;
//
//	RECT rc;
//	GetClientRect(hWnd, &rc);
//	int g_width = rc.right - rc.left;
//	int g_height = rc.bottom - rc.top;
//
//	UINT createDeviceFlags = 0;
//#ifdef _DEBUG
//	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
//#endif
//
//	D3D_DRIVER_TYPE driverType;
//
//	D3D_DRIVER_TYPE driverTypes[] =
//	{
//		D3D_DRIVER_TYPE_HARDWARE,
//		D3D_DRIVER_TYPE_REFERENCE,
//	};
//	UINT numDriverTypes = sizeof(driverTypes) / sizeof(driverTypes[0]);
//
//	DXGI_SWAP_CHAIN_DESC sd;
//	ZeroMemory(&sd, sizeof(sd));
//	sd.BufferCount = 1;
//	sd.BufferDesc.Width = g_width;
//	sd.BufferDesc.Height = g_height;
//	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
//	sd.BufferDesc.RefreshRate.Numerator = 60;
//	sd.BufferDesc.RefreshRate.Denominator = 1;
//	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
//	sd.OutputWindow = hWnd;
//	sd.SampleDesc.Count = 1;
//	sd.SampleDesc.Quality = 0;
//	sd.Windowed = TRUE;
//
//	D3D_FEATURE_LEVEL featureLevelsToTry[] = {
//		D3D_FEATURE_LEVEL_11_0,
//		D3D_FEATURE_LEVEL_10_1,
//		D3D_FEATURE_LEVEL_10_0
//	};
//	D3D_FEATURE_LEVEL initiatedFeatureLevel;
//
//	//ID3D11DeviceContext *contexts[2] = { deviceContext1, deviceContext2 };
//
//	for(UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
//	{
//		driverType = driverTypes[driverTypeIndex];
//		hr = D3D11CreateDeviceAndSwapChain(
//			nullptr,
//			driverType,
//			nullptr,
//			createDeviceFlags,
//			featureLevelsToTry,
//			ARRAYSIZE(featureLevelsToTry),
//			D3D11_SDK_VERSION,
//			&sd,
//			&swapChain,
//			&device,
//			&initiatedFeatureLevel,
//			&deviceContext1);
//
//		//hr = D3D11CreateDevice(
//		//	nullptr,
//		//	driverType,
//		//	nullptr,
//		//	createDeviceFlags,
//		//	featureLevelsToTry,
//		//	ARRAYSIZE(featureLevelsToTry),
//		//	D3D11_SDK_VERSION,
//		//	nullptr,
//		//	&initiatedFeatureLevel,
//		//	&deviceContext1
//		//	);
//		//
//		//hr = D3D11CreateDevice(
//		//	nullptr,
//		//	driverType,
//		//	nullptr,
//		//	createDeviceFlags,
//		//	featureLevelsToTry,
//		//	ARRAYSIZE(featureLevelsToTry),
//		//	D3D11_SDK_VERSION,
//		//	nullptr,
//		//	&initiatedFeatureLevel,
//		//	&deviceContext2
//		//	);
//
//		if(SUCCEEDED(hr))
//			break;
//	}
//	if(FAILED(hr))
//	{
//		MessageBoxA(nullptr, "Failed to create swap chain", "GraphicsCore Error", MB_OK);
//		return hr;
//	}
//
//	//device->CreateDeferredContext(0, &deviceContext2);
//	device->GetImmediateContext(&deviceContext2);
//
//	// Create a render target view
//	ID3D11Texture2D *backBuffer = NULL;
//	hr = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the backbuffer", L"ERROR", 0);
//		return hr;
//	}
//
//	hr = device->CreateRenderTargetView(backBuffer, NULL, &renderTargetView);
//	SAFE_RELEASE(backBuffer);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the rendertargetview", L"ERROR", 0);
//		return hr;
//	}
//
//	// Create depth stencil texture
//	D3D11_TEXTURE2D_DESC descDepth;
//	ZeroMemory(&descDepth, sizeof(descDepth));
//	descDepth.Width = (UINT)g_width;
//	descDepth.Height = (UINT)g_height;
//	descDepth.MipLevels = 1;
//	descDepth.ArraySize = 1;
//	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
//	descDepth.SampleDesc.Count = 1;
//	descDepth.SampleDesc.Quality = 0;
//	descDepth.Usage = D3D11_USAGE_DEFAULT;
//	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
//	descDepth.CPUAccessFlags = 0;
//	descDepth.MiscFlags = 0;
//
//	ID3D11Texture2D *depthStencil = nullptr;
//	hr = device->CreateTexture2D(&descDepth, NULL, &depthStencil);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the depthstencil", L"ERROR", 0);
//		return hr;
//	}
//
//	// Create the depth stencil view
//	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
//	ZeroMemory(&descDSV, sizeof(descDSV));
//	descDSV.Format = descDepth.Format;
//	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
//	descDSV.Texture2D.MipSlice = 0;
//	hr = device->CreateDepthStencilView(depthStencil, &descDSV, &depthStencilView);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the depthstencilview", L"ERROR", 0);
//		return hr;
//	}
//
//	SAFE_RELEASE(depthStencil);
//
//	// Setup the viewport
//	D3D11_VIEWPORT vp;
//	vp.Width = (FLOAT)g_width;
//	vp.Height = (FLOAT)g_height;
//	vp.MinDepth = 0.f;
//	vp.MaxDepth = 1.f;
//	vp.TopLeftX = 0;
//	vp.TopLeftY = 0;
//	deviceContext1->RSSetViewports(1, &vp);
//	deviceContext2->RSSetViewports(1, &vp);
//
//	deviceContext1->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
//	deviceContext2->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
//
//	D3D11_RASTERIZER_DESC rasterDesc;
//	rasterDesc.AntialiasedLineEnable = false;
//	rasterDesc.CullMode = D3D11_CULL_BACK;
//	rasterDesc.DepthBias = 0;
//	rasterDesc.DepthBiasClamp = 0.f;
//	rasterDesc.DepthClipEnable = true;
//	rasterDesc.FillMode = D3D11_FILL_SOLID;
//	rasterDesc.FrontCounterClockwise = false;
//	rasterDesc.MultisampleEnable = false;
//	rasterDesc.ScissorEnable = false;
//	rasterDesc.SlopeScaledDepthBias = 0.f;
//
//	// Create the rasterizer state for normal rendering
//	hr = device->CreateRasterizerState(&rasterDesc, &rasterizerFilled);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the rasterizer state", L"ERROR", 0);
//		return hr;
//	}
//
//	rasterDesc.CullMode = D3D11_CULL_NONE;
//	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
//	// Create the rasterizer state for wireframe rendering
//	hr = device->CreateRasterizerState(&rasterDesc, &rasterizerWireframe);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the rasterizer state for wireframe", L"ERROR", 0);
//		return hr;
//	}
//
//	// Now set the rasterizer state.
//	deviceContext1->RSSetState(rasterizerFilled);
//	deviceContext2->RSSetState(rasterizerFilled);
//
//	//create blendstates
//	D3D11_BLEND_DESC blendDesc;
//	ZeroMemory(&blendDesc, sizeof(blendDesc));
//	blendDesc.RenderTarget[0].BlendEnable = TRUE;
//	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
//	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
//	
//	blendDesc.RenderTarget[0].BlendEnable = FALSE;
//	hr = device->CreateBlendState(&blendDesc, &blendDisable);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the blenddisable", L"ERROR", 0);
//		return hr;
//	}
//
//	// Create depthstencil states
//	D3D11_DEPTH_STENCIL_DESC depthDesc;
//	depthDesc.DepthEnable = TRUE;
//	depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	depthDesc.DepthFunc = D3D11_COMPARISON_LESS;
//	depthDesc.StencilEnable = FALSE;
//	depthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
//	depthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
//	depthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//	depthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//	depthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
//	depthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
//	depthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//
//	hr = device->CreateDepthStencilState(&depthDesc, &depthStencilState);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create the depthStencilState", L"ERROR", 0);
//		return hr;
//	}
//
//	// Create  buffers
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	bd.ByteWidth = sizeof(CBOnce);
//	bd.CPUAccessFlags = 0;
//
//	hr = device->CreateBuffer(&bd, nullptr, &cbOnce);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create constant buffer (once)", L"ERROR", 0);
//		return hr;
//	}
//
//	bd.ByteWidth = sizeof(CBOnMove);
//	hr = device->CreateBuffer(&bd, nullptr, &cbOnMove);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create constant buffer (onMove)", L"ERROR", 0);
//		return hr;
//	}
//
//	bd.ByteWidth = sizeof(CBTriangulationConstants);
//	hr = device->CreateBuffer(&bd, nullptr, &cbTriangulationConstants);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create constant buffer (bufferOffset)", L"ERROR", 0);
//		return hr;
//	}
//
//	bd.ByteWidth = sizeof(CBColour);
//	hr = device->CreateBuffer(&bd, nullptr, &cbColour);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create constant buffer (colour)", L"ERROR", 0);
//		return hr;
//	}
//
//	deviceContext1->VSSetConstantBuffers(0, 1, &cbOnce);
//	deviceContext1->VSSetConstantBuffers(1, 1, &cbOnMove);
//	deviceContext1->CSSetConstantBuffers(0, 1, &cbTriangulationConstants);
//	deviceContext1->PSSetConstantBuffers(0, 1, &cbColour);
//	deviceContext2->VSSetConstantBuffers(0, 1, &cbOnce);
//	deviceContext2->VSSetConstantBuffers(1, 1, &cbOnMove);
//	deviceContext2->CSSetConstantBuffers(0, 1, &cbTriangulationConstants);
//	deviceContext2->PSSetConstantBuffers(0, 1, &cbColour);
//
//	D3D11_SAMPLER_DESC sampDesc;
//	ZeroMemory(&sampDesc, sizeof(sampDesc));
//	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
//	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
//	sampDesc.MinLOD = -D3D11_FLOAT32_MAX;
//	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
//
//	hr = device->CreateSamplerState(&sampDesc, &sampler);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create sampler state", L"ERROR", MB_OK);
//		return hr;
//	}
//
//	// Due to the irregular grid, texture coordinates will be calculated based on the world position of the vertex
//	D3D11_INPUT_ELEMENT_DESC standardLayout[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(float)* 3, D3D11_INPUT_PER_VERTEX_DATA, 0 }
//	};
//
//	auto compileShaderFromFile = [](LPCWSTR fileName, const D3D10_SHADER_MACRO *shaderMacro, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)->HRESULT
//	{
//		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
//#if defined(DEBUG) || defined(_DEBUG)
//		dwShaderFlags |= D3DCOMPILE_DEBUG;
//#endif
//
//		return D3DX11CompileFromFile(fileName, shaderMacro, nullptr, szEntryPoint, szShaderModel,
//			dwShaderFlags, 0, nullptr, ppBlobOut, nullptr, nullptr);
//	};
//	
//	std::string width = toString(Settings::getInstance()->width);
//	std::string pointStep = toString(Settings::getInstance()->pointStep);
//	D3D10_SHADER_MACRO macroVS[] = { "width", width.c_str(), "pointStep", pointStep.c_str(), nullptr, nullptr };
//
//	ID3DBlob *blob;
//	hr = compileShaderFromFile(L"../Shaders/terrainVS.hlsl", macroVS, "main", "vs_4_0", &blob);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create vertex shader", L"ERROR", MB_OK);
//		return hr;
//	}
//
//	ID3D11InputLayout *layout = nullptr;
//	device->CreateInputLayout(standardLayout, ARRAYSIZE(standardLayout), blob->GetBufferPointer(), blob->GetBufferSize(), &layout);
//	deviceContext1->IASetInputLayout(layout);
//	deviceContext2->IASetInputLayout(layout);
//
//	device->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &vertex);
//
//	SAFE_RELEASE(layout);
//	SAFE_RELEASE(blob);
//
//	hr = compileShaderFromFile(L"../Shaders/terrainPS.hlsl", nullptr, "main", "ps_4_0", &blob);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create pixel shader", L"ERROR", MB_OK);
//		return hr;
//	}
//
//	device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &pixel);
//
//	SAFE_RELEASE(blob);
//
//	hr = compileShaderFromFile(L"../Shaders/terrainPS.hlsl", nullptr, "patchColours", "ps_4_0", &blob);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create pixel shader", L"ERROR", MB_OK);
//		return hr;
//	}
//
//	device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &pixelPatchColours);
//
//	SAFE_RELEASE(blob);
//
//	//create helper sys
//	computeSys1 = new ComputeWrap(device, deviceContext1);
//	computeSys2 = new ComputeWrap(device, deviceContext2);
//
//	timer1 = new DispatchTimer(device, deviceContext1);
//	timer2 = new DispatchTimer(device, deviceContext2);
//
//#ifndef CPUTriangulation
//	delaunay1 = computeSys1->CreateComputeShader(L"../Shaders/delaunayCS.hlsl", nullptr, "main", nullptr);
//	delaunay2 = computeSys2->CreateComputeShader(L"../Shaders/delaunayCS.hlsl", nullptr, "main", nullptr);
//#endif
//
//	D3D11_QUERY_DESC desc;
//	desc.MiscFlags = 0;
//	desc.Query = D3D11_QUERY_EVENT;
//
//	hr = device->CreateQuery(&desc, &triangulationQuery);
//	if(FAILED(hr))
//	{
//		MessageBox(hWnd, L"Failed to create a query for asynchronous GPU calls", L"ERROR", MB_OK);
//		return hr;
//	}
//
//	colours = std::vector<elm::vec3>(	(Settings::getInstance()->width / Settings::getInstance()->patchDimensionX) * 
//										(Settings::getInstance()->height / Settings::getInstance()->patchDimensionY));
//
//	for(uint i = 0; i < colours.size(); i++)
//		colours[i] = elm::normalize(elm::vec3((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX));
//
//	regionSize = Settings::getInstance()->bufferOffset;
//
//	// Only one type of rendering, so these does not need to be set on a frame to frame basis
//
//	deviceContext1->OMSetDepthStencilState(depthStencilState, 0);
//	deviceContext1->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	deviceContext2->OMSetDepthStencilState(depthStencilState, 0);
//	deviceContext2->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//
//	deviceContext1->PSSetSamplers(0, 1, &sampler);
//	deviceContext2->PSSetSamplers(0, 1, &sampler);
//
//	deviceContext1->VSSetConstantBuffers(0, 1, &cbOnce);
//	deviceContext1->VSSetConstantBuffers(1, 1, &cbOnMove);
//	deviceContext2->VSSetConstantBuffers(0, 1, &cbOnce);
//	deviceContext2->VSSetConstantBuffers(1, 1, &cbOnMove);
//
//	float blendFactor[4] = { 1.f, 1.f, 1.f, 1.f };
//	deviceContext1->OMSetBlendState(blendDisable, blendFactor, 0xffffffff);
//	deviceContext2->OMSetBlendState(blendDisable, blendFactor, 0xffffffff);
//
//	deviceContext1->VSSetShader(vertex, nullptr, 0);
//	deviceContext1->PSSetShader(pixel, nullptr, 0);
//	deviceContext2->VSSetShader(vertex, nullptr, 0);
//	deviceContext2->PSSetShader(pixel, nullptr, 0);
//	
//	return S_OK;
//}

HRESULT GraphicsDX11::init(RenderStruct &data)
{
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(hWnd, &rc);
	int g_width = rc.right - rc.left;
	int g_height = rc.bottom - rc.top;

	UINT createDeviceFlags = 0;
//#ifdef _DEBUG
//	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
//#endif

	D3D_DRIVER_TYPE driverType;

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = sizeof(driverTypes) / sizeof(driverTypes[0]);

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = g_width;
	sd.BufferDesc.Height = g_height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	D3D_FEATURE_LEVEL featureLevelsToTry[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};
	D3D_FEATURE_LEVEL initiatedFeatureLevel;

	//ID3D11DeviceContext *contexts[2] = { deviceContext1, deviceContext2 };

	for(UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			nullptr,
			driverType,
			nullptr,
			createDeviceFlags,
			featureLevelsToTry,
			ARRAYSIZE(featureLevelsToTry),
			D3D11_SDK_VERSION,
			&sd,
			&data.swapChain,
			&data.device,
			&initiatedFeatureLevel,
			&data.deviceContext);

		if(SUCCEEDED(hr))
			break;
	}
	if(FAILED(hr))
	{
		MessageBoxA(nullptr, "Failed to create swap chain", "GraphicsCore Error", MB_OK);
		return hr;
	}

	//device->CreateDeferredContext(0, &deviceContext2);
	//device->GetImmediateContext(&deviceContext2);

	// Create a render target view
	ID3D11Texture2D *backBuffer = NULL;
	hr = data.swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the backbuffer", L"ERROR", 0);
		return hr;
	}

	hr = data.device->CreateRenderTargetView(backBuffer, NULL, &data.renderTargetView);
	SAFE_RELEASE(backBuffer);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the rendertargetview", L"ERROR", 0);
		return hr;
	}

	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = (UINT)g_width;
	descDepth.Height = (UINT)g_height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	ID3D11Texture2D *depthStencil = nullptr;
	hr = data.device->CreateTexture2D(&descDepth, NULL, &depthStencil);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the depthstencil", L"ERROR", 0);
		return hr;
	}

	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = data.device->CreateDepthStencilView(depthStencil, &descDSV, &data.depthStencilView);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the depthstencilview", L"ERROR", 0);
		return hr;
	}

	SAFE_RELEASE(depthStencil);

	// Setup the viewport
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)g_width;
	vp.Height = (FLOAT)g_height;
	vp.MinDepth = 0.f;
	vp.MaxDepth = 1.f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	data.deviceContext->RSSetViewports(1, &vp);

	data.deviceContext->OMSetRenderTargets(1, &data.renderTargetView, data.depthStencilView);

	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.f;

	// Create the rasterizer state for normal rendering
	hr = data.device->CreateRasterizerState(&rasterDesc, &data.rasterizerFilled);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the rasterizer state", L"ERROR", 0);
		return hr;
	}

	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	// Create the rasterizer state for wireframe rendering
	hr = data.device->CreateRasterizerState(&rasterDesc, &data.rasterizerWireframe);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the rasterizer state for wireframe", L"ERROR", 0);
		return hr;
	}

	// Now set the rasterizer state.
	data.deviceContext->RSSetState(data.rasterizerFilled);

	//create blendstates
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	hr = data.device->CreateBlendState(&blendDesc, &data.blendDisable);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the blenddisable", L"ERROR", 0);
		return hr;
	}

	// Create depthstencil states
	D3D11_DEPTH_STENCIL_DESC depthDesc;
	depthDesc.DepthEnable = TRUE;
	depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDesc.StencilEnable = FALSE;
	depthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	depthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	depthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;

	hr = data.device->CreateDepthStencilState(&depthDesc, &data.depthStencilState);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create the depthStencilState", L"ERROR", 0);
		return hr;
	}

	// Create  buffers
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.ByteWidth = sizeof(CBOnce);
	bd.CPUAccessFlags = 0;

	hr = data.device->CreateBuffer(&bd, nullptr, &data.cbOnce);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create constant buffer (once)", L"ERROR", 0);
		return hr;
	}

	bd.ByteWidth = sizeof(CBOnMove);
	hr = data.device->CreateBuffer(&bd, nullptr, &data.cbOnMove);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create constant buffer (onMove)", L"ERROR", 0);
		return hr;
	}

	bd.ByteWidth = sizeof(CBTriangulationConstants);
	hr = data.device->CreateBuffer(&bd, nullptr, &data.cbTriangulationConstants);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create constant buffer (bufferOffset)", L"ERROR", 0);
		return hr;
	}

	bd.ByteWidth = sizeof(CBColour);
	hr = data.device->CreateBuffer(&bd, nullptr, &data.cbColour);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create constant buffer (colour)", L"ERROR", 0);
		return hr;
	}

	data.deviceContext->VSSetConstantBuffers(0, 1, &data.cbOnce);
	data.deviceContext->VSSetConstantBuffers(1, 1, &data.cbOnMove);
	data.deviceContext->CSSetConstantBuffers(0, 1, &data.cbTriangulationConstants);
	data.deviceContext->PSSetConstantBuffers(0, 1, &data.cbColour);

	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = -D3D11_FLOAT32_MAX;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	hr = data.device->CreateSamplerState(&sampDesc, &data.sampler);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create sampler state", L"ERROR", MB_OK);
		return hr;
	}

	// Due to the irregular grid, texture coordinates will be calculated based on the world position of the vertex
	D3D11_INPUT_ELEMENT_DESC standardLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(float)* 3, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	auto compileShaderFromFile = [](LPCWSTR fileName, const D3D10_SHADER_MACRO *shaderMacro, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)->HRESULT
	{
		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
		dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

		return D3DX11CompileFromFile(fileName, shaderMacro, nullptr, szEntryPoint, szShaderModel,
			dwShaderFlags, 0, nullptr, ppBlobOut, nullptr, nullptr);
	};

	std::string width = toString(Settings::getInstance()->width);
	std::string pointStep = toString(Settings::getInstance()->pointStep);
	D3D10_SHADER_MACRO macroVS[] = { "width", width.c_str(), "pointStep", pointStep.c_str(), nullptr, nullptr };

	ID3DBlob *blob;
	hr = compileShaderFromFile(L"../Shaders/terrainVS.hlsl", macroVS, "main", "vs_4_0", &blob);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create vertex shader", L"ERROR", MB_OK);
		return hr;
	}

	ID3D11InputLayout *layout = nullptr;
	data.device->CreateInputLayout(standardLayout, ARRAYSIZE(standardLayout), blob->GetBufferPointer(), blob->GetBufferSize(), &layout);
	data.deviceContext->IASetInputLayout(layout);

	data.device->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &data.vertex);

	SAFE_RELEASE(layout);
	SAFE_RELEASE(blob);

	hr = compileShaderFromFile(L"../Shaders/terrainPS.hlsl", nullptr, "main", "ps_4_0", &blob);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create pixel shader", L"ERROR", MB_OK);
		return hr;
	}

	data.device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &data.pixel);

	SAFE_RELEASE(blob);

	hr = compileShaderFromFile(L"../Shaders/terrainPS.hlsl", nullptr, "patchColours", "ps_4_0", &blob);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create pixel shader", L"ERROR", MB_OK);
		return hr;
	}

	data.device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &data.pixelPatchColours);

	SAFE_RELEASE(blob);

	//create helper sys
	data.computeSys = new ComputeWrap(data.device, data.deviceContext);

	data.timer = new DispatchTimer(data.device, data.deviceContext);

#ifndef CPUTriangulation
	data.delaunay = data.computeSys->CreateComputeShader(L"../Shaders/delaunayPerPatchCS.hlsl", nullptr, "main", nullptr);
#endif

	D3D11_QUERY_DESC desc;
	desc.MiscFlags = 0;
	desc.Query = D3D11_QUERY_EVENT;

	hr = data.device->CreateQuery(&desc, &data.triangulationQuery);
	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create a query for asynchronous GPU calls", L"ERROR", MB_OK);
		return hr;
	}

	colours = std::vector<elm::vec3>((Settings::getInstance()->width / Settings::getInstance()->patchDimensionX) *
		(Settings::getInstance()->height / Settings::getInstance()->patchDimensionY));

	rand(); rand();
	for(uint i = 0; i < colours.size(); i++)
		colours[i] = elm::normalize(elm::vec3((float)rand(), (float)rand(), (float)rand()) / RAND_MAX);

	regionSize = Settings::getInstance()->bufferOffset;

	// Only one type of rendering, so these does not need to be set on a frame to frame basis

	data.deviceContext->OMSetDepthStencilState(data.depthStencilState, 0);
	data.deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	data.deviceContext->PSSetSamplers(0, 1, &data.sampler);

	data.deviceContext->VSSetConstantBuffers(0, 1, &data.cbOnce);
	data.deviceContext->VSSetConstantBuffers(1, 1, &data.cbOnMove);

	float blendFactor[4] = { 1.f, 1.f, 1.f, 1.f };
	data.deviceContext->OMSetBlendState(data.blendDisable, blendFactor, 0xffffffff);

	data.deviceContext->VSSetShader(data.vertex, nullptr, 0);
	data.deviceContext->PSSetShader(data.pixel, nullptr, 0);

	return S_OK;
}

void GraphicsDX11::updateCBuffer(CBOnMove *cb)
{
	//lockDeviceContext();
	if(activeContext == 1)
		renderData1.deviceContext->UpdateSubresource(renderData1.cbOnMove, 0, NULL, cb, 0, 0);
	else
		renderData2.deviceContext->UpdateSubresource(renderData2.cbOnMove, 0, NULL, cb, 0, 0);
	//unlockDeviceContext();
}

void GraphicsDX11::setCBufferOnce(CBOnce *cb)
{
	//lockDeviceContext();
	renderData1.deviceContext->UpdateSubresource(renderData1.cbOnce, 0, NULL, cb, 0, 0);
	renderData2.deviceContext->UpdateSubresource(renderData2.cbOnce, 0, NULL, cb, 0, 0);
	//unlockDeviceContext();
}

void GraphicsDX11::setCBTriangulationConstants(CBTriangulationConstants *cb)
{
	//if(activeContext == 2)
		renderData2.deviceContext->UpdateSubresource(renderData2.cbTriangulationConstants, 0, NULL, cb, 0, 0);
	//else
		renderData1.deviceContext->UpdateSubresource(renderData1.cbTriangulationConstants, 0, NULL, cb, 0, 0);
	//lockDeviceContext();
	//renderData1.deviceContext->UpdateSubresource(renderData1.cbTriangulationConstants, 0, NULL, cb, 0, 0);
	//renderData2.deviceContext->UpdateSubresource(renderData2.cbTriangulationConstants, 0, NULL, cb, 0, 0);
	//unlockDeviceContext();
}

void GraphicsDX11::setAndCreateTexture(int texRegister, std::wstring filePath)
{
	ID3D11ShaderResourceView *tex1 = nullptr, *tex2 = nullptr;
	HRESULT hr1 = D3DX11CreateShaderResourceViewFromFile(renderData1.device, filePath.c_str(), nullptr, nullptr, &tex1, nullptr);
	HRESULT hr2 = D3DX11CreateShaderResourceViewFromFile(renderData2.device, filePath.c_str(), nullptr, nullptr, &tex2, nullptr);
	if(FAILED(hr1) || FAILED(hr2))
	{
		MessageBoxA(nullptr, "Failed to create texture", "ERROR", MB_OK);
		return;
	}

	renderData1.deviceContext->PSSetShaderResources(texRegister, 1, &tex1);
	renderData2.deviceContext->PSSetShaderResources(texRegister, 1, &tex2);

	SAFE_RELEASE(tex1);
	SAFE_RELEASE(tex2);
}

//void GraphicsDX11::copyIndexBufferData(uint start, uint steps)
//{
//	uint offset = Settings::getInstance()->bufferOffset;
//
//	D3D11_BOX srcBox{};
//	srcBox.left = offset * start;
//	srcBox.right = offset * (start * steps);
//	srcBox.top = 0;
//	srcBox.bottom = 1;
//	srcBox.front = 0;
//	srcBox.back = 1;
//
//	//for(uint i = start; i < start + steps; i++)
//	//{ // Problem: All groupIDs are 0 if called like this, should probably send groupID through a constant buffer to solve this
//	
//		//deviceContext->Dispatch(1, 1, 1);
//		//Sleep(5);
//		lockDeviceContext();
//		deviceContext1->CopySubresourceRegion(indexBuffer1, 0, start * offset, 0, 0, dispatchIndexBuffer, 0, &srcBox);
//	
//		//srcBox.left += offset;
//		//srcBox.right += offset;
//		unlockDeviceContext();
//	//}
//
//	//uint offset = Settings::getInstance()->bufferOffset;
//	//D3D11_MAPPED_SUBRESOURCE indexResource, bufferResource;
//	//
//	//deviceContext->Map(dispatchIndexBuffer, 0, D3D11_MAP_READ, 0, &bufferResource);
//	//uint *bufferData = (uint*)bufferResource.pData;
//	//
//	//for(uint i = 0; i < numPatches; i += 4)
//	//{
//	//	lockDeviceContext();
//	//	deviceContext->Map(indexBuffer1, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &indexResource);
//	//	uint *indices = (uint*)indexResource.pData;
//	//
//	//	for(uint j = i * offset; j < (i + 4) * offset; j++)
//	//	{
//	//		indices[j] = bufferData[j];
//	//	}
//	//
//	//	//uint currentPatch = (uint)	(patchPositions[i].first.x / x + 
//	//	//							(patchPositions[i].first.z / z) * nSplitsX);
//	//	//uint currentPatch = (uint)(patchPositions[sortedPatches[i]].first.x / x +
//	//	//	(patchPositions[sortedPatches[i]].first.z / z) * nSplitsX);
//	//
//	//	//uint *indices = (uint*)indexResource.pData;
//	//	//// Start by clearing all old data
//	//	//for(uint j = i * offset; j < (i + 4) * offset; j++)
//	//	//{
//	//	//	indices[i * offset + j] = patches[i][triList[j] - ids[i * 2]];
//	//	//}
//	//
//	//	instance->deviceContext->Unmap(instance->getIndexBuffer(), 0);
//	//	unlockDeviceContext();
//	//	//instance->unlockDeviceContext();
//	//}
//}

void GraphicsDX11::swapIndexBuffer()
{
	activeContext = activeContext == 1 ? 2 : 1;
}

bool GraphicsDX11::isTriangulationCompleted()
{
	HRESULT hr = (activeContext == 1 ?	renderData2.deviceContext->GetData(renderData2.triangulationQuery, NULL, 0, 0) : 
										renderData1.deviceContext->GetData(renderData1.triangulationQuery, NULL, 0, 0));
	return hr == S_OK;
}

HRESULT GraphicsDX11::createVertexBuffer(uint numElements, uint byteStride, void **data)
{
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.ByteWidth = byteStride * numElements;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA initData;
	ZeroMemory(&initData, sizeof(initData));
	initData.pSysMem = data;

	SAFE_RELEASE(renderData1.vertexBuffer);
	SAFE_RELEASE(renderData2.vertexBuffer);
	HRESULT hr1 = renderData1.device->CreateBuffer(&bufferDesc, &initData, &renderData1.vertexBuffer);
	HRESULT hr2 = renderData2.device->CreateBuffer(&bufferDesc, &initData, &renderData2.vertexBuffer);
	if(FAILED(hr1) || FAILED(hr2))
	{
		MessageBox(hWnd, L"Failed to create the vertex buffer", L"ERROR", S_OK);
		return hr1;
	}

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	renderData1.deviceContext->IASetVertexBuffers(0, 1, &renderData1.vertexBuffer, &stride, &offset);
	renderData2.deviceContext->IASetVertexBuffers(0, 1, &renderData2.vertexBuffer, &stride, &offset);

	return hr1;
}

HRESULT GraphicsDX11::createIndexBuffer(uint numElements)
{
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.MiscFlags = 0;
	bufferDesc.ByteWidth = sizeof(uint) * numElements;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
#ifdef CPUTriangulation
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
#else
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
#endif

	HRESULT hr1 = renderData1.device->CreateBuffer(&bufferDesc, nullptr, &renderData1.indexBuffer);
	HRESULT hr2 = renderData2.device->CreateBuffer(&bufferDesc, nullptr, &renderData2.indexBuffer);
	if(FAILED(hr1) || FAILED(hr2))
	{
		MessageBox(hWnd, L"Failed to create the index buffer", L"ERROR", S_OK);
		return hr1;
	}

	renderData1.deviceContext->IASetIndexBuffer(renderData1.indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	renderData2.deviceContext->IASetIndexBuffer(renderData2.indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	triangleCount = numElements;

	return hr1;
}

HRESULT GraphicsDX11::setDispatchBuffer(uint numElements, uint byteStride, uint registerSpot, void **data)
{
	ID3D11Buffer *buffer = nullptr;

	D3D11_BUFFER_DESC buffDesc;
	ZeroMemory(&buffDesc, sizeof(buffDesc));
	buffDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
	buffDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	buffDesc.Usage = D3D11_USAGE_DEFAULT;
	buffDesc.StructureByteStride = byteStride;
	buffDesc.ByteWidth = byteStride * numElements;

	D3D11_SUBRESOURCE_DATA initData;
	ZeroMemory(&initData, sizeof(initData));
	initData.pSysMem = data;

	HRESULT hr;
	if(activeContext == 1)
		hr = renderData2.device->CreateBuffer(&buffDesc, data == nullptr ? nullptr : &initData, &buffer);
	else
		hr = renderData1.device->CreateBuffer(&buffDesc, data == nullptr ? nullptr : &initData, &buffer);

	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to create buffer for compute shader", L"ERROR", S_OK);
		return hr;
	}

	hr = createShaderResource(buffer, numElements, registerSpot, activeContext == 1 ? renderData2 : renderData1);

	if(FAILED(hr))
	{
		MessageBox(hWnd, L"Failed to set buffers for compute shader", L"ERROR", S_OK);
		return hr;
	}

	return S_OK;
}

HRESULT GraphicsDX11::createShaderResource(ID3D11Buffer* buffer, uint numElements, int registerSpot, RenderStruct &data)
{
	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	desc.Format = DXGI_FORMAT_UNKNOWN;
	desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	desc.Buffer.FirstElement = 0;
	desc.Buffer.Flags = 0;
	desc.Buffer.NumElements = numElements;

	ID3D11UnorderedAccessView* pUAV = nullptr;
	HRESULT hr = data.device->CreateUnorderedAccessView(buffer, &desc, &pUAV);
	if(FAILED(hr))
		return hr;

	data.deviceContext->CSSetUnorderedAccessViews(registerSpot, 1, &pUAV, nullptr);

	SAFE_RELEASE(buffer);
	SAFE_RELEASE(pUAV);

	return S_OK;
}

HRESULT GraphicsDX11::dispatchTriangulation(uint numPatches)
{
	if(activeContext == 1)
	{
		renderData2.deviceContext->Begin(renderData2.triangulationQuery);

		renderData2.timer->start();
		renderData2.delaunay->Set();
		renderData2.deviceContext->Dispatch(std::ceil(numPatches / 1024), 1, 1);
		renderData2.timer->stop();
		
		renderData2.deviceContext->CopyResource(renderData2.indexBuffer, renderData2.dispatchIndexBuffer);

		renderData2.deviceContext->End(renderData2.triangulationQuery);
	}
	else
	{
		renderData1.deviceContext->Begin(renderData1.triangulationQuery);

		renderData1.timer->start();
		renderData1.delaunay->Set();
		renderData1.deviceContext->Dispatch(std::ceil(numPatches / 1024), 1, 1);
		renderData1.timer->stop();

		renderData1.deviceContext->CopyResource(renderData1.indexBuffer, renderData1.dispatchIndexBuffer);

		renderData1.deviceContext->End(renderData1.triangulationQuery);
	}

	return S_OK;
}

HRESULT GraphicsDX11::clearIndexBuffer()
{
	SAFE_RELEASE(activeContext == 1 ? renderData2.dispatchIndexBuffer : renderData1.dispatchIndexBuffer);

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;
	bufferDesc.ByteWidth = sizeof(uint) * triangleCount;
	bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;

	HRESULT hr;
	if(activeContext == 1)
		hr = renderData2.device->CreateBuffer(&bufferDesc, nullptr, &renderData2.dispatchIndexBuffer);
	else
		hr = renderData1.device->CreateBuffer(&bufferDesc, nullptr, &renderData1.dispatchIndexBuffer);
	if(FAILED(hr))
		return hr;

	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	desc.Buffer.FirstElement = 0;
	desc.Format = DXGI_FORMAT_R32_TYPELESS; // Format must be DXGI_FORMAT_R32_TYPELESS, when creating Raw Unordered Access View
	desc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
	desc.Buffer.NumElements = triangleCount;
	ID3D11UnorderedAccessView* pUAV = nullptr;

	if(activeContext == 1)
		hr = renderData2.device->CreateUnorderedAccessView(renderData2.dispatchIndexBuffer, &desc, &pUAV);
	else
		hr = renderData1.device->CreateUnorderedAccessView(renderData1.dispatchIndexBuffer, &desc, &pUAV);
	if(FAILED(hr))
		return hr;

	if(activeContext == 1)
		renderData2.deviceContext->CSSetUnorderedAccessViews(0, 1, &pUAV, nullptr);
	else
		renderData1.deviceContext->CSSetUnorderedAccessViews(0, 1, &pUAV, nullptr);

	return hr;
}

void GraphicsDX11::update()
{
	if(toggleWireframe.isDown(true))
	{
		renderState = renderState == 0 ? 1 : 0;

		lockDeviceContext();
		if(activeContext == 1)
			renderData1.deviceContext->RSSetState(renderState == 0 ? renderData1.rasterizerFilled : renderData1.rasterizerWireframe);
		else
			renderData2.deviceContext->RSSetState(renderState == 0 ? renderData2.rasterizerFilled : renderData2.rasterizerWireframe);
		unlockDeviceContext();
	}
	if(togglePatchColour.isDown(true))
	{
		// 3 different states
		// - regular rendering
		// - each patch rendere with different colour
		// - updated patches rendered with one colour, others with another colour
		colourState = colourState == 0 ? 1 : colourState == 1 ? 2 : 0;

		lockDeviceContext();
		if(activeContext == 1)
			renderData1.deviceContext->PSSetShader(colourState == 0 ? renderData1.pixel : renderData1.pixelPatchColours, nullptr, 0);
		else
			renderData2.deviceContext->PSSetShader(colourState == 0 ? renderData2.pixel : renderData2.pixelPatchColours, nullptr, 0);
		unlockDeviceContext();
	}
}

HRESULT GraphicsDX11::renderTerrain(std::vector<std::pair<uint, uint>> *patches)
{
	//lockDeviceContext();

	ID3D11DeviceContext *&currentContext = activeContext == 1 ? renderData1.deviceContext : renderData2.deviceContext;
	float clear[4] = { 0.2f, 0.2f, 0.2f, 1.f };
	//if(activeContext == 1)
	{
		renderData1.deviceContext->ClearRenderTargetView(renderData1.renderTargetView, clear);
		renderData1.deviceContext->ClearDepthStencilView(renderData1.depthStencilView, D3D11_CLEAR_DEPTH, 1.f, 0);
	}
	//else
	{
		renderData2.deviceContext->ClearRenderTargetView(renderData2.renderTargetView, clear);
		renderData2.deviceContext->ClearDepthStencilView(renderData2.depthStencilView, D3D11_CLEAR_DEPTH, 1.f, 0);
	}

	//deviceContext1->ClearRenderTargetView(renderTargetView, clear);
	//deviceContext1->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH, 1.f, 0);
	//deviceContext2->ClearRenderTargetView(renderTargetView, clear);
	//deviceContext2->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH, 1.f, 0);

	//#ifndef CPUTriangulation
	//	std::stringstream ss;
	//	ss << timer->getTime();
	//	SetWindowTextA(hWnd, ss.str().c_str());
	//#endif

	//ID3D11DeviceContext *&currentContext = activeContext == 2 ? deviceContext1 : deviceContext2;
	//if(activeContext == 1)
	//	deviceContext1->DrawIndexed(triangleCount, 0, 0);
	//else
	//	deviceContext2->DrawIndexed(triangleCount, 0, 0);

	//currentContext->DrawIndexed(triangleCount, 0, 0);

	if(colourState == 1)
	{
		for(uint i = 0; i < patches->size(); i++)
		{
			for(uint j = 0; j < patches->at(i).first; j++)
			{
				CBColour cb;
				cb.colour = colours[patches->at(i).second + j];
				currentContext->UpdateSubresource(activeContext == 1 ? renderData1.cbColour : renderData2.cbColour, 0, NULL, &cb, 0, 0);

				currentContext->DrawIndexed(regionSize, (patches->at(i).second + j) * regionSize, 0);

				//currentContext->UpdateSubresource(cbColour, 0, NULL, &cb, 0, 0);
				//
				//currentContext->DrawIndexed(regionSize, (patches->at(i).second + j) * regionSize, 0);
			}
		}
	}
	else if(colourState == 2)
	{
		for(uint i = 0; i < patches->size(); i++)
		{
			for(uint j = 0; j < patches->at(i).first; j++)
			{
				CBColour cb;
				if(updatedPatches[patches->at(i).second + j] != -1)
					cb.colour = elm::vec3(0.15, 0.6, 0.06);
				else
					cb.colour = elm::vec3(0.95, 0, 0);
				currentContext->UpdateSubresource(activeContext == 1 ? renderData1.cbColour : renderData2.cbColour, 0, NULL, &cb, 0, 0);

				currentContext->DrawIndexed(regionSize, (patches->at(i).second + j) * regionSize, 0);
			}
		}
	}
	else
	{
		for(uint i = 0; i < patches->size(); i++)
			currentContext->DrawIndexed(patches->at(i).first * regionSize, patches->at(i).second * regionSize, 0);

	}

	//unlockDeviceContext();

	if(activeContext == 1)
	{
		if(FAILED(renderData1.swapChain->Present(0, 0)))
			return E_FAIL;
	}
	else
	{
		if(FAILED(renderData2.swapChain->Present(0, 0)))
			return E_FAIL;
	}


	//if(FAILED(activeContext == 1 ? renderData1.swapChain->Present(0, 0) : renderData2.swapChain->Present(0, 0)))
	//	return E_FAIL;

	return S_OK;
}

GraphicsDX11::~GraphicsDX11()
{
//#ifndef CPUTriangulation
//	delaunay1->Unset();
//	delaunay2->Unset();
//#endif
//
//	SAFE_DELETE(timer1);
//	SAFE_DELETE(timer2);
//	SAFE_DELETE(computeSys1);
//	SAFE_DELETE(computeSys2);
//	SAFE_DELETE(delaunay1);
//	SAFE_DELETE(delaunay2);
//	
//	SAFE_RELEASE(depthStencilState);
//	SAFE_RELEASE(rasterizerFilled);
//	SAFE_RELEASE(rasterizerWireframe);
//	SAFE_RELEASE(blendDisable);
//	SAFE_RELEASE(renderTargetView);
//	SAFE_RELEASE(depthStencilView);
//
//	SAFE_RELEASE(sampler);
//	SAFE_RELEASE(swapChain);
//	SAFE_RELEASE(device);		
//	SAFE_RELEASE(deviceContext1);
//	SAFE_RELEASE(deviceContext2);
//	
//	SAFE_RELEASE(cbTriangulationConstants);
//	SAFE_RELEASE(cbOnce);
//	SAFE_RELEASE(cbOnMove);
//	SAFE_RELEASE(indexBuffer1);
//	SAFE_RELEASE(indexBuffer2);
//	SAFE_RELEASE(vertexBuffer);
//	SAFE_RELEASE(dispatchIndexBuffer);
//		
//	SAFE_RELEASE(vertex);
//	SAFE_RELEASE(pixel);

	instance = nullptr;
}