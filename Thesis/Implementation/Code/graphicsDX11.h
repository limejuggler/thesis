#ifndef _GRAPHICS_DX11_
#define _GRAPHICS_DX11_

// If this flag is set, then none of the compute shaders will be dispatched or even compiled
#define CPUTriangulation

#include <D3D11.h>
#include <mutex>

#include "key.h"
#include "stdafx.h"
#include "computeHelp.h"
#include "timer.h"

// Declared here, defined in main
extern HWND hWnd;

struct CBOnce
{
	elm::mat4 projection;
};

struct CBOnMove
{
	elm::mat4 view;
	elm::vec4 cameraPos;
};

struct CBTriangulationConstants
{
	uint		offset;
	float		patchDimX,
				patchDimZ,
				padding;
};

struct CBColour
{
	elm::vec3 colour;
	float padding;
};

struct Vertex
{
	elm::vec3	pos,
				norm;
};

class GraphicsDX11
{
private:
	static GraphicsDX11			*instance;

	struct RenderStruct
	{
		IDXGISwapChain				*swapChain;
		ID3D11Device				*device;
		ID3D11DeviceContext			*deviceContext;

		ID3D11DepthStencilState		*depthStencilState;
		ID3D11BlendState			*blendDisable;
		ID3D11RasterizerState		*rasterizerFilled,
									*rasterizerWireframe;
		ID3D11RenderTargetView		*renderTargetView;
		ID3D11DepthStencilView		*depthStencilView;
		ID3D11SamplerState			*sampler;

		ID3D11VertexShader			*vertex;
		ID3D11PixelShader			*pixel,
									*pixelPatchColours;
		ID3D11Buffer				*cbOnce,
									*cbOnMove,
									*cbTriangulationConstants,
									*cbColour,
									*indexBuffer,
									*dispatchIndexBuffer,
									*vertexBuffer;

		ID3D11Query					*triangulationQuery;

		ComputeWrap					*computeSys;
		ComputeShader				*delaunay;

		DispatchTimer				*timer;
	};

	//IDXGISwapChain				*swapChain;
	//ID3D11Device				*device;
	//ID3D11DeviceContext			*deviceContext1,
	//							*deviceContext2;
	//
	//ID3D11DepthStencilState		*depthStencilState;
	//ID3D11BlendState			*blendDisable;
	//ID3D11RasterizerState		*rasterizerFilled,
	//							*rasterizerWireframe;
	//ID3D11RenderTargetView		*renderTargetView;
	//ID3D11DepthStencilView		*depthStencilView;
	//ID3D11SamplerState			*sampler;
	//
	//ID3D11VertexShader			*vertex;
	//ID3D11PixelShader			*pixel,
	//							*pixelPatchColours;
	//ID3D11Buffer				*cbOnce,
	//							*cbOnMove,
	//							*cbTriangulationConstants,
	//							*cbColour,
	//							*indexBuffer1,
	//							*indexBuffer2,
	//							*dispatchIndexBuffer,
	//							*vertexBuffer;
	//
	//ID3D11Query					*triangulationQuery;
	//
	//ComputeWrap					*computeSys1,
	//							*computeSys2;
	//ComputeShader				*delaunay1,
	//							*delaunay2;
	//
	//DispatchTimer				*timer1,
	//							*timer2;

	Key							toggleWireframe,
								togglePatchColour;

	std::mutex					deviceContextMutex;

	HINSTANCE					hInst;

	RenderStruct				renderData1,
								renderData2;

	uint						regionSize;

	int							prevFps,
								fps,
								renderState,
								colourState,
								triangleCount,
								activeContext;

	double						fpsFreq,
								dispatchFreq,
								lastDispatchTime;

	std::vector<int>			updatedPatches;
	std::vector<elm::vec3>		colours;

	GraphicsDX11();
	HRESULT init(RenderStruct &data);

	HRESULT createShaderResource(ID3D11Buffer* buffer, uint numElements, int registerSpot, RenderStruct &data);
public:
	~GraphicsDX11();

	void updateCBuffer(CBOnMove *cb);
	void setCBufferOnce(CBOnce *cb);
	void setCBTriangulationConstants(CBTriangulationConstants *cb);

	//void copyIndexBufferData(uint start, uint steps);
	void swapIndexBuffer();
	void setAndCreateTexture(int texRegister, std::wstring filePath);
	void setUpdatedPatches(std::vector<int> patches) { updatedPatches = patches; }

	void update();

	void lockDeviceContext()					{ /*if(activeContext == 1) deviceContextMutex.lock();*/ }
	void unlockDeviceContext()					{ /*if(activeContext == 1) deviceContextMutex.unlock();*/ }

	// Return true when the dispatch call has completed, so that the buffer copy may be conducted on the index buffer
	// that is currently bound
	bool isTriangulationCompleted();

	HRESULT renderTerrain(std::vector<std::pair<uint, uint>> *patches);

	// Clears the index buffer used in the dispatch call to the GPU
	HRESULT clearIndexBuffer();

	HRESULT setDispatchBuffer(uint numElements, uint byteStride, uint registerSpot, void **data = nullptr);
	HRESULT dispatchTriangulation(uint numPatches);

	HRESULT createVertexBuffer(uint numElements, uint byteStride, void **data);
	// CPU triangulation
	HRESULT createIndexBuffer(uint numElements);
	// GPU triangulation
	//HRESULT initiateIndexBuffer(uint numPatches, uint indexBufferOffset);

	static GraphicsDX11 *getInstance();

	HWND getHwnd()								{ return hWnd; }
	ID3D11Device *getDevice()					{ return activeContext == 1 ? renderData1.device : renderData2.device; }
	ID3D11Buffer *getIndexBuffer()				{ return activeContext == 1 ? renderData1.indexBuffer : renderData2.indexBuffer; }
	ID3D11DeviceContext *getDeviceContext()		{ return activeContext == 1 ? renderData1.deviceContext : renderData2.deviceContext; }
};

#endif