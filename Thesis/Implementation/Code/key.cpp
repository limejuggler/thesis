#include <Windows.h>

#include "key.h"

Key::Key(int keyCode)
{
	this->keyCode = keyCode;

	lastState = 0;
}

bool Key::isDown(bool previousFrameUp)
{
	bool result = false;
	bool state = GetAsyncKeyState(keyCode) != 0;

	if(state)
		result = previousFrameUp ? !lastState : true;

	lastState = state;

	return result;
}

Key::~Key()
{
}
