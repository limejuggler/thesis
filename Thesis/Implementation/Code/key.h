#ifndef _KEY_
#define _KEY_

class Key
{
private:
	int keyCode;
	bool lastState;

public:
	Key() {}
	Key(int keyCode);
	~Key();

	bool isDown(bool previousFrameUp = false);
};

#endif