#include <crtdbg.h>
#include <sstream>

#include "terrainManager.h"
#include "camera.h"
#include "pointSelection.h"
#include "graphicsDX11.h"

HINSTANCE				hInst = nullptr;
// Defined here, declared in graphicsDX11.h
HWND					hWnd = nullptr;

HRESULT             InitWindow(HINSTANCE hInstance, int nCmdShow);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	if(FAILED(InitWindow(hInstance, nCmdShow)))
		return 0;

	GraphicsDX11 *dx = GraphicsDX11::getInstance();
	Camera *camera = new Camera();
	TerrainManager *manager = new TerrainManager(camera->getPos());

	camera->setTerrainHeights(manager->getHeights());

	RECT rc;
	GetWindowRect(hWnd, &rc);
	camera->setMouseLock(rc.left, rc.top);

	__int64 currTimeStamp = 0, prevTimeStamp, cntsPerSec = 0;

	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	double dt, time = 0;
	double secsPerCnt = 1.0 / (double)cntsPerSec;

	QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);

	bool activeWindow = true;

	double timerer = 0;
	int counter = 0;

	ShowCursor(false);

	// Main message loop
	MSG _msg = { 0 };
	while(WM_QUIT != _msg.message)
	{
		prevTimeStamp = currTimeStamp;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);

		time += dt = (currTimeStamp - prevTimeStamp) * secsPerCnt;

		//if(time < 0.016666666666666666)  // 60 fps
		//	Sleep((DWORD)(16.666666666666666 - time * 1000));

		if(PeekMessage(&_msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&_msg);
			DispatchMessage(&_msg);
		}
		else// if(time > 0.016) // 60 fps
		{
			if(GetActiveWindow() == hWnd)
			{
				if(!activeWindow)
				{
					activeWindow = true;
					ShowCursor(false);

					RECT rc;
					GetWindowRect(hWnd, &rc);
					camera->setMouseLock(rc.left, rc.top);
				}

				camera->update(time);
				manager->update();
				dx->update();
			}
			else
			{
				QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
				prevTimeStamp = currTimeStamp;

				if(activeWindow)
				{
					activeWindow = false;
					ShowCursor(true);
				}
			}

			dx->renderTerrain(manager->getPatches(camera->getFrustum()));

			counter++;
			timerer += time;

			if(timerer >= 1.0)
			{
				std::stringstream ss;
				ss << counter;
				//ss << triList.size();
				SetWindowTextA(hWnd, ss.str().c_str());
				
				timerer = 0;
				counter = 0;
			}

			time = 0;
		}
	}

	SAFE_DELETE(camera);
	SAFE_DELETE(manager);
	SAFE_DELETE(dx);

	_CrtDumpMemoryLeaks();
	return (int)_msg.wParam;
}

HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"Point selection";
	wcex.hIconSm = 0;
	if(!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	hInst = hInstance;
	RECT rc = { 0, 0, screenWidth, screenHeight };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

	if(!(hWnd = CreateWindow(L"Point selection",
		L"Dynamic multi-resolution TIN",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		rc.right - rc.left,
		rc.bottom - rc.top,
		NULL,
		NULL,
		hInstance,
		NULL)))
	{
		return E_FAIL;
	}

	ShowWindow(hWnd, nCmdShow);

	return S_OK;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch(message)
	{
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			EndPaint(hWnd, &ps);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
				PostQuitMessage(0);
				break;
			}
			break;

		default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}