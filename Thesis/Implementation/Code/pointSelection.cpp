#include "pointSelection.h"
#include "settings.h"

#define reductionFactorNormal 0.875f
#define reductionFactorBorder 0.65f

std::vector<std::vector<uint>> calculatePatchedIndices(	const std::vector<elm::vec3> &points, 
														const std::vector<float> &pixelErrors,
														const std::vector<int> &updatedPatches)
{
	elm::vec3 startPos(0, 0, 0);

	Settings *settingsCopy = Settings::getInstance();

	float pointStep = settingsCopy->pointStep;

	uint width = settingsCopy->width;
	uint height = settingsCopy->height;
	uint nSplitsX = width / settingsCopy->patchDimensionX;
	uint nSplitsZ = height / settingsCopy->patchDimensionY;

	uint xChunk = settingsCopy->patchDimensionX;
	uint zChunk = settingsCopy->patchDimensionY;

	float minCond = settingsCopy->minCondition;
	float maxCond = settingsCopy->maxCondition;

	std::vector<std::vector<uint>> patches(nSplitsX * nSplitsZ);

	auto average = [&points, &width, &height](uint id)->float
	{
		return (points[id - width - 1].y + points[id - width].y + points[id - width + 1].y +
				points[id - 1].y + points[id].y * 3 + points[id + 1].y +
				points[id + width - 1].y + points[id + width].y + points[id + width + 1].y) / 11;
	};

	// Temporary points to make the code more perspicuous
	float p1, p2, p3;

	int patchID;
	float xPoint, zPoint;
	double closestDist, dist;
	uint closestPointID1, closestPointID2, previousID;

	float maxDist = sqrt(width * pointStep * width * pointStep + height * pointStep * height * pointStep), condition;

	/****************************************************
						Bottom frame
	****************************************************/

	patches[0].push_back(0);

	previousID = 0;

	float heightDiffX, prevHeightDiffX = (points[1].y - points[0].y) / pointStep, stepTimesDistX = pointStep;
	for(uint i = 2; i < width; i++)
	{
		patchID = (i - 1) / xChunk;
		//if(updatedPatches[patchID] == -1 && (i - 1) % xChunk != 0)
		//	continue;
		//else if(updatedPatches[patchID] == -1 && (i - 1) % xChunk == 0 && updatedPatches[patchID - 1] == -1)
		//	continue;

		p1 = points[i].y;
		p2 = points[i - 1].y;
		heightDiffX = (p1 - p2) / pointStep;

		if((i - 1) % xChunk == 0)
			condition = updatedPatches[patchID - 1] == -1 ? pixelErrors[patchID - 1] : _min(	pixelErrors[patchID],
																								pixelErrors[patchID - 1]);
		else
			condition = pixelErrors[patchID];

		if((i - 1) % xChunk == 0)
		{
			previousID = i - 1;

			patches[patchID - 1].push_back(previousID);
			patches[patchID].push_back(previousID);

			stepTimesDistX = 0;
		}
		else if(abs(heightDiffX - prevHeightDiffX) > condition ||
			abs(heightDiffX - (p1 - points[previousID].y) / (stepTimesDistX * powf(reductionFactorBorder, stepTimesDistX / pointStep))) > condition)
		{
			previousID = i - 1;

			patches[patchID].push_back(previousID);

			stepTimesDistX = 0;
		}

		prevHeightDiffX = heightDiffX;
		stepTimesDistX += pointStep;
	}

	patches[nSplitsX - 1].push_back(width - 1);

	/*-------------------------------------------------*/

	// Compute left and right borders separately and during different times 
	uint previousBorderIDLeft = 0, previousBorderIDRight = patches[nSplitsX - 1].back();
	float heightDiffZLeft, prevHeightDiffZLeft, stepTimesDistZLeft;
	float heightDiffZRight, prevHeightDiffZRight, stepTimesDistZRight;

	stepTimesDistX = stepTimesDistZLeft = stepTimesDistZRight = pointStep;
	prevHeightDiffZLeft = (points[width].y - points[previousBorderIDLeft].y) / pointStep;
	prevHeightDiffZRight = (points[width * 2 - 1].y - points[previousBorderIDRight].y) / pointStep;
	for(uint i = 2; i < height; i++)
	{
		/****************************************************
							Left frame
		****************************************************/
		patchID = ((i - 1) / zChunk) * nSplitsX;
		//if(updatedPatches[patchID] != -1)
		//{
			p1 = points[i * width].y;
			p2 = points[(i - 1) * width].y;
			heightDiffZLeft = (p1 - p2) / pointStep;

			if((i - 1) % zChunk == 0)
				condition = updatedPatches[patchID - nSplitsX] == -1 ? pixelErrors[patchID - nSplitsX] : _min(	pixelErrors[patchID],
																												pixelErrors[patchID - nSplitsX]);
			else
				condition = pixelErrors[patchID];

			if((i - 1) % zChunk == 0)
			{
				previousID = previousBorderIDLeft = (i - 1) * width;

				patches[patchID - nSplitsX].push_back(previousBorderIDLeft);
				patches[patchID].push_back(previousBorderIDLeft);

				stepTimesDistZLeft = 0;
			}
			else if(abs(heightDiffZLeft - prevHeightDiffZLeft) > condition ||
				abs(heightDiffZLeft - (p1 - points[previousBorderIDLeft].y) / (stepTimesDistZLeft * powf(reductionFactorBorder, stepTimesDistZLeft / pointStep))) > condition)
			{
				previousID = previousBorderIDLeft = (i - 1) * width;

				patches[patchID].push_back(previousBorderIDLeft);

				stepTimesDistZLeft = 0;
			}

			prevHeightDiffZLeft = heightDiffZLeft;
			stepTimesDistZLeft += pointStep;
		//}
		/*-------------------------------------------------*/

		/****************************************************
				All points not part of the frame
		****************************************************/

		stepTimesDistX = pointStep;
		prevHeightDiffX = (points[(i - 1) * width + 1].y - points[(i - 1) * width].y) / pointStep;
		for(uint j = 2; j < width; j++)
		{
			patchID = ((i - 1) / zChunk) * nSplitsX + (j - 1) / xChunk;
			//if(updatedPatches[patchID] == -1 && (j - 1) % xChunk != 0)
			//	continue;
			//else if(updatedPatches[patchID] == -1 && (j - 1) % xChunk == 0 && updatedPatches[patchID - 1] == -1)
			//	continue;

			//if(updatedPatches[patchID] == -1 && updatedPatches[patchID] == -1)
			//else if(updatedPatches[patchID] == -1 && (i - 1) % zChunk != 0 && updatedPatches[patchID - nSplitsX] == -1)
			//	continue;

			xPoint = startPos.x + (j - 1) * pointStep;
			zPoint = startPos.z + (i - 1) * pointStep;

			// Corners, add the index to all 4 patches sharing the corner
			if((i - 1) % zChunk == 0 && (j - 1) % xChunk == 0)
			{
				previousID = (i - 1) * width + j - 1;

				patches[patchID - 1].push_back(previousID);
				patches[patchID].push_back(previousID);
				patches[patchID - nSplitsX - 1].push_back(previousID);
				patches[patchID - nSplitsX].push_back(previousID);

				prevHeightDiffX = (points[(i - 1) * width + j].y - points[previousID].y) / pointStep;
				stepTimesDistX = pointStep;

				continue;
			}
			else if((i - 1) % (zChunk / 2) == 0 && (j - 1) % xChunk == 0 || (j - 1) % (xChunk / 2) == 0 && (i - 1) % zChunk == 0)
			{
				previousID = (i - 1) * width + j - 1;

				if((j - 1) % xChunk == 0)
					patches[patchID - 1].push_back(previousID);
				else if((i - 1) % zChunk == 0)
					patches[patchID - nSplitsX].push_back(previousID);

				patches[patchID].push_back(previousID);

				prevHeightDiffX = (points[(i - 1) * width + j].y - points[previousID].y) / pointStep;
				stepTimesDistX = pointStep;

				continue;
			}

			std::vector<uint> &currentPatch = patches[patchID];

			closestDist = 1.0e+12;
			// There will always be at least one point of which to check against
			closestPointID1 = 0;
			closestPointID2 = -1;
			for(uint k = currentPatch.size() - 1; k > 0; k--)
			{
				if(points[currentPatch[k]].z == zPoint || ((i - 1) % zChunk == 0 && points[currentPatch[k]].x != xPoint))
					continue;

				p1 = points[currentPatch[k]].x - xPoint;
				p2 = points[currentPatch[k]].z - zPoint;
				// Don't use square root, as it is a slow operation (around 30% increase in computation time (debug))
				dist = p1 * p1 + p2 * p2;
				if(dist <= closestDist)
				{
					//if(dist == closestDist && abs(p2 - points[closestPointID1].y) > abs(p2 - points[k].y))
					//	closestPointID1 = k;
					//else if(dist < closestDist)
					//	closestPointID1 = k;
					dist < closestDist ? closestPointID1 = k : closestPointID2 = k;

					closestDist = dist;
				}

				// Problem identified: loop breaks too early, thereby returning the wrong point

				// closestDist * closestDist seams to take care of the problem, but it is not an optimal solution
				if(dist > closestDist * closestDist)
					break;
			}

			p1 = points[(i - 1) * width + j].y;
			p2 = points[(i - 1) * width + j - 1].y;
			p3 = points[i * width + j - 1].y;
			heightDiffX = (p1 - p2) / pointStep;

			// The patch with the lowest pixel error dictates which pixel error is used,
			// unless a patch remains unchanged, then it takes presidence
			if((i - 1) % zChunk == 0 || (j - 1) % xChunk == 0)
			{
				if((j - 1) % xChunk == 0)
				{
					if(updatedPatches[patchID] == -1 && updatedPatches[patchID - 1] == -1)
						condition = _min(pixelErrors[patchID], pixelErrors[patchID - 1]);

					else if(updatedPatches[patchID] == -1)				condition = pixelErrors[patchID];
					else if(updatedPatches[patchID - 1] == -1)			condition = pixelErrors[patchID - 1];
					else												condition = _min(	pixelErrors[patchID],
																							pixelErrors[patchID - 1]);
				}
				else
				{
					if(updatedPatches[patchID] == -1 && updatedPatches[patchID - nSplitsX] == -1)
						condition = _min(pixelErrors[patchID], pixelErrors[patchID - nSplitsX]);

					else if(updatedPatches[patchID] == -1)				condition = pixelErrors[patchID];
					else if(updatedPatches[patchID - nSplitsX] == -1)	condition = pixelErrors[patchID - nSplitsX];
					else												condition = _min(	pixelErrors[patchID], 
																							pixelErrors[patchID - nSplitsX]);
				}
				//	condition = updatedPatches[patchID - 1] == -1 ? pixelErrors[patchID - 1] : 
				//													_min(	pixelErrors[patchID],
				//															pixelErrors[patchID - 1]);
				//else
				//	condition = updatedPatches[patchID - nSplitsX] == -1 ? pixelErrors[patchID - nSplitsX] :
				//															_min(	pixelErrors[patchID],
				//																	pixelErrors[patchID - nSplitsX]);
			}
			else
				condition = pixelErrors[patchID];

			if(closestPointID2 != -1 && abs(p2 - points[currentPatch[closestPointID1]].y) < abs(p2 - points[currentPatch[closestPointID2]].y))
				closestPointID1 = closestPointID2;

			float distFactor1, distFactor2, len = elm::vecLength(elm::vec2(xPoint, zPoint) - points[currentPatch[closestPointID1]].xz);
			if((i - 1) % zChunk == 0 || (j - 1) % xChunk == 0)
			{
				distFactor1 = powf(reductionFactorNormal, stepTimesDistX / pointStep - 2) * reductionFactorBorder;
				distFactor2 = powf(reductionFactorNormal, len / pointStep - 2) * reductionFactorBorder;
			}
			else
			{
				distFactor1 = powf(reductionFactorNormal, stepTimesDistX / pointStep - 1);
				distFactor2 = powf(reductionFactorNormal, len / pointStep - 1);
			}

			float diffs[] =
			{
				// X-axis
				abs(heightDiffX - prevHeightDiffX) - condition,
				abs(heightDiffX - (p1 - points[previousID].y) / (stepTimesDistX * distFactor1)) - condition,
				abs(heightDiffX - (p1 - points[(i - 1) * width].y) / (stepTimesDistX * distFactor1)) - condition,
				// Z-axis
				//abs(p3 - p2) / (stepTimesDistX * distanceReductionFactor) - condition,
				abs(p3 - p2) / pointStep - condition,
				abs(p3 - points[currentPatch[closestPointID1]].y) / (len * distFactor2) - condition,
			};

			if(diffs[0] > 0 || (points[previousID].z == zPoint && diffs[1] > 0) || (points[previousID].z != zPoint && diffs[2] > 0) ||
				diffs[3] > 0 || diffs[4] > 0)
			{
				previousID = (i - 1) * width + j - 1;

				if((j - 1) % xChunk == 0)
					patches[patchID - 1].push_back(previousID);
				else if((i - 1) % zChunk == 0)
					patches[patchID - nSplitsX].push_back(previousID);

				currentPatch.push_back(previousID);

				stepTimesDistX = 0;
			}

			prevHeightDiffX = heightDiffX;
			stepTimesDistX += pointStep;
		}

		/*-------------------------------------------------*/

		/****************************************************
							Right frame
		****************************************************/
		patchID = ((i - 1) / zChunk + 1) * nSplitsX - 1;
		//if(updatedPatches[patchID] != -1)
		//{
			p1 = points[(i + 1) * width - 1].y;
			p2 = points[i * width - 1].y;
			heightDiffZRight = (p1 - p2) / pointStep;

			if((i - 1) % zChunk == 0)
				condition = updatedPatches[patchID - nSplitsX] == -1 ? pixelErrors[patchID - nSplitsX] :
																		_min(	pixelErrors[patchID],
																				pixelErrors[patchID - nSplitsX]);
			else
				condition = pixelErrors[patchID];

			if((i - 1) % zChunk == 0)
			{
				previousBorderIDRight = i * width - 1;

				patches[patchID].push_back(previousBorderIDRight);
				patches[patchID - nSplitsX].push_back(previousBorderIDRight);

				stepTimesDistZRight = 0;
			}
			else if(abs(heightDiffZRight - prevHeightDiffZRight) > condition ||
				abs(heightDiffZRight - (p1 - points[previousBorderIDRight].y) / (stepTimesDistZRight * powf(reductionFactorBorder, stepTimesDistZRight / pointStep))) > condition)
			{
				previousBorderIDRight = i * width - 1;

				patches[patchID].push_back(previousBorderIDRight);

				stepTimesDistZRight = 0;
			}

			prevHeightDiffZRight = heightDiffZRight;
			stepTimesDistZRight += pointStep;
		//}
		/*-------------------------------------------------*/
	}

	/****************************************************
						Top frame
	****************************************************/

	previousID = width * (height - 1);
	patches[nSplitsX * (nSplitsZ - 1)].push_back(previousID);

	prevHeightDiffX = (points[width * (height - 1) + 1].y - points[width * (height - 1)].y) / pointStep;
	stepTimesDistX = pointStep;
	for(uint i = 2; i < width; i++)
	{
		p1 = points[width * (height - 1) + i].y;
		p2 = points[width * (height - 1) + i - 1].y;
		heightDiffX = (p1 - p2) / pointStep;

		condition = pixelErrors[nSplitsX * (nSplitsZ - 1) + (i - 1) / xChunk];
		//currentDist = elm::vecLength(cameraPos->xz - (startPos.xz + elm::vec2((i - 1) * pointStep, (height - 1) * pointStep)));
		//condition = (currentDist / maxDist) * (maxCond - minCond) + minCond;

		if((i - 1) % xChunk == 0)
		{
			previousID = width * (height - 1) + i - 1;

			patches[nSplitsX * (nSplitsZ - 1) + (i - 1) / xChunk - 1].push_back(previousID);
			patches[nSplitsX * (nSplitsZ - 1) + (i - 1) / xChunk].push_back(previousID);

			stepTimesDistX = 0;
		}
		else if(abs(heightDiffX - prevHeightDiffX) > condition ||
			abs(heightDiffX - (p1 - points[previousID].y) / (stepTimesDistX * powf(reductionFactorBorder, stepTimesDistX / pointStep))) > condition)
		{
			previousID = width * (height - 1) + i - 1;

			patches[nSplitsX * (nSplitsZ - 1) + (i - 1) / xChunk].push_back(previousID);

			stepTimesDistX = 0;
		}

		prevHeightDiffX = heightDiffX;
		stepTimesDistX += pointStep;
	}

	patches[nSplitsZ * nSplitsX - 1].push_back(width * height - 1);

	/*-------------------------------------------------*/

	return patches;
}

std::vector<uint> calculateSinglePatchedIndices(const std::vector<elm::vec3> &points,
												float pixelError,
												uint x, uint y, SinglePatchInfo &info)
{
	elm::vec3 startPos(0, 0, 0);

	Settings *settingsCopy = Settings::getInstance();

	float pointStep = settingsCopy->pointStep;

	uint width = settingsCopy->width;
	uint height = settingsCopy->height;
	uint nSplitsX = width / settingsCopy->patchDimensionX;
	uint nSplitsZ = height / settingsCopy->patchDimensionY;

	uint xChunk = settingsCopy->patchDimensionX;
	uint zChunk = settingsCopy->patchDimensionY;

	float minCond = settingsCopy->minCondition;
	float maxCond = settingsCopy->maxCondition;

	std::vector<uint> patch;

	auto average = [&points, &width, &height](uint id)->float
	{
		return (points[id - width - 1].y + points[id - width].y + points[id - width + 1].y +
				points[id - 1].y + points[id].y * 3 + points[id + 1].y +
				points[id + width - 1].y + points[id + width].y + points[id + width + 1].y) / 11;
	};

	// Temporary points to make the code more perspicuous
	float p1, p2, p3;

	int patchID;
	float xPoint, zPoint;
	double closestDist, dist;
	uint closestPointID1, closestPointID2, previousID;

	float maxDist = sqrt(width * pointStep * width * pointStep + height * pointStep * height * pointStep), condition;

	/****************************************************
						Bottom frame
	****************************************************/

	previousID = y * width + x;
	patch.push_back(previousID);

	// Are there any points below?
	if(y > 0)
		//condition = info.stateBottom == 0 ? info.pixelErrorBottom : _min(info.pixelErrorBottom, pixelError);
		condition = info.stateBottom == 0 ? info.prevPEBottom : _min(info.pEBottom, pixelError);
	else
		condition = pixelError;

	float heightDiffX, prevHeightDiffX = (points[previousID + 1].y - points[previousID].y) / pointStep, stepTimesDistX = pointStep;
	for(uint i = x + 2; i < x + xChunk + 1; i++)
	{
		p1 = points[y * width + i].y;
		p2 = points[y * width + i - 1].y;
		heightDiffX = (p1 - p2) / pointStep;

		if(	abs(heightDiffX - prevHeightDiffX) > condition || //(i - 1) % (xChunk / 2) == 0 ||
			abs(heightDiffX - (p1 - points[previousID].y) / (stepTimesDistX * powf(reductionFactorBorder, stepTimesDistX / pointStep))) > condition)
		{
			previousID = y * width + i - 1;

			patch.push_back(previousID);
			//if(info.bottom)
			//	info.bottom->push_back(previousID);

			stepTimesDistX = 0;
		}

		prevHeightDiffX = heightDiffX;
		stepTimesDistX += pointStep;
	}

	patch.push_back(y * width + x + xChunk);

	/*-------------------------------------------------*/

	// Compute left and right borders separately and during different times 
	uint previousBorderIDLeft = y * width + x, previousBorderIDRight = patch.back();
	float heightDiffZLeft, prevHeightDiffZLeft, stepTimesDistZLeft;
	float heightDiffZRight, prevHeightDiffZRight, stepTimesDistZRight;

	stepTimesDistX = stepTimesDistZLeft = stepTimesDistZRight = pointStep;
	prevHeightDiffZLeft = (points[(y + 1) * width + x].y - points[previousBorderIDLeft].y) / pointStep;
	prevHeightDiffZRight = (points[(y + 1) * width + x + xChunk - 1].y - points[previousBorderIDRight].y) / pointStep;
	for(uint i = y + 2; i < y + zChunk + 1; i++)
	{
		/****************************************************
							Left frame
		****************************************************/

		p1 = points[i * width + x].y;
		p2 = points[(i - 1) * width + x].y;
		heightDiffZLeft = (p1 - p2) / pointStep;

		if(x > 0)
			//condition = info.stateLeft == 0 ? info.pixelErrorLeft : _min(info.pixelErrorLeft, pixelError);
			condition = info.stateLeft == 0 ? info.prevPELeft : _min(info.pELeft, pixelError);
		else
			condition = pixelError;
		
		if(	abs(heightDiffZLeft - prevHeightDiffZLeft) > condition || //(i - 1) % (zChunk / 2) == 0 ||
			abs(heightDiffZLeft - (p1 - points[previousBorderIDLeft].y) / (stepTimesDistZLeft * powf(reductionFactorBorder, stepTimesDistZLeft / pointStep))) > condition)
		{
			previousID = previousBorderIDLeft = (i - 1) * width + x;

			patch.push_back(previousID);
			//if(info.left)
			//	info.left->push_back(previousID);

			stepTimesDistZLeft = 0;
		}

		prevHeightDiffZLeft = heightDiffZLeft;
		stepTimesDistZLeft += pointStep;

		/*-------------------------------------------------*/

		/****************************************************
				All points not part of the frame
		****************************************************/

		condition = pixelError;
		stepTimesDistX = pointStep;
		prevHeightDiffX = (points[(i - 1) * width + x + 1].y - points[(i - 1) * width + x].y) / pointStep;
		for(uint j = x + 2; j < x + xChunk + 1; j++)
		{
			xPoint = startPos.x + (j - 1) * pointStep;
			zPoint = startPos.z + (i - 1) * pointStep;

			if((i - 1) % (zChunk / 2) == 0 && (j - 1) % (xChunk / 2) == 0)
			{
				previousID = (i - 1) * width + j - 1;
			
				patch.push_back(previousID);
			
				stepTimesDistX = pointStep;
				prevHeightDiffX = (points[(i - 1) * width + j].y - points[(i - 1) * width + j - 1].y) / pointStep;
				
				continue;
			}

			closestDist = 1.0e+12;
			// There will always be at least one point of which to check against
			closestPointID1 = 0;
			closestPointID2 = -1;
			for(uint k = patch.size() - 1; k > 0; k--)
			{
				if(points[patch[k]].z == zPoint)// || ((i - 1) % zChunk == 0 && points[patch[k]].x != xPoint))
					continue;

				p1 = points[patch[k]].x - xPoint;
				p2 = points[patch[k]].z - zPoint;
				// Don't use square root, as it is a slow operation (around 30% increase in computation time (debug))
				dist = p1 * p1 + p2 * p2;
				if(dist <= closestDist)
				{
					dist < closestDist ? closestPointID1 = k : closestPointID2 = k;

					closestDist = dist;
				}

				// Problem identified: loop breaks too early, thereby returning the wrong point

				// closestDist * closestDist seams to take care of the problem, but it is not an optimal solution
				if(dist > closestDist * closestDist)
					break;
			}

			p1 = points[(i - 1) * width + j].y;
			p2 = points[(i - 1) * width + j - 1].y;
			p3 = points[i * width + j - 1].y;
			heightDiffX = (p1 - p2) / pointStep;

			if(closestPointID2 != -1 && abs(p2 - points[patch[closestPointID1]].y) < abs(p2 - points[patch[closestPointID2]].y))
				closestPointID1 = closestPointID2;

			float distFactor1, distFactor2, len = elm::vecLength(elm::vec2(xPoint, zPoint) - points[patch[closestPointID1]].xz);

			distFactor1 = powf(reductionFactorNormal, stepTimesDistX / pointStep - 1);
			distFactor2 = powf(reductionFactorNormal, len / pointStep - 1);

			float diffs[] =
			{
				// X-axis
				abs(heightDiffX - prevHeightDiffX) - condition,
				abs(heightDiffX - (p1 - points[previousID].y) / (stepTimesDistX * distFactor1)) - condition,
				abs(heightDiffX - (p1 - points[(i - 1) * width + x].y) / (stepTimesDistX * distFactor1)) - condition,
				// Z-axis
				//abs(p3 - p2) / (stepTimesDistX * distanceReductionFactor) - condition,
				abs(p3 - p2) / pointStep - condition,
				abs(p3 - points[patch[closestPointID1]].y) / (len * distFactor2) - condition,
			};

			if(	diffs[0] > 0 || (points[previousID].z == zPoint && diffs[1] > 0) || (points[previousID].z != zPoint && diffs[2] > 0) ||
				diffs[3] > 0 || diffs[4] > 0)
			{
				previousID = (i - 1) * width + j - 1;

				patch.push_back(previousID);

				stepTimesDistX = 0;
			}

			prevHeightDiffX = heightDiffX;
			stepTimesDistX += pointStep;
		}

		/*-------------------------------------------------*/

		/****************************************************
							Right frame
		****************************************************/

		//p1 = points[(i + 1) * width - 1].y;
		//p2 = points[i * width - 1].y;
		p1 = points[i * width + x + xChunk].y;
		p2 = points[(i - 1) * width + x + xChunk].y;
		heightDiffZRight = (p1 - p2) / pointStep;

		if(x < width - xChunk)
			//condition = info.stateRight == 0 ? info.pixelErrorRight : _min(info.pixelErrorRight, pixelError);
			condition = info.stateRight == 0 ? info.prevPERight : _min(info.pERight, pixelError);
		else
			condition = pixelError;
		
		if(	abs(heightDiffZRight - prevHeightDiffZRight) > condition || //(i - 1) % (zChunk / 2) == 0 ||
			abs(heightDiffZRight - (p1 - points[previousBorderIDRight].y) / (stepTimesDistZRight * powf(reductionFactorBorder, stepTimesDistZRight / pointStep))) > condition)
		{
			previousBorderIDRight = (i - 1) * width + x + xChunk;

			patch.push_back(previousBorderIDRight);
			//if(info.right)
			//	info.right->push_back(previousBorderIDRight);

			stepTimesDistZRight = 0;
		}

		prevHeightDiffZRight = heightDiffZRight;
		stepTimesDistZRight += pointStep;

		/*-------------------------------------------------*/
	}

	/****************************************************
						Top frame
	****************************************************/

	previousID = (y + zChunk) * width + x;
	patch.push_back(previousID);

	if(y < height - zChunk)
		//condition = info.stateTop == 0 ? info.pixelErrorTop : _min(info.pixelErrorTop, pixelError);
		condition = info.stateTop == 0 ? info.prevPETop : _min(info.pETop, pixelError);
	else
		condition = pixelError;

	prevHeightDiffX = (points[previousID + 1].y - points[previousID].y) / pointStep;
	stepTimesDistX = pointStep;
	for(uint i = x + 2; i < x + xChunk + 1; i++)
	{
		p1 = points[(y + zChunk) * width + i].y;
		p2 = points[(y + zChunk) * width + i - 1].y;
		heightDiffX = (p1 - p2) / pointStep;
		
		if(	abs(heightDiffX - prevHeightDiffX) > condition || //(i - 1) % (xChunk / 2) == 0 ||
			abs(heightDiffX - (p1 - points[previousID].y) / (stepTimesDistX * powf(reductionFactorBorder, stepTimesDistX / pointStep))) > condition)
		{
			previousID = (y + zChunk) * width + i - 1;

			patch.push_back(previousID);
			//if(info.top)
			//	info.top->push_back(previousID);

			stepTimesDistX = 0;
		}

		prevHeightDiffX = heightDiffX;
		stepTimesDistX += pointStep;
	}

	patch.push_back((y + zChunk) * width + x + xChunk);

	/*-------------------------------------------------*/

	//																		//
	//	Set information regarding previous pixel error for further updates	//
	//																		//

	// Bottom
	if(y > 0)
		info.prevPEBottom = info.stateBottom == 0 ? info.prevPEBottom : _min(info.pEBottom, pixelError);
	else
		info.prevPEBottom = pixelError;

	// Left
	if(x > 0)
		info.prevPELeft = info.stateLeft == 0 ? info.prevPELeft : _min(info.pELeft, pixelError);
	else
		info.prevPELeft = pixelError;

	// Right
	if(x < width - xChunk)
		info.prevPERight = info.stateRight == 0 ? info.prevPERight : _min(info.pERight, pixelError);
	else
		info.prevPERight = pixelError;

	// Top
	if(y < height - zChunk)
		info.prevPETop = info.stateTop == 0 ? info.prevPETop : _min(info.pETop, pixelError);
	else
		info.prevPETop = pixelError;

	return patch;
}