#ifndef _POINT_SELECTION_
#define _POINT_SELECTION_

#include "settings.h"

struct SinglePatchInfo
{
	// pixel errors
	float				pETop,
						pEBottom,
						pELeft,
						pERight,
						prevPETop,
						prevPEBottom,
						prevPELeft,
						prevPERight;

	// 0: not updated since last update
	// 1: updated last update
	int					stateTop,
						stateBottom,
						stateLeft,
						stateRight;
};

std::vector<std::vector<uint>> calculatePatchedIndices(	const std::vector<elm::vec3> &points, 
														const std::vector<float> &pixelErrors, 
														const std::vector<int> &updatedPatches);

std::vector<uint> calculateSinglePatchedIndices(const std::vector<elm::vec3> &points,
												float pixelError,
												uint x, uint y, SinglePatchInfo &info);

#endif