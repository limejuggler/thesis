#define parentDirectory "../"

#define maximumPatchElements 4096
#define minimumPatchSize 8

#define defaultHeightMap "../Heightmaps/heightMap3.raw"
#define defaultWidth 256
#define defaultHeight 256
#define defaultPointStep 5.f
#define defaultHeightScale 1.f

#define defaultTexture1 "../Textures/grass.jpg"
#define defaultTexture2 "../Textures/rock.jpg"
#define defaultTexture3 "../Textures/hardgrass.jpg"
#define defaultBlendmap "../Textures/blendmap.png"

#define defaultUpdateDistance 250.f
#define defaultMinimumCondition 0.2f
#define defaultMaximumCondition 1.5f
#define defaultPatchDimensionX 32
#define defaultPatchDimensionY 32

#define defaultTriangulateDuringInit false

#include <fstream>
#include "settings.h"

Settings *Settings::instance = nullptr;

Settings::Settings()
{
	init();
}

Settings *Settings::getInstance()
{
	if(!instance)
		instance = new Settings();

	return instance;
}

void Settings::init()
{
	// Start by reading the settings file, if it cannot be located or if some values are missing from it,
	// then replace them with default values

	std::ifstream file = std::ifstream(parentDirectory + std::string("Settings.txt"), std::ios::in);

	if(!file.fail())
	{
		auto readComments = [&file]()
		{
			char buffer[512];

			while(file.peek() == '#' || file.peek() == '\n')
				file.getline(buffer, 512);
		};

		auto setFilePath = [](std::string &filePathOut, std::string directory)
		{
			std::string endSlash = directory.substr(directory.length() - 1, directory.length() - 1);
			if(endSlash != "/")
				directory += "/";

			filePathOut = parentDirectory + directory + filePathOut;
		};

		readComments();

		std::string directory;

		file >> heightmapName;
		file >> width;
		file >> height;
		file >> directory;
		file >> pointStep;
		file >> heightScale;

		setFilePath(heightmapName, directory);

		readComments();

		file >> texture1Name;
		file >> texture2Name;
		file >> texture3Name;
		file >> blendmapName;
		file >> directory;

		setFilePath(texture1Name, directory);
		setFilePath(texture2Name, directory);
		setFilePath(texture3Name, directory);
		setFilePath(blendmapName, directory);

		readComments();

		file >> updateDistance;
		file >> minCondition;
		file >> maxCondition;
		file >> patchDimensionX;
		file >> patchDimensionY;
		file >> triangulateDuringInit;

		// Make sure that the patch dimensions are reasonable
		auto isPowerOfTwo = [](uint patchDim)->bool
		{
			for(uint i = 0; (uint)pow(2.0, (int)i) <= patchDim; i++)
			{
				if(patchDim % (uint)pow(2.0, (int)i) != 0)
					return false;
			}

			return true;
		};

		if(patchDimensionX * patchDimensionY > maximumPatchElements)
		{
			patchDimensionX = defaultPatchDimensionX;
			patchDimensionY = defaultPatchDimensionY;
		}
		else
		{
			if(patchDimensionX < minimumPatchSize || !isPowerOfTwo(patchDimensionX))
				patchDimensionX = defaultPatchDimensionX;
			if(patchDimensionY < minimumPatchSize || !isPowerOfTwo(patchDimensionY))
				patchDimensionY = defaultPatchDimensionY;
		}
	}
	else
	{
		heightmapName = defaultHeightMap;
		width = defaultWidth;
		height = defaultHeight;
		pointStep = defaultPointStep;
		heightScale = defaultHeightScale;

		texture1Name = defaultTexture1;
		texture2Name = defaultTexture2;
		texture3Name = defaultTexture3;
		blendmapName = defaultBlendmap;

		updateDistance = defaultUpdateDistance;
		minCondition = defaultMinimumCondition;
		maxCondition = defaultMaximumCondition;
		patchDimensionX = defaultPatchDimensionX;
		patchDimensionY = defaultPatchDimensionY;

		triangulateDuringInit = defaultTriangulateDuringInit;
	}

	bufferOffset = (patchDimensionX + 1) * (patchDimensionY + 1) * 8;
}

Settings::~Settings()
{
	instance = nullptr;
}