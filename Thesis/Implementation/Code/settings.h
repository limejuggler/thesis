#ifndef _SETTINGS_
#define _SETTINGS_

#include "stdafx.h"

class Settings
{
private:
	static Settings *instance;

	void init();

	Settings();

public:
	std::string heightmapName,
				texture1Name,
				texture2Name,
				texture3Name,
				blendmapName;

	uint		width,
				height,
				patchDimensionX,
				patchDimensionY,
				bufferOffset;

	float		pointStep,
				updateDistance,
				minCondition,
				maxCondition,
				heightScale;

	bool		triangulateDuringInit;

	~Settings();

	static Settings *getInstance();
};

#endif