#ifndef _STDAFX_
#define _STDAFX_

//#include <Windows.h>
#include <iostream>
#include <elm\elm.hpp>
#include <vector>
#include <string>

typedef unsigned int uint;

#define screenWidth 1024
#define screenHeight 720

#define maxNeighbours 16

#define SAFE_DELETE(x)	if(x)	{ delete x;			x = nullptr; }
#define SAFE_RELEASE(x)	if(x)	{ (x)->Release();	x = nullptr; }

template<typename T>
std::string toString(T x)
{
	std::stringstream ss;
	ss << x;
	return ss.str();
}

//template<typename T>
//const char* toCString(T x)
//{
//	std::stringstream ss;
//	ss << x;
//	std::string s = ss.str();
//	return s.c_str();
//}

template<typename T1, typename T2, typename T3>
struct triple
{
	T1 first;
	T2 second;
	T3 third;

	triple()
	{
		first = T1();
		second = T2();
		third = T3();
	}
	triple(T1 t1, T2 t2, T3 t3)
	{
		first = t1;
		second = t2;
		third = t3;
	}
};

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#endif