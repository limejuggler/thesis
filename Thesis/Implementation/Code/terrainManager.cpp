#include <fstream>
#include <sstream>

#include <LodePNG/lodepng.h>
#include "terrainManager.h"
#include "graphicsDX11.h"
#include "CPUTriangulation.h"

TerrainManager::TerrainManager(const elm::vec3 *camPos)
{
	cameraPos = camPos;

	updateState = 1;
	cullingUpdateState = frustumCullingState = 0;

	bufferCopyLock = threadLock = false;

	timer = new CPUTimer();

	toggleFrustumCulling = Key(0x35);
	toggleCullingUpdate = Key(0x36);

	settings = Settings::getInstance();

	lastUpdatePos = *camPos - elm::vec3(settings->updateDistance, settings->updateDistance, settings->updateDistance);

	nSplitsX = settings->width / settings->patchDimensionX;
	nSplitsZ = settings->height / settings->patchDimensionY;
	nPatches = nSplitsX * nSplitsZ;

	pixelErrors = std::vector<float>(nPatches);
	patchPositions = std::vector<triple<elm::vec3, elm::vec3, uint>>(nPatches);
	pixelErrorInPatch = std::vector<std::pair<float, float>>(nPatches);
	minMaxHeights = std::vector<std::pair<float, float>>(nPatches);

	//maxIndicesInPatch = settings->patchDimensionX * settings->patchDimensionY * 6;
	maxIndicesInPatch = settings->bufferOffset;

	CBTriangulationConstants cb;
	cb.offset = maxIndicesInPatch;
	cb.patchDimX = settings->patchDimensionX * settings->pointStep;
	cb.patchDimZ = settings->patchDimensionY * settings->pointStep;
	GraphicsDX11::getInstance()->setCBTriangulationConstants(&cb);

	init();
}

void TerrainManager::init()
{
	std::vector<unsigned char> pointHeights;

	// Different behavious for raw and png files
	std::string fileType = settings->heightmapName;
	while(fileType.find(".") != std::string::npos)
		fileType = fileType.substr(fileType.find(".") + 1, fileType.length());

	if(fileType == "png")
	{
		uint w, h, error;
		error = lodepng::decode(pointHeights, w, h, settings->heightmapName);

		if(error)
			MessageBoxA(hWnd, "An error occured while reading the png image!", "ERROR", S_OK);

		if(settings->width != w || settings->height != h)
			MessageBoxA(hWnd, "Incorrect dimensions was specified for height map!", "ERROR", S_OK);
	}

	uint width = settings->width;
	uint height = settings->height;
	uint size = width * height;

	heightData = std::vector<float>(size);
	verts = std::vector<elm::vec3>(size);
	std::vector<elm::vec3> normals = std::vector<elm::vec3>(size);

	if(fileType == "raw")
	{
		pointHeights = std::vector<unsigned char>(size);

		std::ifstream file = std::ifstream(settings->heightmapName, std::ios::binary);

		file.read((char*)&pointHeights[0], (std::streamsize)(size));
		file.close();

		for(uint i = 0; i < height; i++)
			for(uint j = 0; j < width; j++)
				heightData[i * width + j] = (float)pointHeights[i * width + j] * settings->heightScale;
	}
	
	if(fileType == "png")
	{
		for(uint i = 0; i < height; i++)
			for(uint j = 0; j < width; j++)
				heightData[i * width + j] = (float)pointHeights[(i * width + j) * 4] * settings->heightScale;
	}

	// Calculate min and max height for each patch. Used in frustum culling
	uint dimY = settings->patchDimensionY;
	uint dimX = settings->patchDimensionX;

	float minHeight, maxHeight;
	for(uint i = 0; i < nSplitsZ; i++)
	{
		for(uint j = 0; j < nSplitsX; j++)
		{
			minHeight = 999999.f;
			maxHeight = -999999.f;
			for(uint y = i * dimY; y < (i + 1) * dimY; y++)
			{
				for(uint x = j * dimX; x < (j + 1) * dimX; x++)
				{
					if(		minHeight > heightData[y * width + x])	minHeight = heightData[y * width + x];
					else if(maxHeight < heightData[y * width + x])	maxHeight = heightData[y * width + x];
				}
			}
			minMaxHeights[i * nSplitsX + j] = std::pair<float, float>(minHeight, maxHeight);
		}
	}


	float pointStep = settings->pointStep;
	int index1, index2;
	elm::vec3 normal, p1, p2, p3;
	for(uint y = 0; y < height - 1; y++)
	{
		for(uint x = 0; x < width - 1; x++)
		{
			index1 = (int)(y		* width + x);
			index2 = (int)((y + 1)	* width + x);

			p1 = elm::vec3(x * pointStep, heightData[index1], y * pointStep);
			p2 = elm::vec3(x * pointStep, heightData[index2], (y + 1) * pointStep);
			p3 = elm::vec3((x + 1) * pointStep, heightData[index2 + 1], (y + 1) * pointStep);

			// Cross product between the two sides results in the normal for the triangle.
			normal = cross(p1 - p2, p1 - p3);

			// Adding the normal will give a vector pointing out from the vertexpoint.
			normals[index1] += normal;
			normals[index2] += normal;
			normals[index2 + 1] += normal;

			p2 = p3;
			p3 = elm::vec3((x + 1) * pointStep, heightData[index1 + 1], y * pointStep);

			normal = cross(p1 - p2, p1 - p3);

			normals[index1] += normal;
			normals[index2 + 1] += normal;
			normals[index1 + 1] += normal;

			normals[index1] = elm::normalize(normals[index1]);
		}
		normals[index1 + 1] = elm::normalize(normals[index1 + 1]);
	}

	// Lastly, normalizing the normals not covered by the loop.
	for(uint i = width * (height - 1); i < normals.size(); i++)
		normals[i] = elm::normalize(normals[i]);

	auto average = [&width, this](uint id)
	{
		return (heightData[id - width - 1] + heightData[id - width] + heightData[id - width + 1] +
				heightData[id - 1] + heightData[id] * 3 + heightData[id + 1] +
				heightData[id + width - 1] + heightData[id + width] + heightData[id + width + 1]) / 11;
	};

	std::vector<Vertex> vertices(size);
	for(uint i = 0; i < height; i++)
	{
		for(uint j = 0; j < width; j++)
		{
			if(j == 0 || i == 0 || i == height - 1 || j == width - 1)
				verts[i * width + j] = elm::vec3(j * pointStep, heightData[i * width + j], i * pointStep);
			else
				verts[i * width + j] = elm::vec3(j * pointStep, average(i * width + j), i * pointStep);

			vertices[i * width + j].pos = verts[i * width + j];
			vertices[i * width + j].norm = normals[i * width + j];
		}
	}

	storePatchPixelError();

	// Fill with arbitrary values, since the previous pixel errors are only use once patches are skippe
	// i.e. not the first iteration
	float maxVal = settings->maxCondition;
	patchInfo = std::vector<SinglePatchInfo>(nPatches);
	for(uint i = 0; i < nPatches; i++)
	{
		patchInfo[i].pEBottom	= maxVal;
		patchInfo[i].pELeft		= maxVal;
		patchInfo[i].pERight	= maxVal;
		patchInfo[i].pETop		= maxVal;
	}

	HRESULT h0 = GraphicsDX11::getInstance()->createVertexBuffer(vertices.size(), sizeof(Vertex), (void**)&vertices[0]);
	HRESULT h1 = GraphicsDX11::getInstance()->createIndexBuffer(settings->bufferOffset * nPatches);

	if(FAILED(h0) || FAILED(h1))
	{
		MessageBox(hWnd, L"Failed to create vertex and / or index buffer", L"ERROR", S_OK);
		return;
	}

	if(settings->triangulateDuringInit)
	{
		lastUpdatePos = *cameraPos;
		initTriangulation();
	}
	else
		updateState = 0;

	GraphicsDX11::getInstance()->setAndCreateTexture(0, std::wstring(settings->texture1Name.begin(), settings->texture1Name.end()));
	GraphicsDX11::getInstance()->setAndCreateTexture(1, std::wstring(settings->texture2Name.begin(), settings->texture2Name.end()));
	GraphicsDX11::getInstance()->setAndCreateTexture(2, std::wstring(settings->texture3Name.begin(), settings->texture3Name.end()));
	GraphicsDX11::getInstance()->setAndCreateTexture(3, std::wstring(settings->blendmapName.begin(), settings->blendmapName.end()));
}

void TerrainManager::initTriangulation()
{
//#ifdef CPUTriangulation
	timer->start();
//#endif
	GraphicsDX11 *instance = GraphicsDX11::getInstance();

	float pointStep = settings->pointStep;

	float x = settings->patchDimensionX * pointStep;
	float z = settings->patchDimensionY * pointStep;

	float minCond = settings->minCondition;
	float maxCond = settings->maxCondition;
	//float maxDist = sqrt(	settings->width * settings->pointStep * settings->width * settings->pointStep +
	//						settings->height * settings->pointStep * settings->height * settings->pointStep);
	float maxDist = elm::vecLength(elm::vec2((nSplitsX - 0.5) * x, (nSplitsZ - 0.5) * z) - elm::vec2(x / 2, z / 2));

	std::vector<uint> ids(nPatches * 2);
	std::vector<int> sortedPatches(nPatches);

	for(uint i = 0; i < nPatches; i++)
	{
		elm::vec3 min = elm::vec3((i % nSplitsX) * x, minMaxHeights[i].first, (i / nSplitsZ) * z);
		elm::vec3 max = elm::vec3(((i % nSplitsX) + 1) * x, minMaxHeights[i].second, ((i / nSplitsZ) + 1) * z);
		patchPositions[i] = triple<elm::vec3, elm::vec3, uint>(min, max, i);

		elm::vec2 pos = (patchPositions[i].first + patchPositions[i].second).xz / 2;

		float error = (elm::vecLength(cameraPos->xz - pos) / maxDist) * (maxCond - minCond) + minCond;
		if(abs(error - pixelErrors[i]) < 0.1 && pixelErrors[i] != 0.f)
		{
			sortedPatches[i] = -1;
			continue;
		}

		sortedPatches[i] = i;
		pixelErrors[i] = error;
	}

	std::vector<std::vector<uint>> patches(nPatches);
	for(uint i = 0; i < nPatches; i++)
	{
		SinglePatchInfo &info = patchInfo[i];

		info.stateBottom = info.stateLeft = info.stateRight = info.stateTop = 1;

		if(i >= nSplitsX)
		{
			//info.prevPEBottom = _min(info.pEBottom, pixelErrors[i]);
			info.pEBottom = pixelErrors[i - nSplitsX];
			info.stateBottom = sortedPatches[i - nSplitsX] != -1;
		}
		if(i % nSplitsX != 0)
		{
			//info.prevPELeft = _min(info.pELeft, pixelErrors[i]);
			info.pELeft = pixelErrors[i - 1];
			info.stateLeft = sortedPatches[i - 1] != -1;
		}
		if(i < nPatches - 1 && (i + 1) % nSplitsX != 0)
		{
			//info.prevPERight = _min(info.pERight, pixelErrors[i]);
			info.pERight = pixelErrors[i + 1];
			info.stateRight = sortedPatches[i + 1] != -1;
		}
		if(i < nPatches - nSplitsX)
		{
			//info.prevPETop = _min(info.pETop, pixelErrors[i]);
			info.pETop = pixelErrors[i + nSplitsX];
			info.stateTop = sortedPatches[i + nSplitsX] != -1;
		}

		if(pixelErrors[i] != -1)
		//																							uint, so it will throw away the decimal part
		patches[i] = calculateSinglePatchedIndices(verts, pixelErrors[i], (i % nSplitsX) * nSplitsX, (i / nSplitsX) * nSplitsX, info);
	}

	//std::vector<std::vector<uint>> patches = calculatePatchedIndices(verts, pixelErrors, sortedPatches);

	std::vector<elm::vec2> points(0);
	//std::vector<uint> ids(nPatches * 2), sortedPatches(nPatches);

	for(uint i = 0; i < nPatches; i++)
	{
		if(sortedPatches[i] == -1)
			continue;

		uint offset = points.size();

		ids[i * 2] = points.size();
		points.resize(points.size() + patches[i].size());
		ids[i * 2 + 1] = points.size();

		for(uint j = 0; j < patches[i].size(); j++)
			points[offset + j] = verts[patches[i][j]].xz;

		//sortedPatches[i] = i;
	}

	instance->setUpdatedPatches(sortedPatches);

//#ifndef CPUTriangulation
//	HRESULT h = instance->setDispatchBuffer(nPatches, sizeof(uint), 7, (void**)&sortedPatches[0]);
//	if(FAILED(h))
//	{
//		MessageBox(hWnd, L"Failed to initiate a triangulation dispatch", L"ERROR", S_OK);
//		return;
//	}
//#endif

	uint numElements = points.size();

	// Sort the points based on distance to camera. Re-arrange the index list for minimal computation
	int swapID;
	float closestDist, dist;
	for(uint i = 0; i < patches.size(); i++)
	{
		swapID = -1;
		if(sortedPatches[i] == -1)
			continue;

		closestDist = elm::vecLength((patchPositions[sortedPatches[i]].first + patchPositions[sortedPatches[i]].second).xz / 2 - cameraPos->xz);
		for(uint j = i + 1; j < patches.size(); j++)
		{
			if(sortedPatches[j] == -1)
				continue;

			dist = elm::vecLength((patchPositions[sortedPatches[j]].first + patchPositions[sortedPatches[j]].second).xz / 2 - cameraPos->xz);
			if(dist < closestDist)
			{
				swapID = j;
				closestDist = dist;
			}
		}
		
		if(swapID != -1)
		{
			uint temp = sortedPatches[i];
			sortedPatches[i] = sortedPatches[swapID];
			sortedPatches[swapID] = temp;

//#ifndef CPUTriangulation
//			temp = ids[i * 2];
//			ids[i * 2] = ids[swapID * 2];
//			ids[swapID * 2] = temp;
//			temp = ids[i * 2 + 1];
//			ids[i * 2 + 1] = ids[swapID * 2 + 1];
//			ids[swapID * 2 + 1] = temp;
//#endif
		}
	}

	timer->stop();

	std::stringstream ss;
	ss << timer->getTime();
	//ss << triList.size();
	SetWindowTextA(hWnd, ss.str().c_str());
	ss.str("");

	timer->start();

#ifndef CPUTriangulation
	std::vector<uint> patchIndices(0);
	for(uint i = 0; i < nPatches; i++)
	{
		uint size = patches[i].size(), oldSize = patchIndices.size();
		patchIndices.resize(patchIndices.size() + size);
		for(uint j = 0; j < size; j++)
			patchIndices[oldSize + j] = patches[i][j];
	}

	//instance->lockDeviceContext();
	HRESULT h0 = instance->clearIndexBuffer();

	HRESULT h1 = instance->setDispatchBuffer(numElements, sizeof(elm::vec2), 1, (void**)&points[0]);
	HRESULT h2 = instance->setDispatchBuffer(nPatches * 2, sizeof(uint), 2, (void**)&ids[0]);
	HRESULT h3 = instance->setDispatchBuffer(numElements, maxNeighbours * sizeof(uint)* 3, 3);
	//HRESULT h4 = instance->setDispatchBuffer(numElements, maxNeighbours * sizeof(uint)* 3, 4);
	HRESULT h4 = instance->setDispatchBuffer(patchIndices.size(), sizeof(uint), 4, (void**)&patchIndices[0]);
	HRESULT h5 = instance->setDispatchBuffer(numElements * 2, sizeof(int), 5);
	HRESULT h6 = instance->setDispatchBuffer(nPatches, sizeof(uint), 6, (void**)&sortedPatches[0]);
	HRESULT h8 = instance->setDispatchBuffer(numElements, sizeof(uint), 7);
	//instance->unlockDeviceContext();

	HRESULT h7 = instance->dispatchTriangulation(nPatches);

	if(FAILED(h0) || FAILED(h1) || FAILED(h2) || FAILED(h3) || FAILED(h4) || FAILED(h5) || FAILED(h6) || FAILED(h7))
	{
		MessageBox(hWnd, L"Failed to initiate a triangulation dispatch", L"ERROR", S_OK);
		return;
	}

	bufferCopyLock = true;

	do{
		Sleep(5);
	} while(!instance->isTriangulationCompleted());
	//Sleep(1000);

	instance->swapIndexBuffer();
	instance->clearIndexBuffer();

#else

	uint offset = settings->bufferOffset;
	std::vector<uint> triList;
	std::vector<std::vector<int>> belongList;

	x = settings->patchDimensionX * settings->pointStep;
	z = settings->patchDimensionY * settings->pointStep;

	D3D11_MAPPED_SUBRESOURCE indexResource;
	for(uint i = 0; i < nPatches; i++)
	{
		uint size = triList.size(), start, end;

		if(sortedPatches[i] == -1)
			continue;
	
		start = ids[sortedPatches[i] * 2];
		end = ids[sortedPatches[i] * 2 + 1];

		belongList = std::vector<std::vector<int>>(end - start);
	
		// Write into the triangle list and then map the values to the index buffer. Do not write directly into
		// the mapped resource, as DirectX does not like using an index buffer that is currently mapped
		//for(uint j = ids[i * 2]; j < ids[i * 2 + 1]; j++)
		for(uint j = start; j < end; j++)
		{
			bool isBorderPoint =	(int)points[j].x % (int)x == 0 || 
									(int)points[j].y % (int)z == 0 ||
									points[j].x + pointStep == settings->width * pointStep ||
									points[j].y + pointStep == settings->height * pointStep;
		
			triangulate(j, points, start, end, triList, belongList, isBorderPoint);
		}
	
		instance->lockDeviceContext();
		instance->getDeviceContext()->Map(instance->getIndexBuffer(), 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &indexResource);
	
		uint currentPatch = (uint)	(patchPositions[sortedPatches[i]].first.x / x + 
									(patchPositions[sortedPatches[i]].first.z / z) * nSplitsX);
	
		uint *indices = (uint*)indexResource.pData;
		// Start by clearing all old data
		for(uint j = currentPatch * offset; j < (currentPatch + 1) * offset; j++)
			indices[j] = 0;
		for(uint j = size; j < triList.size(); j++)
			indices[currentPatch * offset + j - size] = patches[currentPatch][triList[j] - start];
	
		instance->getDeviceContext()->Unmap(instance->getIndexBuffer(), 0);
		instance->unlockDeviceContext();
	}

	//std::thread t1(&TerrainManager::threadedTriangulation, this, 0, 4, ids, points, patches);
	//std::thread t2(&TerrainManager::threadedTriangulation, this, 1, 4, ids, points, patches);
	//std::thread t3(&TerrainManager::threadedTriangulation, this, 2, 4, ids, points, patches);
	//std::thread t4(&TerrainManager::threadedTriangulation, this, 3, 4, ids, points, patches);
	//
	//t1.join();
	//t2.join();
	//t3.join();
	//t4.join();

	timer->stop();

	//std::stringstream ss;
	ss << timer->getTime();
	//ss << triList.size();
	SetWindowTextA(hWnd, ss.str().c_str());
#endif

	threadLock = false;
}

void TerrainManager::threadedTriangulation(uint threadOffset, uint numThreads, const std::vector<uint> &ids, const std::vector<elm::vec2> &points, const std::vector<std::vector<uint>> &patches)
{
	GraphicsDX11 *instance = GraphicsDX11::getInstance();

	uint offset = settings->bufferOffset;
	std::vector<uint> triList;
	std::vector<std::vector<int>> belongList;

	float x = settings->patchDimensionX * settings->pointStep;
	float z = settings->patchDimensionY * settings->pointStep;
	float pointStep = settings->pointStep;

	D3D11_MAPPED_SUBRESOURCE indexResource;
	for(uint i = threadOffset; i < nPatches; i += numThreads)
	{
		uint size = triList.size();

		belongList = std::vector<std::vector<int>>(ids[i * 2 + 1] - ids[i * 2]);

		// Write into the triangle list and then map the values to the index buffer. Do not write directly into
		// the mapped resource, as DirectX does not like using an index buffer that is currently mapped
		for(uint j = ids[i * 2]; j < ids[i * 2 + 1]; j++)
		{
			bool isBorderPoint =	(int)points[j].x % (int)x == 0 ||
									(int)points[j].y % (int)z == 0 ||
									points[j].x + pointStep == settings->width * pointStep ||
									points[j].y + pointStep == settings->height * pointStep;

			triangulate(j, points, ids[i * 2], ids[i * 2 + 1], triList, belongList, isBorderPoint);
		}

		instance->lockDeviceContext();
		instance->getDeviceContext()->Map(instance->getIndexBuffer(), 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &indexResource);

		uint currentPatch = (uint)(patchPositions[i].first.x / x +
			(patchPositions[i].first.z / z) * nSplitsX);

		uint *indices = (uint*)indexResource.pData;
		// Start by clearing all old data
		for(uint j = currentPatch * offset; j < (currentPatch + 1) * offset; j++)
			indices[j] = 0;
		for(uint j = size; j < triList.size(); j++)
			indices[currentPatch * offset + j - size] = patches[currentPatch][triList[j] - ids[i * 2]];

		instance->getDeviceContext()->Unmap(instance->getIndexBuffer(), 0);
		instance->unlockDeviceContext();
	}
}

void TerrainManager::storePatchPixelError()
{
	uint width = settings->width;
	uint dimY = settings->patchDimensionY;
	uint dimX = settings->patchDimensionX;

	float minDiff, maxDiff, diff;

	for(uint i = 0; i < nSplitsZ; i++)
	{
		for(uint j = 0; j < nSplitsX; j++)
		{
			minDiff = 999999.f;
			maxDiff = -999999.f;
			for(uint y = i * dimY + 1; y < (i + 1) * dimY; y++)
			{
				for(uint x = j * dimX + 1; x < (j + 1) * dimX; x++)
				{
					diff = abs(heightData[y * width + x] - heightData[y * width + x - 1]);
					if(diff < minDiff)
						minDiff = diff;
					else if(diff > maxDiff)
						maxDiff = diff;

					diff = abs(heightData[y * width + x] - heightData[(y - 1) * width + x]);
					if(diff < minDiff)
						minDiff = diff;
					else if(diff > maxDiff)
						maxDiff = diff;
				}
			}

			pixelErrorInPatch[i * nSplitsX + j].first = minDiff / settings->pointStep;
			pixelErrorInPatch[i * nSplitsX + j].second = maxDiff / settings->pointStep;
		}
	}
}

int counter = 0;
void TerrainManager::update()
{
	if(updateState == 1 && GetAsyncKeyState(0x33) != 0)
		updateState = 0;
	else if(updateState == 0 && GetAsyncKeyState(0x34) != 0)
		updateState = 1;

	if(toggleFrustumCulling.isDown(true))
		frustumCullingState = frustumCullingState == 0 ? 1 : 0;

	if(toggleCullingUpdate.isDown(true))
		cullingUpdateState = cullingUpdateState == 0 ? 1 : 0;

	// Lets start with 2D conditions for the time being
	// The state can be set so that the entire mesh can be explored in its current state without constant updates
	if(updateState == 0 && elm::vecLength(cameraPos->xz - lastUpdatePos.xz) > settings->updateDistance && !threadLock)
	{
		threadLock = true;
	
		lastUpdatePos = *cameraPos;
	
		thread = std::thread(&TerrainManager::initTriangulation, this);
		
		// Avoid having to join the thread
		thread.detach();
	}

	//if(bufferCopyLock)
	//{
	//	if(GraphicsDX11::getInstance()->isTriangulationCompleted())
	//	{
	//		GraphicsDX11::getInstance()->swapIndexBuffer();
	//		bufferCopyLock = false;
	//		//GraphicsDX11::getInstance()->copyIndexBufferData(counter, nSplitsX);
	//		//counter += nSplitsX;
	//		//if(counter >= nPatches)
	//		//{
	//		//	counter = 0;
	//		//	bufferCopyLock = false;
	//		//}
	//	}
	//}
}

// Try with a tree structure

std::vector<std::pair<uint, uint>> *TerrainManager::getPatches(const std::vector<elm::vec4> &frustum)
{
	elm::vec3 normal, axisVert, min, max;

	if(cullingUpdateState == 0)
		patchesToRender.clear();
	else
		return &patchesToRender;

	// No culling
	if(frustumCullingState == 1)
	{
		patchesToRender.push_back(std::pair<uint, uint>(nPatches, 0));
		return &patchesToRender;
	}

	for(uint i = 0; i < nPatches; i++)
	{
		bool hit = true;

		for(uint j = 0; j < frustum.size() && hit; j++)
		{
			normal = frustum[j].xyz;
			
			axisVert = elm::vec3(	normal.x < 0.f ? patchPositions[i].first.x : patchPositions[i].second.x,
									normal.y < 0.f ? patchPositions[i].first.y : patchPositions[i].second.y,
									normal.z < 0.f ? patchPositions[i].first.z : patchPositions[i].second.z);
			
			hit = (elm::dot(normal, axisVert) + frustum[j].w >= 0.f);
		}

		if(hit)
		{
			// Combine indices to reduce the number of draw calls. Impact on frame rate depends on
			// how much of the terrain is culled away, less culling means more improved frame rate
			//if(hit)
			//{
				if(patchesToRender.size() > 0 && i == patchesToRender.back().second + patchesToRender.back().first)
					patchesToRender.back().first++;
				else
					patchesToRender.push_back(std::pair<uint, uint>(1, i));
			//}
			//bool existingNeighbour = false;
			//for(uint j = 0; j < patchesToRender.size() && !existingNeighbour; j++)
			//{
			//	if(patchesToRender[j].first + patchesToRender[j].second == patchPositions[i].third)
			//	{
			//		existingNeighbour = true;
			//		patchesToRender[j].first++;
			//	}
			//}
			//if(!existingNeighbour)
			//	patchesToRender.push_back(std::pair<uint, uint>(1, patchPositions[i].third));
		}
	}

	return &patchesToRender;
}

TerrainManager::~TerrainManager()
{
	//if(thread.joinable())
	//	thread.join();

	SAFE_DELETE(settings);
	SAFE_DELETE(timer);
}