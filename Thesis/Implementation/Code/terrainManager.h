#ifndef _TERRAIN_MANAGER_
#define _TERRAIN_MANAGER_

#include <thread>

#include "stdafx.h"
#include "pointSelection.h"
#include "timer.h"
#include "settings.h"
#include "key.h"

class TerrainManager
{
private:
	int updateState, frustumCullingState, cullingUpdateState;
	bool threadLock, bufferCopyLock;
	uint nSplitsX, nSplitsZ, nPatches, maxIndicesInPatch;

	// Pixel errors and states for the point selection
	std::vector<SinglePatchInfo> patchInfo;

	elm::vec3 lastUpdatePos;
	const elm::vec3 *cameraPos;

	std::thread thread;

	std::vector<float> heightData, pixelErrors;
	std::vector<elm::vec3> verts;

	std::vector<std::pair<uint, uint>> patchesToRender;
	std::vector<triple<elm::vec3, elm::vec3, uint>> patchPositions;
	std::vector<std::pair<float, float>> pixelErrorInPatch, minMaxHeights;

	CPUTimer *timer;

	Settings *settings;

	Key toggleFrustumCulling, toggleCullingUpdate;

	void init();
	void initTriangulation();

	void threadedTriangulation(uint threadOffset, uint numThreads, const std::vector<uint> &ids, const std::vector<elm::vec2> &points, const std::vector<std::vector<uint>> &patches);

	void storePatchPixelError();
public:
	TerrainManager(const elm::vec3 *camPos);
	~TerrainManager();

	void update();

	std::vector<std::pair<uint, uint>> *getPatches(const std::vector<elm::vec4> &frustum);

	std::vector<float> *getHeights()	{ return &heightData; }
};

#endif