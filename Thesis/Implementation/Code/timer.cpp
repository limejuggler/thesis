#include "timer.h"

DispatchTimer::DispatchTimer(ID3D11Device *device, ID3D11DeviceContext *deviceContext)
{
	this->device = device;
	this->deviceContext = deviceContext;

	init();
}

void DispatchTimer::init()
{
	D3D11_QUERY_DESC desc;
	desc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
	desc.MiscFlags = 0;
	device->CreateQuery(&desc, &disjoint);

	desc.Query = D3D11_QUERY_TIMESTAMP;
	device->CreateQuery(&desc, &begin);
	device->CreateQuery(&desc, &end);
}

void DispatchTimer::start()
{
	deviceContext->Begin(disjoint);

	deviceContext->End(begin);
}

void DispatchTimer::stop()
{
	deviceContext->End(end);

	deviceContext->End(disjoint);
}

double DispatchTimer::getTime()
{
	UINT64 startTime = 0, endTime = 0;
	D3D11_QUERY_DATA_TIMESTAMP_DISJOINT disjointData;

	while(FAILED(deviceContext->GetData(begin, &startTime, sizeof(UINT64), 0)));
	while(FAILED(deviceContext->GetData(end, &endTime, sizeof(UINT64), 0)));
	while(FAILED(deviceContext->GetData(disjoint, &disjointData, sizeof(D3D11_QUERY_DATA_TIMESTAMP_DISJOINT), 0)));

	double time = -1.0;
	if(disjointData.Disjoint == FALSE)
	{
		double frequency = static_cast<double>(disjointData.Frequency);
		time = ((endTime - startTime) / frequency);
	}

	return time;
}

DispatchTimer::~DispatchTimer()
{
	if(begin)		begin->Release();
	if(end)			end->Release();
	if(disjoint)	disjoint->Release();
}

CPUTimer::CPUTimer()
{
	__int64 countsPerSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);

	secsPerCount = 1.0 / (double)countsPerSec;
}

void CPUTimer::start()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTimeStamp);
}

void CPUTimer::stop()
{
	previousTimeStamp = currentTimeStamp;

	QueryPerformanceCounter((LARGE_INTEGER*)&currentTimeStamp);
}

double CPUTimer::getTime()
{
	return (currentTimeStamp - previousTimeStamp) * secsPerCount;
}

CPUTimer::~CPUTimer()
{
}