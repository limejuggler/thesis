#ifndef _DISPATCH_TIMER_
#define _DISPATCH_TIMER_

#include <d3d11.h>

class DispatchTimer
{
private:
	ID3D11Query				*disjoint,
							*begin,
							*end;

	ID3D11Device			*device;
	ID3D11DeviceContext		*deviceContext;

	void init();
public:
	DispatchTimer(ID3D11Device *device, ID3D11DeviceContext *deviceContext);
	~DispatchTimer();

	void start();
	void stop();

	double getTime();
};

class CPUTimer
{
private:
	__int64					currentTimeStamp,
							previousTimeStamp;

	double					secsPerCount;
public:
	CPUTimer();
	~CPUTimer();

	void start();
	void stop();

	double getTime();
};

#endif