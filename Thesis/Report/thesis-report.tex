\documentclass[a4paper,oneside]{bth}

\usepackage{amsmath}
\usepackage{mathenv}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{textcomp}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{pifont}
\usepackage{changepage}
\usepackage{listings}
\usepackage{url}
\usepackage{xspace}
\usepackage{xtab}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{hyperref}
\DeclareGraphicsExtensions{.pdf}

\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

\lstset{language=C++}

\newtheorem{lem}{\textsc{Lemma}}[chapter]
\newtheorem{thm}{\textsc{Theorem}}[chapter]
\newtheorem{prop}{\textsc{Proposition}}[chapter]
\newtheorem{post}{Postulate}[chapter]
\newtheorem{corr}{\textsc{Corollary}}[chapter]
\newtheorem{defs}{\textsc{Definition}}[chapter]
\newtheorem{cons}{\textsc{Constraint}}[chapter]
\newtheorem{ex}{\textbf{Example}}[chapter]
\newtheorem{qu}{\textbf{Question}}[chapter]

\begin{document}

\pagestyle{plain}
\pagenumbering{roman}

% Front matter

{\pagestyle{empty}
\changepage{5cm}{1cm}{-0.5cm}{-0.5cm}{}{-2cm}{}{}{}
\noindent%   
{\small
\begin{tabular}{p{0.75\textwidth} p{0.25\textwidth}}
\textit{Master Thesis}&\multirow{4}{*}{\bthcsnotextlogo{3cm}}\\
\textit{Computer Science}\\
\textit{Thesis no: MCS-20YY-NN}\\
\textit{MM 2015}\\
\end{tabular}}

\begin{center}

\par\vspace {7cm}

{\Huge\textbf{Dynamic Multi-resolution Creation of Triangulated Irregular Network\\*[0.25cm]}}   

\par\vspace {0.5cm}

{\Large\textbf{Centered Subtitle Times Font Size 16 Bold}}                   

\par\vspace {3cm}

{\Large\textbf{Emil Bertilsson}}
\par\vspace {7cm}

\end{center}

\noindent%
{\small Dept. Computer Science \& Engineering \\
Blekinge Institute of Technology\\
SE--371 79 Karlskrona, Sweden}

\clearpage
}

{\pagestyle{empty}
\changepage{5cm}{1cm}{-0.5cm}{-0.5cm}{}{-2cm}{}{}{}
\noindent%
\begin{tabular}{p{\textwidth}}
{\small This thesis is submitted to the Department of Computer Science \& Engineering at Blekinge
Institute of Technology in partial fulfillment of the requirements for the degree of Master
of Science in Computer Science. The thesis is equivalent to 20 weeks of
full-time studies.}
\end{tabular}

\par\vspace {12cm}

\noindent%
\begin{tabular}{p{0.5\textwidth}lcl}
\textbf{Contact Information:}\\
Author(s):\\
Emil Bertilsson\\
E-mail: embl10@student.bth.se\\
\par\vspace {5cm}
University advisor:\\
Prof.\ Firstname Lastname\\
Dept. Computer Science \& Engineering
\par\vspace {1cm}
\noindent%
 \\
Dept. Computer Science \& Engineering & Internet & : & www.bth.se/didd\\
Blekinge Institute of Technology & Phone	& : & +46 455 38 50 00 \\
SE--371 79 Karlskrona, Sweden & Fax & : & +46 455 38 50 57 \\
\end{tabular}
\clearpage
} % Back to \pagestyle{plain}

\setcounter{page}{1}

% ABSTRACT

\abstract
\begin{changemargin}{+1cm}{+1cm}
\noindent
Triangulated irregular network can produce terrain meshes with a reduced triangle count, compared to a regular grid. These meshes are more difficult to optimize and are subsequently outperformed by regular and semi regular approaches with good optimization techniques. This thesis explores generation of view dependent triangulated irregular network meshes during run-time and how often these can be generated, as well as how strong the basis this work results in for continuous research. Triangle count, amount of triangles produced per second and pixel error will be used to measure the result, all of which will be gathered empirically.

%\textbf{Context}. Strategic release planning (sometimes referred to as road-mapping) is an important phase of the requirements engineering process performed at product level. It is concerned with selection and assignment of requirements in sequences of releases such that important technical and resource constraints are fulfilled.\newline

%\textbf{Objectives}. In this study we investigate which strategic release planning models have been proposed, their degree of empirical validation, their factors for requirements selection, and whether they are intended for a bespoke or market-driven requirements engineering context.\newline

%\textbf{Methods}. In this systematic review a number of article sources are used, including Compendex, Inspec, IEEE Xplore, ACM Digital Library, and Springer Link. Studies are selected after reading titles and abstracts to decide whether the articles are peer reviewed, and relevant to the subject.\newline

%\textbf{Results}. 24 strategic release planning models are found and mapped in relation to each other, and a taxonomy of requirements selection factors is constructed.\newline

%\textbf{Conclusions}. We conclude that many models are related to each other and use similar techniques to address the release planning problem. We also conclude that several requirement selection factors are covered in the different models, but that many methods fail to address factors such as stakeholder value or internal value. Moreover, we conclude that there is a need for further empirical validation of the models in full scale industry trials.

\par\vspace {1cm}
% 3-4 keywords, maximum 2 of these from the title, starts 1 line below the
% abstract.
\noindent
\textbf{Keywords:} Triangulated irregular network, view dependent terrain generation, GPGPU.

\end{changemargin}

%\include{acknowledgments} %OPTIONAL
%\listoffigures %in case you have them
%\listoftables %in case you have them
\listofalgorithms %in case you have them
\tableofcontents 

\cleardoublepage
\pagestyle{headings}
\pagenumbering{arabic}

\chapter{Introduction}
In the field of computer science, visualization and representation of large worlds is a subject that has been well studied. Terrain rendering can be found in a variety of applications and the detail and requirements of these terrain meshes has vastly increased over the years. Computer games and simulators often have heavy requirements on the visualization of the terrain, as well to what resolution it can be presented without overwhelming the other parts of the application. Whether the focus lies on preprocessing data or generating during start up or in real-time, many different techniques have been developed to address the problem of representing large quantities of terrain data.

These techniques can be divided into three major groups: regular meshes, semi irregular meshes and fully irregular meshes. A regular mesh refers to a mesh where the distance between each vertex point is always the same and triangles are subsequently of the same size. Due to their simplicity, regular meshes are easy to implement and the structure makes them well suited for further optimizations. The main disadvantage of regularly triangulated meshes is the absence of differentiation of terrain features, that is, areas lacking terrain features are treated no differently than areas with terrain features.

Semi irregular meshes utilizes splitting, merging and subdivision to produce a triangle count closer to what is required to present the terrain features without wasting too many triangles. This can be done either on a frame to frame basis or during selected time steps. The downside to any technique done on a frame to frame basis is that time is always spent on processing the mesh. While this reduces the effectiveness of the technique, it does mean that the technique can fully utilize view dependent operations which always optimizes the mesh based on the location of the camera.
% check that one, might not be 100% true
% also, ref lots of stuff to gobbettis survey here

Fully irregular meshes are theoretically very powerful, as the mesh can be created so that it consists of only the exact amount of points needed to represent the features of the mesh. Drawbacks of fully irregular approaches are the costly calculations that hinders the ability to generate in real-time and the increased complexity of the mesh. To address the time consuming operations, the terrain mesh can be generated in offline tools, but that adds unwanted complexity if there are any procedurally generated parts.

The amount of redundant triangles created by these techniques vary on different circumstances and settings. The redundant triangles created by a regular grid is based on the elevation differences of the mesh and whether or not much information is required to store the terrain features. For instance, a mesh representing a mountain with jagged edges and much detail will be more suited to a regular mesh than a mesh representing fields and small hills. A semi irregular mesh performs well but struggles where different terrain features meet. The transition from a low elevation area to a high elevation area cause extra complexity because of the edge following and the approximation made by suboptimal division. Fully irregular meshes created in offline tools are, like regular meshes, static and cannot utilize view dependent optimizations. Because of this restriction, fully irregular meshes are outperformed by semi irregular approaches.

The aim of this thesis is to develop and present a technique to produce view dependent procedurally generated irregular meshes to reduce the information required to represent the terrain mesh. This will result in a highly simplified mesh that is not generated each frame, but during selected distance intervals that are dependent on a user specified pixel error. The mesh is generated again once the specified distance has been covered. By varying the distance condition and the pixel error, the user has control over how the generation of the mesh in terms of its resolution and how often the mesh is updated. 

The resulting algorithm consists of two major steps:
\begin{enumerate}
   \item{A selection process on the central processing unit (CPU) that calculates what points are required for each update and}
   \item{A Delaunay triangulation done in parallel on the graphics processing unit (GPU).}
\end{enumerate}
The mesh is divided into patches with shared edge and border points to ensure that the resulting triangulation near the borders does not differ between patches. In other words, to prevent any holes or inconsistencies along the borders. By sharing vertices across patches, there is a slight increase of the total number of vertices, as some vertices are stored multiple times. Splitting the data into patches has the advantage that an entire patch can be fitted into one warp on the compute shader. In DirectX 11, the maximum threads that can be used in a warp is 1024. The patches can be defined in a few different ways: 32 by 32, 64 by 16, 64 by 32 (where each thread computes values for two patches) and so forth. 

Other advantages of splitting the generation into patches is the non linear scalability inherent to the algorithm. A Delaunay triangulation defines a circle of which no more than 3 points may be present. The size of the circle is incrementally increased until a third point for the triangle is found. Of the points that are inside the defined circle, the point that, combined with the other two points, create the largest angle is selected. Predictions can be made to reduce the amount of points examined during this step, but splitting into patches reduces the points much more efficiently. A patch is well defined with clear borders, but a prediction must work on any size and dimension which decreases the possibility of making optimized predictions.

Additionally, since the triangulation will be done using the compute shader, dependencies between the different groups of threads should be avoided, which fits the strategy of implementing patches well. Each patch can be handled separately with no dependency of any data present elsewhere, thanks to the shared points described earlier.

%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%				RELATED WORK				%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%										%
\chapter{Related Work}
Rendering the representation of a terrain with adaptive resolution have been studied in a number of papers, with different aims and angles. The earlier approaches \cite{surfaceMeshing1995, heightFields1996} has since been followed by more complex algorithms and algorithms that utilizes the GPU to different extent.

Many high performance techniques \cite{bdam2003, simplificationSimplified2002, soar2002, gputessellation2009} utilize per-frame calculations to achieve a mesh with close to an optimal amount of triangles, by performing splitting, merging and subdivision on the mesh. The focus of this thesis lies on triangulated irregular network (TIN) and generating view dependent meshes valid for a few frames up to potentially thousands of frames. As such, methods operating on a frame-to-frame basis will not be as extensively covered here, but any reader may find more information in \cite{roam1997, terrainSurvey2007, gputessellation2009}.

Apart from the relevance of algorithms that produce multi-resolution meshes, Delaunay triangulation \cite{delaunay1997} and TINs are the most related to what is being presented in this thesis. A few techniques will be discussed along with the relevance and differentiation to what is presented in this work. Techniques that will not be covered in this chapter are any algorithm dealing with optimized fully regular meshes, as these are too far away from the basic approach of a fully irregular mesh.

In Parallel Delaunay triangulation \cite{parallelDelaunay2012} and Optimistic parallel Delaunay triangulation \cite{optimisticParallelDelaunay2002}, algorithms are presented that uses point insertions to triangulate randomly generated spatial points. The algorithm presented in \cite{parallelDelaunay2012} groups points together into cells of roughly equal sizes and triangulates the cells in parallel, without any data dependency between them. It uses tetrahedrons and circumsphere to perform the three dimensional triangulation. The other algorithm in \cite{optimisticParallelDelaunay2002} does not utilize cells or tetrahedrons (and operates on two dimensions rather than three), but instead recursive checks after each insertion. Both algorithms does however operate on the CPU. But since the points used in the algorithm presented in this thesis spans a terrain, the points have properties that random points do not. Subsequently, simplifications can be made that render the more complex algorithms in \cite{optimisticParallelDelaunay2002} and especially \cite{parallelDelaunay2012} less fit. The strategy of computing independent cells (or patches) was implemented in a similar way, but less generic than what was presented. Both algorithms are relevant to this work and provided a good basis of which to build on.

Several papers discuss optimizing TIN meshes and different techniques to reduce the triangle count in real-time. One of the more interesting one is Batched Dynamic Adaptive Meshes (BDAM) \cite{bdam2003} which deals with small triangle patches of a few hundreds of triangles as primitives rather than single triangles. These patches, which are optimized TINs, are generated offline and are stored in a bintree, similar to \cite{roam1997}. To render the scene, a hierarchical view frustum culling is performed and the batches that are visible are then assembled. This is highly advantageous compared to other techniques that operate on a frame-to-frame basis in terms of time spent generating the scene. However, this approach does require a lot more memory storage, and can only operate on statically defined terrain meshes that has been preprocessed into small patches. Given that the focus of this work lies on minimizing preprocessing and more versatile usage than static terrains, the structure and ideas of the BDAM technique is harder to incorporate to what is presented here. Generating the TIN patches used in BDAM during real-time would be more along the lines of the focus here.

Another technique that utilizes tree structures is QuadTIN: Quadtree based Triangulated Irregular Networks \cite{quadtin2002} in which a quadtree is generated over any TIN surface. The algorithm generates a quadtree triangulation hierarchy offline and uses an adaptive level-of-detail (LOD) along with the stored quadtree, thereby achieving good performance. Furthermore, the algorithm can take any arbitrary LOD and represent the mesh with a single triangle strip. Even though the algorithm can operate on any TIN surface, the mesh must be predefined so that the preprocessing pass can generate and store the quadtree. It is a different angle of approach than what is presented here, but the two algorithms can to some extent be combined into something with low or no required preprocessing.

In Multiresolution Indexing of Triangulated Irregular Networks \cite{multirestin2004}, indexing the vertices in the TIN mesh is discussed in such a way that multi-resolution is achieved. By searching the mesh and defining an ordered index list in which consecutive triangles share an edge or a vertex, the mesh can be coarsened using a space-filling curve. The usage of space-filling curves are continuous and is oriented so that the authors refer to as the highest index and the lowest index in adjacent triangles coincide. It does however, like many other TIN implementations, rely on a TIN mesh generated offline. That being said, the method of searching the mesh to find an index list representing a triangle strip could be valid alternative to the structure discussed in Chapter \ref{chap:method}, where the mesh is rendered as a triangle list with an index buffer pointing to what vertices will be used.

Hierarchical Delaunay triangulation is discussed in Construction of Multi-resolution Terrain Models using Hierarchical Delaunay Triangulated Irregular Networks \cite{hierarchicalDelaunay1997} to generate multi-resolution terrain meshes based on a fixed error metric. Patches of the mesh are generated in a tree structure with decreasing error metrics until a predefined level has been reached. The technique generates several terrain models with good approximation, but the execution time of the algorithm fails to operate at run-time performance. If not for the increased memory consumption and static refinement thresholds, this algorithm could be the basis for this work to build upon.

%										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%				METHOD					%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%										%
\chapter{Method}
\label{chap:method}
By using the GPU to perform the triangulation, the initial approach was to bind an index buffer so that the compute shader step could write directly into it, and then use indexed drawing in the rendering method. By handling all the data read and write on the GPU, the costly operation of sending data between the CPU and GPU would be minimized. Additionally, since the output of the triangulation is in essence an index list, it would be a very suitable method without the need for any further processing. However, DirectX 11 does not allow index buffers to be bound on both output and input, meaning that an index buffer is not allowed to have write access on the GPU. If attempted, DirectX will output a warning during runtime stating that `Resource being set to Index Buffer is still bound on output! Forcing to NULL'. Any data in the index buffer would then subsequently be ignored.

This is briefly mentioned in the DirectX documentation, but there are no references to whether or not the functionality will be changed in following versions. The direct result of this was to rethink the strategy and look into other ways of making the data available for rendering, as there was no workaround that could use the same buffer in the different shader stages.

The simplest way of addressing this issue was to perform a copy between buffers. Two buffers were created with similar bind flags, the actual index buffer was given the regular flags that allows writing to it from CPU but not GPU, whereas the list filled out on the GPU was created with bind flags that interpreted the buffer as a byte address buffer. The data was then copied using \texttt{ID3D11DeviceContext::CopyResource} which performs the copy operation on the GPU as an asynchronous call, just like the dispatch call to the compute shader. The disadvantage with sending the data back to the CPU is therefore to some extent reduced, as the copy will occur right after the completion of the triangulation. In other words, there will never be a situation where the CPU is required to wait for the GPU and nor will there be any waiting time between the completion of the triangulation and the replacement of the old index buffer.

Similar to index buffers, vertex buffers does not exist in the compute shader step and must be handled differently if the intent is to fill it using the GPU. The same issue that prevents index buffers from being both read and write access on the GPU also limits the usage of vertex buffers. The selection process described in Algorithm \ref{alg:pointSelection} is more suited to CPU due to the shared boundary values and is made less beneficial for GPU usage because of the aforementioned issue. The directX shader model contains methods for converting between floating points and unsigned integers, which would allow a vertex buffer also bound as a byte address buffer to be filled during another shader step.

In the future, if the functionality is changed to allow read and write access, then the performance of the algorithm would be improved by the removal of data sending between the CPU and GPU. It would affect smaller terrain sizes to a higher degree than larger sizes, as a larger portion of the time is spent sending data with a smaller data set.




% write about the absence of t-junctions and cracks




%										%
%			ALGORITHM STRUCTURE				%
%										%
\subsection{Structure of the algorithm}
The whole calculation is done in an asynchronous thread that triggers once the user specified distance has been covered. When it is triggered, the thread receives the current location of the camera and uses the distance from the camera to the centre position of each patch in the pixel error calculation. In instances where the user travels faster than what the algorithm can generate the new mesh, the mesh will alternate between up to date and out of date, as the usage of the current camera location allows skipping outdated update steps. A patch also contains a resolution value, to keep track on the current minimal resolution of a patch so that patches can be excluded from the update if needed. Exclusion of a patch can occur when the position of the patch, in relation to the camera position, remains close to its previous value.

Starting with the selection process in Algorithm \ref{alg:pointSelection}, the thread is in charge of calculating what points are needed for the current update and sends these to the triangulation step. The patches are sorted based on the distance to the camera and once the triangulation in Algorithm \ref{alg:triangulation} has been completed for a patch, it is mapped into the corresponding area in the index buffer. The list of two dimensional positions is not changed during the sorting and is sent as one list containing all points for all patches, instead of splitting it into smaller pieces. Each patch receives a start and an end index, and it is these that are sorted to minimize operations when sorting. Since the points are selected separately, the triangulation is able to use only the two diminsional positions for the Voronoi diagram \cite{voronoi2013} and the Delaunay triangulation.

\begin{algorithm}[h!]
   \caption{Point selection}
   \label{alg:pointSelection}
   \begin{algorithmic}[1]

      \Procedure {Selection}{List<heightData>}
         \ForAll {$\text{point } p \text{ in List}$}
            \If {$p = \text{edge vertex}$}
               \State $\text{patch}[j] \gets p$ %\Comment{Average of all points surrounding p, including p}
               \State $\text{patch}[j-1] \gets p$
               \State $\text{patch}[j-width] \gets p$
               \State $\text{patch}[j-width-1] \gets p$
            \ElsIf {$\textit{diff}(p - 1, p) > \textit{cond}$ | $\textit{diff}(p - width, p) > \textit{cond}$ | $\textit{diff}(p, \text{last selected } p) > \textit{cond}$}
               \State $\text{patch}[j] \gets p$
               \If {$p = \text{border vertex}$}
                  \State $\text{patch}[\textit{left | right | up | down}] \gets p$
               \EndIf
            \Else
               \State $\text{continue}$
            \EndIf
         \EndFor
      \EndProcedure

   \end{algorithmic}
\end{algorithm}

The way the selection algorithm described in Algorithm \ref{alg:pointSelection} works is by taking a list of floating points, the distance between vertices and the location of the camera and outputs a list of indices. The distance between the current patch and the camera location is used as the condition that has to be satisfied in order to include the current index in the list. The algorithm compares the height differences between the point currently handled and the previous point and divides with the distance between each the points to normalize the difference. The height difference between points not adjacent (i.e. separated by more than one iteration) is divided by the distance between the two points and then multiplied with a factor that decreases for each iteration. This additional decrease solves the problem shown in Figure \ref{fig:gradualAscent} where the ascent from one point to the next is gradual enough to cause the algorithm to simplify to a higher extent than what is often intended.

\begin{figure}[h]
   \centering
   \includegraphics[width=6cm]{Images/gradualAscent2}
   \caption{Problem visualization of selecting points where the ascent is gradual, simplified to one dimension. The red line shows the outcome if the height difference between any of two adjacent points would not exceed the pixel error.}
   \label{fig:gradualAscent}
\end{figure}

Different settings are specified outside of the executable file, in a settings file which allows for changes to the overall structure of the regions. The thresholds for minimum and maimum allowed pixel difference per patch, the dimensions of the patches and the distance that must be covered before the triangulation is launched again is what can be set through this file. During run-time, different rendering states help visualizing the qualities of the mesh in terms of points belonging to what patch (shown by colour coding the patches) and drawing the lines making up the triangles rather than the full triangles. The triangulation can also be paused so that exploration of the mesh in its current state is made possible.

To provide a smoother result, the average height of the surrounding points are used instead of the original height value corresponding to the selected vertex. Points on the edge of a patch are always selected regardless of height differences. This condition translates into a minimum count of four vertices in any given patch, which in turns ensures that a patch will never have less than two triangles produced by the triangulation. 

\begin{algorithm}[h!]
   \caption{Delaunay triangulation}
   \label{alg:triangulation}
   \begin{algorithmic}[1]

      \Procedure {Triangulation}{\textit{i}, List<2D positions>}
         \State $\textit{p1} \gets \textit{findClosestPoint}(i)$
         \State $\textit{neighbour1} \gets \textit{p1}$
         \State $\textit{neighbour2} \gets -1$
         \While {$true$}
            \State $\textit{p2Included} \gets false$
            \ForAll {\text{triangles that include $i$ and} \textit{p1}}
               \If {\textit{p1} is in triangle}
                  \If {$\textit{angle}(List[i], List[\textit{p1}], List[\textit{p2}]) < PI$}
                     \State $\textit{p2Included} \gets true$
                     \State $\textit{p2} \gets \text{third triangle point}$
                     \State break
                  \EndIf
                  %\State
               \Else
                  \State $\text{continue}$
               \EndIf
            \EndFor
            \While {\textit{p2} not found}
               \State $circle \gets Line(List[i], List[\textit{p1}])$
               \Comment{define a circle that goes through $List[i]$ and $List[\textit{p1}]$ with an increased radius for each iteration}
               \ForAll {$\text{point } p \text{ in patch}$}
                  \If {$p$ is right of $line$}
                     \If {$p$ is not inside $circle$}
                        \State continue
                     \EndIf
                     \State $angle \gets getAngle(List[\textit{p1}], p, List[i])$
                     \If {$angle >= maxAngle$}
                        \State $\textit{p2} \gets p$
                        \State $maxAngle \gets angle$
                     \EndIf
                  \EndIf
               \EndFor
            \EndWhile
            \If {$\textit{p2Included} = false$}
               \State $\textit{triangleList} \gets \textit{Triangle(id, p1, p2)}$
               \If {\textit{neighbour1} = \textit{neighbour2}}
                  \State $\text{break}$
               \EndIf
            \EndIf
            \State $\textit{p1} \gets \textit{p2}$
            \If {\textit{neighbour2} = -1}
               \State $\textit{neighbour2} \gets \textit{p2}$
            \EndIf
         \EndWhile
      \EndProcedure

   \end{algorithmic}
\end{algorithm}

The Delaunay triangulation described in Algorithm \ref{alg:triangulation} is simplified down to two dimensions, which reduces the complexity and time consumption of the algorithm, without affecting the resulting mesh. This can be done as Algorithm \ref{alg:pointSelection} covers all conditions for whether or not a vertex is needed in the mesh's current state. This implementation strategy allowed calculation with circles and two dimensional lines, which simplified calculating if points are located so that the circle (or sphere) would include them if the radius increased. 

The triangulation itself can be described as three different tasks:
\begin{enumerate}
   \item{Finding the closest point for the point \textit{p}.}
   \item{Determining that the triangle about to added does not already exist (inside loop).}
   \item{Locating the third point in the triangle by geometrical calculations (inside loop).}
\end{enumerate}

Finding the closest point is less straight forward than it may sound, as it is done differently for the points on the border and those not on the border, to improve the quality of the selection. Unlike the case of a point inside the frame, points on the border does not per definition receive the closest point, but the closest point on the same border. Edge points are handled as though they are present on either the top or bottom frame and not on the left or right frame. The second triangle point for the first iteration of the loop is defined as such, but for subsequent iterations is defined as the third triangle point in the previous triangle.

For each patch, a list with the same size as the number of points in the patch is stored. Every element is a list containing pointers to a spot in the index buffer, so that the triangles a point belongs to can be tracked. This belong list is examined in the second step in the triangulation and ensures that no identical triangles are created. If a point and its first neighbour matches any region in the index buffer and if the angle between the three points in that region is less than pi, then the triangle that is about to be added already exists. When a new triangles is added, the belong list at each of the new triangles indices grows with the index to the region of the index buffer where the triangle is stored. The belong list is local within each patch.

\begin{figure}[h!]
   \centering
   \includegraphics[width=6cm]{Images/delaunayStep}
   \caption{Visualization of the triangulation step, where a) shows some of the circles that would be defined (with growing radius until a point is found) and b) the outcome for this set of points.}
   \label{fig:circleRadius}
\end{figure}

Calculating the third and final point in a triangle is done by defining a vector between the first and second point which is used to determine if a point will be enveloped by the circle that runs through the first two points. Figure \ref{fig:circleRadius} illustrates a few of the circles that would be defined during this stage and the resulting triangulation, with points and triangulation acquired during execution. The radius of the circle expands linnearly until a point is found. Examining the figure more closely shows that the third triangle point is always part of the hypotenuse, since the circle is defined so that it cuts the first two points.

%										%
%				PREPROCESSING				%
%										%
\subsection{Preprocessing}
In terms of required preprocessing before the technique can be utilized, the presented algorithm requires very little. Comparing with other TIN algorithms \cite{multirestin2004, bdam2003, hierarchicalDelaunay1997, quadtin2002} that processes up to the entire mesh outside of run-time, what is needed before the algorithm presented here can be launched is creating a vertex buffer from the height data. The height data can be read from a height map or generated procedurally, the only requirement is a set of three dimensional vertex positions spanning a terrain. The settings file also contains a flag for calculating the entire mesh during initialization or to launch the application as quickly as possible. If the flag is set to false and the camera is located up above the terrain, then the procedural generation may be observed (if dealing with larger terrain sets). Either way, this presented technique requires very little preprocessing. Furthermore, no visual artefacts or inconsistencies are produced between different levels of detail (LOD), eliminating any need for predefined triangle strips to merge patches of different LOD.

%										%
%				OPTIMIZATIONS				%
%										%
\subsection{Optimizations}
To increase the frame rate of the application, the algorithm is launched and dealt with in a thread separeted from the main application. The thread is asynchronous and therefore never joined, to avoid any blocking. In DirectX, a device \cite{directx11device} is free threaded, i.e. all member functions can be accessed to by several threads simultaneously. The device context on the other hand, are not free threaded. Therefore, every call to the device context from the asynchronous thread needed to employ mutex locks to prevent crashes.

A simple view frustum culling \cite{frustumCulling1999} was also implemented, where the frustum checks was performed against each patch. Instead of doing recursive checks with different levels, the frustum collision was tested for each patch and then the draw calls were combined so that the number of draw calls would be minimized. The reason for not implementing different levels were that the structure of the algorithm would require more time adapting the strategy for it to work. Firstly, the patches are sorted based on the distance from the camera and secondly, the algorithm supports user defined patch sizes. Both of which would require more time to find a general solution for defining the number of levels and distributing the nodes within these levels.

\chapter{Results}
CPU vs GPU

\chapter{Analysis}

\chapter{Conclusions and Future Work}

\bibliographystyle{plain}
\bibliography{thesis-report}
%\chapter*{References}

\end{document}
